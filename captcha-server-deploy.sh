#!/usr/bin/env bash

captcha_server_name=captcha-server
test_captcha_server_name=test-captcha-server

if [[ -n "$(docker ps -a | grep ${captcha_server_name})" ]];then
    docker rm -f ${captcha_server_name}
fi

docker run -d --name ${captcha_server_name} -e DB_INDEX=0 redis
docker run -d -p 6369:6379 --name ${test_captcha_server_name} redis

captcha_server_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${captcha_server_name})

while true;do
    nc -nvz ${captcha_server_ip} 6379
    if [[ $? -eq 0 ]];then
        echo "${captcha_server_name} ready"
        break
    fi
    sleep 1
done

while true;do
    nc -nvz 127.0.0.1 6369
    if [[ $? -eq 0 ]];then
        echo "${test_captcha_server_name} ready"
        break
    fi
    sleep 1
done