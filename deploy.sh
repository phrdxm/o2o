#!/usr/bin/env bash

mysql_name=mysql-server
mysql_pwd=a071e9bc
mysql_data_dir=/var/lib/mysql

mysql_slave_name=mysql-slave-server
mysql_slave_pwd=cfd974e0
mysql_slave_data_dir=/var/lib/mysql_slave

backend_app_name=backend-app

minio_name=minio-server
minio_access_key=79481043dd7c43f9
minio_secret_key=6e32199c8cd24d3a

test_captcha_server_name=test-captcha-server

log_dir=~/log/o2o

if [[ -e "$log_dir" ]];then
    rm -rf ${log_dir}
fi
mkdir -p ${log_dir}

if [[ -n "$(docker ps -a | grep ${test_captcha_server_name})" ]];then
    docker rm -f ${test_captcha_server_name}
fi

if [[ -n "$(docker ps -a | grep ${mysql_name})" ]];then
    docker rm -f ${mysql_name}
fi

if [[ -n "$(docker ps -a | grep ${mysql_slave_name})" ]];then
    docker rm -f ${mysql_slave_name}
fi

if [[ -n "$(docker ps -a | grep ${minio_name})" ]];then
    docker rm -f ${minio_name}
fi

docker create -e MYSQL_ROOT_PASSWORD=${mysql_pwd} -v ${mysql_data_dir}:/var/lib/mysql --name ${mysql_name} mysql-o2o
docker cp ./mysql/master-my.cnf ${mysql_name}:/etc/mysql/my.cnf
docker start ${mysql_name}

docker create -e MYSQL_ROOT_PASSWORD=${mysql_slave_pwd} -v ${mysql_slave_data_dir}:/var/lib/mysql --name ${mysql_slave_name} mysql-o2o
docker cp ./mysql/slave-my.cnf ${mysql_slave_name}:/etc/mysql/my.cnf
docker start ${mysql_slave_name}

docker run -d --name ${minio_name} \
  -e MINIO_ACCESS_KEY=${minio_access_key} \
  -e MINIO_SECRET_KEY=${minio_secret_key} \
  -v /mnt/data:/data \
  -v /mnt/config:/root/.minio \
  minio/minio server /data

mysql_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${mysql_name})
mysql_slave_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${mysql_slave_name})
minio_ip=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' ${minio_name})

while true;do
    nc -nvz ${mysql_ip} 3306
    if [[ $? -eq 0 ]];then
        echo "${mysql_name} ready"
        break
    fi
    sleep 1
done

while true;do
    nc -nvz ${mysql_slave_ip} 3306
    if [[ $? -eq 0 ]];then
        echo "${mysql_slave_name} ready"
        break
    fi
    sleep 1
done

/home/gitlab-runner/groovy-2.5.2/bin/groovy ./mysql/MasterSlaveConfig.groovy ${mysql_ip} ${mysql_pwd} ${mysql_slave_ip} ${mysql_slave_pwd}

/home/gitlab-runner/liquibase --driver=com.mysql.jdbc.Driver \
  --classpath=/home/gitlab-runner/.m2/repository/mysql/mysql-connector-java/5.1.47/mysql-connector-java-5.1.47.jar \
  --changeLogFile=./src/main/resources/db/include_all.xml \
  --url="jdbc:mysql://$mysql_ip/o2o_db?useUnicode=true&characterEncoding=utf-8&useSSL=false&autoReconnect=true&connectTimeout=30000&serverTimezone=GMT%2B8" \
  --username=root \
  --password=${mysql_pwd} migrate

while true;do
    nc -nvz ${minio_ip} 9000
    if [[ $? -eq 0 ]];then
        echo "${minio_name} ready"
        break
    fi
    sleep 1
done

docker build -t phr-o2o -f ./docker/Dockerfile .

if [[ -n "$(docker ps -a | grep ${backend_app_name})" ]];then
    docker rm -f ${backend_app_name};
fi

docker run -d -p 8080:8080 --name ${backend_app_name} \
  --link ${minio_name}:minio \
  --link ${mysql_name}:mysql \
  --link ${mysql_slave_name}:mysql_slave \
  --link captcha-server:captcha phr-o2o

none_images=$(docker images -f "dangling=true" -q)
echo ${none_images}

if [[ -n "$none_images" ]];then
    docker rmi ${none_images};
fi