package priv.phr.o2o.mapper;

import java.util.Collection;
import java.util.List;

import priv.phr.o2o.entity.ProductCategoryE;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface ProductCategoryMapper extends BaseMapper<ProductCategoryE> {

    @Insert({
            "<script>",
            "INSERT INTO t_product_category(id, shop_id, name, create_time, last_update_time) VALUES",
            "<foreach item='productCategory' collection='productCategories' separator=','>",
            "(#{productCategory.id}, #{productCategory.shopId}, #{productCategory.name}, #{productCategory.createTime}, #{productCategory.lastUpdateTime})",
            "</foreach>",
            "</script>"
    })
    int insertProductCategories(@Param("productCategories") Collection<ProductCategoryE> productCategories);

    @Select({
            "<script>",
            "SELECT name FROM t_product_category WHERE shop_id = #{shopId} AND name IN ",
            "<foreach item='productCategoryName' collection='productCategoryNames' open='(' separator=',' close=')'>",
            "#{productCategoryName}",
            "</foreach>",
            "</script>"
    })
    List<String> selectExists(@Param("shopId") String shopId, @Param("productCategoryNames") Collection<String> productCategoryNames);

    @Delete("DELETE FROM t_product_category WHERE shop_id = #{shopId}")
    int removeAllInShop(@Param("shopId") String shopId);

    @Select({
            " <script> ",
            " SELECT count(1) FROM t_product_category WHERE shop_id = #{shopId} AND name = #{name} ",
            " <if test='id != null'> ",
            " AND id != #{id} ",
            " </if> ",
            " </script> "
    })
    int existsInShop(@Param("shopId") String shopId, @Param("name") String name, @Param("id") String id);
}
