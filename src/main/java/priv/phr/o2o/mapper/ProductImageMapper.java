package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.ProductImageE;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface ProductImageMapper extends BaseMapper<ProductImageE> {

    @Delete("DELETE FROM t_product_image WHERE product_id = #{productId}")
    int removeProductDetailImages(@Param("productId") String productId);

    @Select("SELECT file_name FROM t_product_image WHERE product_id = #{productId}")
    List<String> selectFileNameByProductId(@Param("productId") String productId);
}
