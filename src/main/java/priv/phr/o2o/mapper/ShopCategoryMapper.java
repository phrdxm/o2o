package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.ShopCategoryE;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface ShopCategoryMapper extends BaseMapper<ShopCategoryE> {

    @Select(" SELECT id, create_time, last_update_time, name, description, image_file_name, parent_id FROM t_shop_category WHERE parent_id IS NULL ")
    List<ShopCategoryE> selectPrimaryCategories();

    @Select({
            " <script> ",
            " SELECT id, create_time, last_update_time, name, description, image_file_name, parent_id FROM t_shop_category ",
            " <where> ",
            " <choose> ",
            " <when test='parentId != null'> ",
            " parent_id = #{parentId} ",
            " </when> ",
            " <otherwise> ",
            " parent_id IS NOT NULL ",
            " </otherwise> ",
            " </choose> ",
            " </where> ",
            " </script> "
    })
    List<ShopCategoryE> selectSubCategories(@Param("parentId") String parentId);
}
