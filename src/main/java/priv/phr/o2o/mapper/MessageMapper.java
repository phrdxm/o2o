package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.MessageE;
import priv.phr.o2o.entity.UserE;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface MessageMapper extends BaseMapper<MessageE> {

    @Select({
            " SELECT id, last_update_time, create_time, message, send_time, from_user_id `from`, to_user_id `to` ",
            " FROM t_message ",
            " WHERE (from_user_id = #{id1} AND to_user_id = #{id2}) OR (from_user_id = #{id2} AND to_user_id = #{id1}) "
    })
    List<MessageE> selectMessagesBetween(@Param("id1") String userId1, @Param("id2") String userId2);

    @Select({
            " SELECT id, last_update_time, create_time, nickname, avator_file_name, email, gender, frozen, role ",
            " FROM t_user ",
            " WHERE id IN ( ",
            " SELECT DISTINCT CASE WHEN from_user_id = #{myUserId} THEN to_user_id ELSE from_user_id END ",
            " FROM t_message ",
            " WHERE from_user_id = #{myUserId} OR to_user_id = #{myUserId}) "
    })
    List<UserE> selectUsersTalkingTo(@Param("myUserId") String myUserId);
}
