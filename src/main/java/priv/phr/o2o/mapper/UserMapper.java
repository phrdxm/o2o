package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.dto.UserManagementInfoDTO;
import priv.phr.o2o.entity.UserE;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface UserMapper extends BaseMapper<UserE> {

    @Select({
            " SELECT (SELECT username FROM t_local_auth WHERE user_id = u.id) username, ",
            " nickname, avator_file_name, email, gender, frozen, role FROM t_user u WHERE role != 3 "
    })
    List<UserManagementInfoDTO> selectUsersInPage();
}
