package priv.phr.o2o.mapper;

import priv.phr.o2o.entity.AreaE;
import tk.mybatis.mapper.common.BaseMapper;

public interface AreaMapper extends BaseMapper<AreaE> {
}
