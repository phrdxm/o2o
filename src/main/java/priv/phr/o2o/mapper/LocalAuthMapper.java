package priv.phr.o2o.mapper;

import priv.phr.o2o.entity.LocalAuthE;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface LocalAuthMapper extends BaseMapper<LocalAuthE> {

    @Select(" SELECT COUNT(1) FROM t_local_auth WHERE username = #{username} ")
    int existsWithUsername(@Param("username") String username);
}
