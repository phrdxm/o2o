package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.ProductE;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.BaseMapper;

public interface ProductMapper extends BaseMapper<ProductE> {

    @Update("UPDATE t_product SET category_id = NULL, last_update_time = NOW() WHERE category_id = #{categoryId}")
    int updateCategoryWithNull(@Param("categoryId") String categoryId);

    @Delete("DELETE FROM t_product WHERE shop_id = #{shopId}")
    int removeAllInShop(@Param("shopId") String shopId);

    @Select({
            "<script>",
            " SELECT COUNT(1) FROM t_product WHERE shop_id = #{shopId} AND name = #{productId} ",
            " <if test='id != null'> ",
            " AND id != #{id} ",
            " </if> ",
            "</script>"
    })
    int existsInShop(@Param("shopId") String shopId, @Param("productId") String productName, @Param("id") String id);

    @Select({" <script> ",
            " SELECT id, create_time, last_update_time, name, description, image_file_name, number, normal_price, promotion_price, available, category_id, shop_id",
            " FROM t_product ",
            " <where> ",
            " shop_id = #{shopId} ",
            " <if test='categoryId != null'> ",
            " AND category_id = #{categoryId} ",
            " </if> ",
            " <if test='available'> ",
            " AND available = #{available} ",
            " </if> ",
            " </where> ",
            " </script> "
    })
    List<ProductE> selectProductsInPageByShopId(@Param("shopId") String shopId, @Param("categoryId") String categoryId, @Param("available") boolean available);

    @Select(" SELECT owner_id FROM t_shop WHERE id = (SELECT shop_id FROM t_product WHERE id = #{id}) ")
    String selectOwner(@Param("id") String id);
}
