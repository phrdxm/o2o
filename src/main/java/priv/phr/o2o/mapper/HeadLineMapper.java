package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.HeadLineE;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface HeadLineMapper extends BaseMapper<HeadLineE> {

    @Select({
            " <script> ",
            " SELECT id, last_update_time, create_time, title, url, image_file_name, available, priority FROM t_head_line ",
            " <if test='available != null'> ",
            " WHERE available = #{available} ",
            " </if> ",
            " </script> "
    })
    List<HeadLineE> selectByAvailable(@Param("available") Boolean available);
}
