package priv.phr.o2o.mapper.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import priv.phr.o2o.common.enumeration.ShopStatus;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes(ShopStatus.class)
public class ShopStatusTypeHandler implements TypeHandler<ShopStatus> {

    @Override
    public void setParameter(PreparedStatement ps, int i, ShopStatus parameter, JdbcType jdbcType) throws SQLException {
        ps.setInt(i, parameter.getCode());
    }

    @Override
    public ShopStatus getResult(ResultSet rs, String columnName) throws SQLException {
        return ShopStatus.parse(rs.getInt(columnName));
    }

    @Override
    public ShopStatus getResult(ResultSet rs, int columnIndex) throws SQLException {
        return ShopStatus.parse(rs.getInt(columnIndex));
    }

    @Override
    public ShopStatus getResult(CallableStatement cs, int columnIndex) throws SQLException {
        return ShopStatus.parse(cs.getInt(columnIndex));
    }
}
