package priv.phr.o2o.mapper.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import priv.phr.o2o.common.enumeration.Role;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes(Role.class)
public class RoleTypeHandler implements TypeHandler<Role> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Role role, JdbcType jdbcType) throws SQLException {
        preparedStatement.setInt(i, role.getCode());
    }

    @Override
    public Role getResult(ResultSet resultSet, String s) throws SQLException {
        return Role.parse(resultSet.getInt(s));
    }

    @Override
    public Role getResult(ResultSet resultSet, int i) throws SQLException {
        return Role.parse(resultSet.getInt(i));
    }

    @Override
    public Role getResult(CallableStatement callableStatement, int i) throws SQLException {
        return Role.parse(callableStatement.getInt(i));
    }
}
