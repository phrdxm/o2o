package priv.phr.o2o.mapper.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import priv.phr.o2o.common.enumeration.Gender;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

@MappedTypes(Gender.class)
public class GenderTypeHandler implements TypeHandler<Gender> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Gender gender, JdbcType jdbcType) throws SQLException {
        preparedStatement.setInt(i, gender.getCode());
    }

    @Override
    public Gender getResult(ResultSet resultSet, String s) throws SQLException {
        return Gender.parse(resultSet.getInt(s));
    }

    @Override
    public Gender getResult(ResultSet resultSet, int i) throws SQLException {
        return Gender.parse(resultSet.getInt(i));
    }

    @Override
    public Gender getResult(CallableStatement callableStatement, int i) throws SQLException {
        return Gender.parse(callableStatement.getInt(i));
    }
}
