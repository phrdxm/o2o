package priv.phr.o2o.mapper;

import java.util.List;

import priv.phr.o2o.entity.ShopE;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.BaseMapper;

public interface ShopMapper extends BaseMapper<ShopE> {

    @Select({
            " <script> ",
            " SELECT COUNT(1) FROM t_shop WHERE owner_id = #{ownerId} AND name = #{name} ",
            " <if test='id != null'> ",
            " AND id != #{id} ",
            " </if> ",
            " </script> "
    })
    int existsWithOwner(@Param("ownerId") String ownerId, @Param("name") String name, @Param("id") String id);

    @Select("SELECT COUNT(1) FROM t_shop WHERE owner_id = #{ownerId}")
    int selectCountWithOwner(@Param("ownerId") String ownerId);

    @Select({
            " <script> ",
            " SELECT id, last_update_time, create_time, name, description, address, phone, image_file_name, advice, status, area_id, owner_id, category_id ",
            " FROM t_shop ",
            " <where> ",
            " status = 1 ",
            " <if test='areaId != null'> ",
            " AND area_id = #{areaId} ",
            " </if> ",
            " <if test='categoryId != null'> ",
            " AND category_id IN (SELECT id FROM t_shop_category WHERE parent_id = #{categoryId}) ",
            " </if> ",
            " </where> ",
            " </script> "
    })
    List<ShopE> selectWithPrimaryCategoryAndArea(@Param("categoryId") String primaryCategoryId, @Param("areaId") String areaId);
}
