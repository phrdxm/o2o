package priv.phr.o2o.service;

import java.util.Collection;
import java.util.List;

import priv.phr.o2o.dto.ProductCategoryInfoDTO;
import priv.phr.o2o.dto.ProductCategoryUpdateDTO;
import priv.phr.o2o.entity.ProductCategoryE;
import org.springframework.transaction.annotation.Transactional;

public interface ProductCategoryService {

    ProductCategoryInfoDTO getProductCategoryByIdAndCheckOwner(String id);

    ProductCategoryE getProductCategoryById(String id);

    void checkOwner(ProductCategoryE productCategoryE);

    void checkNamesake(String shopId, String name, String id);

    List<ProductCategoryInfoDTO> getProductCategories(String shopId);

    @Transactional(rollbackFor = Throwable.class)
    Collection<ProductCategoryInfoDTO> addProductCategories(String shopId, Collection<String> productCategoryNames);

    @Transactional(rollbackFor = Throwable.class)
    void removeProductCategory(String productCategoryId);

    @Transactional(rollbackFor = Throwable.class)
    ProductCategoryInfoDTO updateProductCategory(ProductCategoryUpdateDTO productCategoryUpdateDTO);
}
