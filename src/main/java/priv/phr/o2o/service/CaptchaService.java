package priv.phr.o2o.service;

import java.io.IOException;

import priv.phr.o2o.dto.CaptchaInfoDTO;

public interface CaptchaService {

    CaptchaInfoDTO createCaptcha() throws IOException;
}
