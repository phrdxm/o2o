package priv.phr.o2o.service;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.dto.UserManagementInfoDTO;
import org.springframework.transaction.annotation.Transactional;

public interface UserManagementService {
    @Transactional(rollbackFor = Throwable.class)
    UserInfoDTO updateUserFrozen(String userId, boolean frozen);

    PageInfo<UserManagementInfoDTO> getUsersInPage(int pageNum, int pageSize);
}
