package priv.phr.o2o.service;

import java.io.IOException;

import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.dto.*;
import priv.phr.o2o.entity.LocalAuthE;
import priv.phr.o2o.entity.UserE;

public interface UserService {

    LocalAuthE getCurrentLocalAuth();

    void checkNamesake(String username);

    UserE getUserById(String id);

    UserInfoDTO convertUserEToDTO(UserE userE);

    JwtDTO signIn(LocalAuthDTO localAuthDTO);

    @Transactional(rollbackFor = Throwable.class)
    UserInfoDTO register(UserRegisterDTO registerDTO) throws IOException;

    UserInfoDTO getUserInfo(String userId);

    UserInfoDTO updateUserInfo(UserUpdateDTO updateDTO) throws IOException;

    void updatePassword(UpdatePasswordDTO updatePasswordDTO);

    String createAuthToken(AuthTokenCreateDTO authTokenDTO);
}
