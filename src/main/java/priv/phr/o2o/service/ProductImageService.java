package priv.phr.o2o.service;

import java.io.IOException;
import java.util.List;

import priv.phr.o2o.dto.ProductImageCreateDTO;
import priv.phr.o2o.dto.ProductImageInfoDTO;
import priv.phr.o2o.dto.ProductImageUpdateDTO;
import priv.phr.o2o.entity.ProductImageE;
import org.springframework.transaction.annotation.Transactional;

public interface ProductImageService {
    ProductImageE getProductImageById(String id);

    void checkOwner(ProductImageE productImageE);

    List<ProductImageInfoDTO> getProductImages(String productId);

    @Transactional(rollbackFor = Throwable.class)
    ProductImageInfoDTO createProductImage(ProductImageCreateDTO createDTO) throws IOException;

    @Transactional(rollbackFor = Throwable.class)
    ProductImageInfoDTO updateProductImage(ProductImageUpdateDTO updateDTO);

    @Transactional(rollbackFor = Throwable.class)
    void removeProductImage(String id);
}
