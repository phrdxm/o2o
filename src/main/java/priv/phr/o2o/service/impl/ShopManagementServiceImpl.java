package priv.phr.o2o.service.impl;

import java.util.Objects;

import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.enumeration.ShopStatus;
import priv.phr.o2o.common.exception.IllegalShopStatusException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.dto.ShopAdviceDTO;
import priv.phr.o2o.dto.ShopInfoDTO;
import priv.phr.o2o.entity.ShopE;
import priv.phr.o2o.mapper.ShopMapper;
import priv.phr.o2o.service.ShopManagementService;
import priv.phr.o2o.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.Util;

@Service
public class ShopManagementServiceImpl implements ShopManagementService {

    @Autowired
    private ShopService shopService;

    @Autowired
    private ShopMapper shopMapper;

    private void checkStatus(ShopE shopE, ShopStatus expected) {
        if (!Objects.equals(shopE.getStatus(), expected)) {
            throw new IllegalShopStatusException();
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShopInfoDTO commitShop(String shopId) {
        ShopE shopE = shopService.getShopById(shopId);
        shopService.checkOwner(shopE);
        checkStatus(shopE, ShopStatus.DISABLE);

        shopE.setStatus(ShopStatus.UNDER_REVIEW);
        AssertionUtil.equals(shopMapper.updateByPrimaryKey(shopE), 1, Constant.UPDATE_ROW_ERROR);

        ShopInfoDTO shopInfoDTO = Util.convert(shopE, ShopInfoDTO.class);
        shopInfoDTO.setStatus(shopE.getStatus().getCode());
        return shopInfoDTO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShopInfoDTO enableShop(String shopId) {
        ShopE shopE = shopService.getShopById(shopId);
        checkStatus(shopE, ShopStatus.UNDER_REVIEW);

        shopE.setStatus(ShopStatus.ENABLE);
        shopE.setAdvice(null);
        AssertionUtil.equals(shopMapper.updateByPrimaryKey(shopE), 1, Constant.UPDATE_ROW_ERROR);

        ShopInfoDTO shopInfoDTO = Util.convert(shopE, ShopInfoDTO.class);
        shopInfoDTO.setStatus(shopE.getStatus().getCode());
        return shopInfoDTO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShopInfoDTO disableShop(String shopId, ShopAdviceDTO shopAdviceDTO) {
        ShopE shopE = shopService.getShopById(shopId);
        if (!Objects.equals(ContextUtil.getCurrentUser().getRole(), Role.ADMIN)) {
            shopService.checkOwner(shopE);
        }

        shopE.setStatus(ShopStatus.DISABLE);
        shopE.setAdvice(shopAdviceDTO == null ? null : shopAdviceDTO.getAdvice());
        AssertionUtil.equals(shopMapper.updateByPrimaryKey(shopE), 1, Constant.UPDATE_ROW_ERROR);

        ShopInfoDTO shopInfoDTO = Util.convert(shopE, ShopInfoDTO.class);
        shopInfoDTO.setStatus(shopE.getStatus().getCode());
        return shopInfoDTO;
    }
}
