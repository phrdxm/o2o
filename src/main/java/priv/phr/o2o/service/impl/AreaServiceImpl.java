package priv.phr.o2o.service.impl;

import java.util.List;

import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.AreaInfoDTO;
import priv.phr.o2o.mapper.AreaMapper;
import priv.phr.o2o.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AreaServiceImpl implements AreaService {

    @Autowired
    private AreaMapper areaMapper;

    @Override
    public List<AreaInfoDTO> getAllAreas() {
        return Util.convertList(areaMapper.selectAll(), AreaInfoDTO.class);
    }
}
