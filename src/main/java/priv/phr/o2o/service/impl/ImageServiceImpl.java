package priv.phr.o2o.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Objects;
import java.util.UUID;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.annotation.BucketExist;
import priv.phr.o2o.common.enumeration.FileType;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.exception.FileDownloadException;
import priv.phr.o2o.common.exception.FileUploadException;
import priv.phr.o2o.common.exception.ResourceNotFoundException;
import priv.phr.o2o.common.exception.UploadFileNotImageException;
import priv.phr.o2o.service.ImageService;
import priv.phr.o2o.service.RemoteService;
import io.minio.errors.MinioException;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.xmlpull.v1.XmlPullParserException;

@Service
@Validated
public class ImageServiceImpl implements ImageService {

    private static final Logger log = LoggerFactory.getLogger(ImageServiceImpl.class);

    private RemoteService remoteService;

    @Override
    @Autowired // setter inject for mock
    public void setRemoteService(RemoteService remoteService) {
        this.remoteService = remoteService;
    }

    @Override
    public String uploadBase64Image(String base64Code, ImageCategory imageCategory) throws IOException {
        return uploadImage(Base64.getDecoder().decode(base64Code), imageCategory);
    }

    @Override
    public String uploadImage(byte[] fileSrc, ImageCategory imageCategory) throws IOException {
        FileType fileType = FileType.getFileType(fileSrc);
        if (Objects.equals(fileType, FileType.UNKNOWN)) {
            throw new UploadFileNotImageException();
        }
        byte[] uploadImageSrc;

        try (
                ByteArrayInputStream inputStream = new ByteArrayInputStream(fileSrc);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        ) {
            Thumbnails.of(inputStream).size(200, 200)
                    .watermark(Positions.BOTTOM_RIGHT, WATERMARK, .75f)
                    .outputQuality(.8f).toOutputStream(outputStream);

            uploadImageSrc = outputStream.toByteArray();
        }

        String fileName = UUID.randomUUID().toString();
        try (
                ByteArrayInputStream uploadStream = new ByteArrayInputStream(uploadImageSrc)
        ) {
            remoteService.uploadImageFile(uploadStream, fileType, imageCategory, fileName);
        } catch (MinioException | NoSuchAlgorithmException | XmlPullParserException | InvalidKeyException | IOException e) {
            try {
                log.info("上传文件失败，执行回滚");
                remoteService.removeIncompleteUpload(imageCategory.getName(), fileName);
            } catch (MinioException | XmlPullParserException | NoSuchAlgorithmException | InvalidKeyException rollbackError) {
                log.error("回滚失败", rollbackError);
            }
            log.info("回滚完成");
            throw new FileUploadException(e);
        }

        return fileName;
    }

    @Override
    public boolean isResourceNotFoundException(Throwable e) {
        return Objects.equals(e.getMessage(), "The specified key does not exist.");
    }

    @Override
    public byte[] getImage(@BucketExist String bucketName,
                           @Pattern(regexp = Constant.UUID_REGEXP, message = "文件名错误") String fileName) {
        try (
                InputStream inputStream = remoteService.getFromMinio(bucketName, fileName);
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream()
        ) {
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, len);
            }
            return outputStream.toByteArray();
        } catch (MinioException | NoSuchAlgorithmException | XmlPullParserException | InvalidKeyException | IOException e) {
            if (isResourceNotFoundException(e)) {
                throw new ResourceNotFoundException(e);
            } else {
                throw new FileDownloadException(e);
            }
        }
    }

    @Override
    public void removeImage(ImageCategory imageCategory, String fileName) {
        if (!StringUtils.hasText(fileName)) {
            return;
        }

        try {
            remoteService.removeImageFile(imageCategory.getName(), fileName);
        } catch (Throwable e) {
            log.error("删除文件 /{}/{}/{} 失败", Constant.IMAGE_URI_PREFIX, imageCategory.getName(), fileName, e);
        }
    }
}
