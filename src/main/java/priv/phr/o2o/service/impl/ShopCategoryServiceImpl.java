package priv.phr.o2o.service.impl;

import java.util.List;

import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.ShopCategoryInfoDTO;
import priv.phr.o2o.mapper.ShopCategoryMapper;
import priv.phr.o2o.service.ShopCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopCategoryServiceImpl implements ShopCategoryService {

    @Autowired
    private ShopCategoryMapper shopCategoryMapper;

    @Override
    public List<ShopCategoryInfoDTO> getAllPrimaryCategories() {
        return Util.convertList(shopCategoryMapper.selectPrimaryCategories(), ShopCategoryInfoDTO.class);
    }

    @Override
    public List<ShopCategoryInfoDTO> getSubCategories(String parentId) {
        return Util.convertList(shopCategoryMapper.selectSubCategories(parentId), ShopCategoryInfoDTO.class);
    }
}
