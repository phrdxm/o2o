package priv.phr.o2o.service.impl;

import static org.springframework.util.StringUtils.hasText;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.enumeration.Gender;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.exception.*;
import priv.phr.o2o.common.util.*;
import priv.phr.o2o.dto.*;
import priv.phr.o2o.entity.LocalAuthE;
import priv.phr.o2o.entity.UserE;
import priv.phr.o2o.mapper.LocalAuthMapper;
import priv.phr.o2o.mapper.UserMapper;
import priv.phr.o2o.service.ImageService;
import priv.phr.o2o.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private ImageService imageService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private LocalAuthMapper localAuthMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public LocalAuthE getCurrentLocalAuth() {
        LocalAuthE localAuthE = new LocalAuthE();
        localAuthE.setUserId(ContextUtil.getCurrentUserId());
        localAuthE = localAuthMapper.selectOne(localAuthE);
        AssertionUtil.notNull(localAuthE, Constant.SERVER_INNER_DATA_ERROR);

        return localAuthE;
    }

    @Override
    public void checkNamesake(String username) {
        if (localAuthMapper.existsWithUsername(username) > 0) {
            throw new UserNamesakeException();
        }
    }

    @Override
    public UserE getUserById(String id) {
        UserE userE = userMapper.selectByPrimaryKey(id);
        if (userE == null) {
            throw new UserNotFoundException();
        }

        return userE;
    }

    @Override
    public UserInfoDTO convertUserEToDTO(UserE userE) {
        UserInfoDTO userInfoDTO = Util.convert(userE, UserInfoDTO.class);
        userInfoDTO.setGender(userE.getGender() == null ? null : userE.getGender().getCode());
        userInfoDTO.setRole(userE.getRole() == null ? null : userE.getRole().getCode());

        return userInfoDTO;
    }

    @Override
    public JwtDTO signIn(LocalAuthDTO localAuthDTO) {
        LocalAuthE localAuthE = new LocalAuthE();
        localAuthE.setUsername(localAuthDTO.getUsername());
        LocalAuthE resultLocalAuthE = localAuthMapper.selectOne(localAuthE);
        if (resultLocalAuthE == null) {
            throw new SignInException(Constant.USERNAME_OR_PWD_ERROR, localAuthDTO.getUsername());
        }

        UserE resultUserE = userMapper.selectByPrimaryKey(resultLocalAuthE.getUserId());
        if (resultUserE.getFrozen()) {
            throw new AccountFrozenException(resultUserE);
        }

        if (PasswordUtil.check(localAuthDTO.getPassword(), resultLocalAuthE.getPassword())) {
            return new JwtDTO(JWTUtil.createJWT(resultUserE));
        }

        throw new SignInException(Constant.USERNAME_OR_PWD_ERROR, localAuthDTO.getUsername());
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public UserInfoDTO register(UserRegisterDTO registerDTO) throws IOException {
        checkNamesake(registerDTO.getUsername());

        String fileName = null;
        AssertionUtil.FailureCallback minioRollback = null;
        if (registerDTO.getBase64Avator() != null) {
            fileName = imageService.uploadBase64Image(registerDTO.getBase64Avator(), ImageCategory.USER_PROFILE);
            minioRollback = new AssertionUtil.MinioRollback(ImageCategory.USER_PROFILE, fileName);
        }

        UserE userE = new UserE();
        userE.setNickname(hasText(registerDTO.getNickname()) ? registerDTO.getNickname() : registerDTO.getUsername());
        userE.setEmail(hasText(registerDTO.getEmail()) ? registerDTO.getEmail() : null);
        userE.setGender(Gender.parse(registerDTO.getGender()));
        userE.setAvatorFileName(fileName);
        userE.setFrozen(false);
        userE.setRole(Role.USER);
        AssertionUtil.equals(userMapper.insert(userE), 1, Constant.INSERT_ROW_ERROR, minioRollback);

        LocalAuthE localAuthE = new LocalAuthE();
        localAuthE.setUsername(registerDTO.getUsername());
        localAuthE.setPassword(PasswordUtil.encode(registerDTO.getPassword()));
        localAuthE.setUserId(userE.getId());
        AssertionUtil.equals(localAuthMapper.insert(localAuthE), 1, Constant.INSERT_ROW_ERROR, minioRollback);

        UserInfoDTO userInfoDTO = convertUserEToDTO(userE);
        userInfoDTO.setJwt(JWTUtil.createJWT(userE));

        return userInfoDTO;
    }

    @Override
    public UserInfoDTO getUserInfo(String userId) {
        UserE targetUser;
        if (userId != null) {
            targetUser = getUserById(userId);
        } else {
            targetUser = ContextUtil.getCurrentUser();
        }

        return convertUserEToDTO(targetUser);
    }

    @Override
    public UserInfoDTO updateUserInfo(UserUpdateDTO updateDTO) throws IOException {
        UserE updateTarget = ContextUtil.getCurrentUser();

        if (!hasText(updateDTO.getNickname())) {
            updateDTO.setNickname(null);
        }

        boolean isUpdatable = false;
        AssertionUtil.MinioRollback minioRollback = null;
        String originalFileName = null;
        if (hasText(updateDTO.getBase64Avator())) {
            String fileName = imageService.uploadBase64Image(updateDTO.getBase64Avator(), ImageCategory.USER_PROFILE);
            minioRollback = new AssertionUtil.MinioRollback(ImageCategory.USER_PROFILE, fileName);
            originalFileName = updateTarget.getAvatorFileName();
            updateTarget.setAvatorFileName(fileName);
            isUpdatable = true;
        } else if (updateDTO.getBase64Avator() != null && hasText(updateTarget.getAvatorFileName())) {
            originalFileName = updateTarget.getAvatorFileName();
            updateTarget.setAvatorFileName(null);
            isUpdatable = true;
        }

        if (Util.updateProperties(updateTarget, updateDTO, "nickname", "email")) {
            isUpdatable = true;
        }

        if (Util.isValueUpdatable(updateTarget.getGender().getCode(), updateDTO.getGender())) {
            updateTarget.setGender(Gender.parse(updateDTO.getGender()));
            isUpdatable = true;
        }

        if (isUpdatable) {
            AssertionUtil.equals(userMapper.updateByPrimaryKey(updateTarget), 1, Constant.UPDATE_ROW_ERROR, minioRollback);
        }

        UserInfoDTO userInfoDTO = convertUserEToDTO(updateTarget);

        imageService.removeImage(ImageCategory.USER_PROFILE, originalFileName);

        return userInfoDTO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void updatePassword(UpdatePasswordDTO updatePasswordDTO) {
        LocalAuthE localAuthE = getCurrentLocalAuth();
        if (!PasswordUtil.check(updatePasswordDTO.getOriginalPassword(), localAuthE.getPassword())) {
            throw new AuthException(Constant.ORIGINAL_PWD_ERROR);
        }

        localAuthE.setPassword(PasswordUtil.encode(updatePasswordDTO.getNewPassword()));
        AssertionUtil.equals(localAuthMapper.updateByPrimaryKey(localAuthE), 1, Constant.UPDATE_ROW_ERROR);
    }

    @Override
    public String createAuthToken(AuthTokenCreateDTO authTokenCreateDTO) {
        LocalAuthE localAuthE = getCurrentLocalAuth();
        if (!PasswordUtil.check(authTokenCreateDTO.getPassword(), localAuthE.getPassword())) {
            throw new AuthException(Constant.PWD_ERROR);
        }

        String token = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set(ContextUtil.getCurrentUserId(), token, 4L, TimeUnit.MINUTES);
        return token;
    }
}
