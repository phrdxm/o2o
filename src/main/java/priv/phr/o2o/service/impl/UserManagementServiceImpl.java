package priv.phr.o2o.service.impl;

import java.util.Objects;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.dto.UserManagementInfoDTO;
import priv.phr.o2o.entity.UserE;
import priv.phr.o2o.mapper.UserMapper;
import priv.phr.o2o.service.UserManagementService;
import priv.phr.o2o.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.common.Constant;

@Service
public class UserManagementServiceImpl implements UserManagementService {

    private static final Logger log = LoggerFactory.getLogger(UserManagementServiceImpl.class);

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public UserInfoDTO updateUserFrozen(String userId, boolean frozen) {
        UserE userE = userService.getUserById(userId);

        if (!Objects.equals(userE.getRole(), Role.ADMIN)) {
            if (!Objects.equals(userE.getFrozen(), frozen)) {
                userE.setFrozen(frozen);
                AssertionUtil.equals(userMapper.updateByPrimaryKey(userE), 1, Constant.UPDATE_ROW_ERROR);
            }
        } else {
            log.warn("用户 {} 尝试修改管理员权限", ContextUtil.getCurrentUserId());
        }

        return userService.convertUserEToDTO(userE);
    }

    @Override
    public PageInfo<UserManagementInfoDTO> getUsersInPage(int pageNum, int pageSize) {
        if (pageSize < 1) {
            pageSize = 1;
        }

        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(userMapper.selectUsersInPage());
    }
}
