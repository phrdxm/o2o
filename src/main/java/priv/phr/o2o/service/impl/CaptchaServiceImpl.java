package priv.phr.o2o.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.imageio.ImageIO;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import priv.phr.o2o.common.enumeration.FileType;
import priv.phr.o2o.dto.CaptchaInfoDTO;
import priv.phr.o2o.service.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Autowired
    private DefaultKaptcha defaultKaptcha;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public CaptchaInfoDTO createCaptcha() throws IOException {
        String code = defaultKaptcha.createText();
        String token = UUID.randomUUID().toString();

        byte[] imageSrc;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            ImageIO.write(defaultKaptcha.createImage(code), FileType.JPEG.getExtension(), outputStream);
            imageSrc = outputStream.toByteArray();
        }

        CaptchaInfoDTO captchaInfoDTO = new CaptchaInfoDTO();
        captchaInfoDTO.setToken(token);
        captchaInfoDTO.setImage(Base64.getEncoder().encodeToString(imageSrc));

        redisTemplate.opsForValue().set(token, code, 8L, TimeUnit.MINUTES);

        return captchaInfoDTO;
    }
}
