package priv.phr.o2o.service.impl;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.enumeration.ShopStatus;
import priv.phr.o2o.common.exception.*;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.ShopInfoDTO;
import priv.phr.o2o.dto.ShopRegisterDTO;
import priv.phr.o2o.dto.ShopUpdateDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.entity.ShopCategoryE;
import priv.phr.o2o.entity.ShopE;
import priv.phr.o2o.entity.UserE;
import priv.phr.o2o.mapper.*;
import priv.phr.o2o.service.ImageService;
import priv.phr.o2o.service.ShopService;
import priv.phr.o2o.service.UserService;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private ImageService imageService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Autowired
    private AreaMapper areaMapper;

    @Autowired
    private ShopCategoryMapper shopCategoryMapper;

    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private UserMapper userMapper;

    private void checkArea(String areaId) {
        if (!areaMapper.existsWithPrimaryKey(areaId)) {
            throw new AreaNotFoundException();
        }
    }

    private ShopCategoryE checkSubCategory(String categoryId) {
        ShopCategoryE shopCategoryE = shopCategoryMapper.selectByPrimaryKey(categoryId);
        if (shopCategoryE == null) {
            throw new ShopCategoryNotFoundException();
        }
        if (shopCategoryE.getParentId() == null) {
            throw new ShopCategoryPrimaryException();
        }

        return shopCategoryE;
    }

    private ShopCategoryE checkCategory(String categoryId) {
        ShopCategoryE shopCategoryE = shopCategoryMapper.selectByPrimaryKey(categoryId);
        if (shopCategoryE == null) {
            throw new ShopCategoryNotFoundException();
        }

        return shopCategoryE;
    }

    @Override
    public ShopE getShopById(String id) {
        ShopE shopE = shopMapper.selectByPrimaryKey(id);
        if (shopE == null) {
            throw new ShopNotFoundException();
        }

        return shopE;
    }

    @Override
    public void checkOwner(ShopE shopE) {
        if (!Objects.equals(ContextUtil.getCurrentUserId(), shopE.getOwnerId())) {
            throw new AuthException(Constant.UNAUTH);
        }
    }

    @Override
    public void checkNamesake(String name, String id) {
        if (!Objects.equals(ContextUtil.getCurrentUser().getRole(), Role.USER)) {
            if (shopMapper.existsWithOwner(ContextUtil.getCurrentUserId(), name, id) > 0) {
                throw new ShopNamesakeException();
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShopInfoDTO registerShop(ShopRegisterDTO registerDTO) throws IOException {
        checkArea(registerDTO.getAreaId());
        checkSubCategory(registerDTO.getCategoryId());
        checkNamesake(registerDTO.getName(), null);

        String fileName = null;
        AssertionUtil.FailureCallback minioRollback = null;
        if (registerDTO.getBase64Image() != null) {
            fileName = imageService.uploadBase64Image(registerDTO.getBase64Image(), ImageCategory.SHOP);
            minioRollback = new AssertionUtil.MinioRollback(ImageCategory.SHOP, fileName);
        }

        ShopE shopE = Util.convert(registerDTO, ShopE.class);
        shopE.setImageFileName(fileName);
        shopE.setStatus(ShopStatus.UNDER_REVIEW);
        shopE.setOwnerId(ContextUtil.getCurrentUserId());
        AssertionUtil.equals(shopMapper.insert(shopE), 1, Constant.INSERT_ROW_ERROR, minioRollback);

        if (Objects.equals(ContextUtil.getCurrentUser().getRole(), Role.USER)) {
            ContextUtil.getCurrentUser().setRole(Role.OWNER);
            AssertionUtil.equals(userMapper.updateByPrimaryKey(ContextUtil.getCurrentUser()), 1, Constant.UPDATE_ROW_ERROR);
        }

        ShopInfoDTO shopInfoDTO = Util.convert(shopE, ShopInfoDTO.class);
        shopInfoDTO.setStatus(shopE.getStatus().getCode());
        return shopInfoDTO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ShopInfoDTO updateShopInfo(ShopUpdateDTO updateDTO) throws IOException {
        ShopE updateTarget = getShopById(updateDTO.getId());
        checkOwner(updateTarget);
        if (!StringUtils.hasText(updateDTO.getAddress())) {
            updateDTO.setAddress(null);
        }

        boolean isUpdatable = false;
        if (StringUtils.hasText(updateDTO.getName())) {
            checkNamesake(updateDTO.getName(), updateTarget.getId());
            if (!Objects.equals(updateDTO.getName(), updateTarget.getName())) {
                updateTarget.setName(updateDTO.getName());
                isUpdatable = true;
            }
        }

        if (Util.isValueUpdatable(updateTarget.getAreaId(), updateDTO.getAreaId())) {
            checkArea(updateDTO.getAreaId());
            updateTarget.setAreaId(updateDTO.getAreaId());
            isUpdatable = true;
        }

        if (Util.isValueUpdatable(updateTarget.getCategoryId(), updateDTO.getCategoryId())) {
            checkSubCategory(updateDTO.getCategoryId());
            updateTarget.setCategoryId(updateDTO.getCategoryId());
            isUpdatable = true;
        }

        AssertionUtil.FailureCallback minioRollback = null;
        String originalFileName = null;
        if (StringUtils.hasText(updateDTO.getBase64Image())) {
            String fileName = imageService.uploadBase64Image(updateDTO.getBase64Image(), ImageCategory.SHOP);
            minioRollback = new AssertionUtil.MinioRollback(ImageCategory.SHOP, fileName);
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(fileName);
            isUpdatable = true;
        } else if (updateDTO.getBase64Image() != null && StringUtils.hasText(updateTarget.getImageFileName())) {
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(null);
            isUpdatable = true;
        }

        if (Util.updateProperties(updateTarget, updateDTO, "description", "address", "phone")) {
            isUpdatable = true;
        }

        if (isUpdatable) {
            if (Objects.equals(updateTarget.getStatus(), ShopStatus.ENABLE)) {
                updateTarget.setStatus(ShopStatus.UNDER_REVIEW);
            }
            AssertionUtil.equals(shopMapper.updateByPrimaryKey(updateTarget), 1, Constant.UPDATE_ROW_ERROR, minioRollback);
        }

        ShopInfoDTO shopInfoDTO = Util.convert(updateTarget, ShopInfoDTO.class);
        shopInfoDTO.setStatus(updateTarget.getStatus().getCode());

        imageService.removeImage(ImageCategory.SHOP, originalFileName);

        return shopInfoDTO;
    }

    @Override
    @SuppressWarnings("unchecked")
    public PageInfo<ShopInfoDTO> getShopsInPage(int pageNum, int pageSize) {
        if (pageSize < 1) {
            pageSize = 1;
        }

        ShopE shopE = new ShopE();
        shopE.setOwnerId(ContextUtil.getCurrentUserId());

        PageHelper.startPage(pageNum, pageSize, "last_update_time DESC");
        PageInfo pageInfo = new PageInfo<>(shopMapper.select(shopE));

        List<ShopE> shops = pageInfo.getList();
        List<ShopInfoDTO> shopInfoDTOs = Util.convertList(shops, ShopInfoDTO.class);
        for (int i = 0; i < shops.size(); i++) {
            shopInfoDTOs.get(i).setStatus(shops.get(i).getStatus() == null ? null : shops.get(i).getStatus().getCode());
        }
        pageInfo.setList(shopInfoDTOs);

        return pageInfo;
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(rollbackFor = Throwable.class)
    public PageInfo<ShopInfoDTO> getShopsByCategoryAndAreaInPage(int pageNum, int pageSize, String categoryId, String areaId) {
        ShopCategoryE shopCategoryE = null;
        if (categoryId != null) {
            shopCategoryE = checkCategory(categoryId);
        }
        if (areaId != null) {
            checkArea(areaId);
        }

        if (pageSize < 1) {
            pageSize = 1;
        }

        PageInfo pageInfo;
        if (shopCategoryE == null || shopCategoryE.getParentId() != null) {
            ShopE shopE = new ShopE();
            shopE.setCategoryId(categoryId);
            shopE.setAreaId(areaId);
            shopE.setStatus(ShopStatus.ENABLE);
            PageHelper.startPage(pageNum, pageSize, "last_update_time DESC");
            pageInfo = new PageInfo<>(shopMapper.select(shopE));
        } else {
            PageHelper.startPage(pageNum, pageSize, "last_update_time DESC");
            pageInfo = new PageInfo<>(shopMapper.selectWithPrimaryCategoryAndArea(categoryId, areaId));
        }

        List<ShopE> shops = pageInfo.getList();
        List<ShopInfoDTO> shopInfoDTOs = Util.convertList(shops, ShopInfoDTO.class);
        for (int i = 0; i < shops.size(); i++) {
            shopInfoDTOs.get(i).setStatus(shops.get(i).getStatus() == null ? null : shops.get(i).getStatus().getCode());
        }
        pageInfo.setList(shopInfoDTOs);

        return pageInfo;
    }

    @Override
    public ShopInfoDTO getShopInfo(String id) {
        ShopE shopE = getShopById(id);
        ShopInfoDTO shopInfoDTO = Util.convert(shopE, ShopInfoDTO.class);
        shopInfoDTO.setStatus(shopE.getStatus() == null ? null : shopE.getStatus().getCode());

        return shopInfoDTO;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void removeShop(String id) {
        ShopE shopE = getShopById(id);
        checkOwner(shopE);

        productMapper.removeAllInShop(id);
        productCategoryMapper.removeAllInShop(id);
        AssertionUtil.equals(shopMapper.deleteByPrimaryKey(id), 1, Constant.DELETE_ROW_ERROR);

        if (shopMapper.selectCountWithOwner(ContextUtil.getCurrentUserId()) == 0) {
            ContextUtil.getCurrentUser().setRole(Role.USER);
            AssertionUtil.equals(userMapper.updateByPrimaryKey(ContextUtil.getCurrentUser()), 1, Constant.UPDATE_ROW_ERROR);
        }

        imageService.removeImage(ImageCategory.SHOP, shopE.getImageFileName());
    }


    @Override
    @Transactional(rollbackFor = Throwable.class)
    public UserInfoDTO getOwnerInfo(String id) {
        ShopE shopE = getShopById(id);
        AssertionUtil.matchesPattern(Constant.UUID_PATTERN, shopE.getOwnerId(), Constant.SERVER_INNER_DATA_ERROR);

        UserE userE = userMapper.selectByPrimaryKey(shopE.getOwnerId());
        AssertionUtil.notNull(userE, Constant.SERVER_INNER_DATA_ERROR);

        return userService.convertUserEToDTO(userE);
    }
}
