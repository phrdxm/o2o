package priv.phr.o2o.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.exception.MessageTargetNotFoundException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.MessageDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.entity.MessageE;
import priv.phr.o2o.mapper.MessageMapper;
import priv.phr.o2o.mapper.UserMapper;
import priv.phr.o2o.service.MessageService;
import priv.phr.o2o.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private MessageMapper messageMapper;

    @Autowired
    private UserService userService;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void saveMessage(MessageDTO messageDTO) {
        if (!userMapper.existsWithPrimaryKey(messageDTO.getTo())) {
            throw new MessageTargetNotFoundException();
        }
        MessageE messageE = Util.convert(messageDTO, MessageE.class);
        AssertionUtil.equals(messageMapper.insert(messageE), 1, Constant.INSERT_ROW_ERROR);
    }

    @Override
    @SuppressWarnings("unchecked")
    public PageInfo<MessageDTO> getLatestMessagesInPage(String with, int pageNum, int pageSize) {
        if (pageSize < 1) {
            pageSize = 1;
        }

        PageHelper.startPage(pageNum, pageSize, "send_time DESC");
        PageInfo pageInfo = new PageInfo<>(messageMapper.selectMessagesBetween(ContextUtil.getCurrentUserId(), with));
        pageInfo.setList(Util.convertList(pageInfo.getList(), MessageDTO.class));
        return pageInfo;
    }

    @Override
    public List<UserInfoDTO> getUsersInfoTalkingTo() {
        return messageMapper.selectUsersTalkingTo(ContextUtil.getCurrentUserId()).stream().map(
                userService::convertUserEToDTO
        ).collect(Collectors.toList());
    }
}
