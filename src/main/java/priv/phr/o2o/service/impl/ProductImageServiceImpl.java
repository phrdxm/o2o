package priv.phr.o2o.service.impl;

import static priv.phr.o2o.common.util.Util.*;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import priv.phr.o2o.common.exception.AuthException;
import priv.phr.o2o.common.exception.ProductImageNotFoundException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.dto.ProductImageCreateDTO;
import priv.phr.o2o.dto.ProductImageInfoDTO;
import priv.phr.o2o.dto.ProductImageUpdateDTO;
import priv.phr.o2o.entity.ProductImageE;
import priv.phr.o2o.mapper.ProductImageMapper;
import priv.phr.o2o.mapper.ProductMapper;
import priv.phr.o2o.service.ImageService;
import priv.phr.o2o.service.ProductImageService;
import priv.phr.o2o.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.util.Util;

@Service
public class ProductImageServiceImpl implements ProductImageService {

    @Autowired
    private ImageService imageService;

    @Autowired
    private ProductImageMapper productImageMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductService productService;

    @Override
    public ProductImageE getProductImageById(String id) {
        ProductImageE productImageE = productImageMapper.selectByPrimaryKey(id);
        if (productImageE == null) {
            throw new ProductImageNotFoundException();
        }

        return productImageE;
    }

    @Override
    public void checkOwner(ProductImageE productImageE) {
        if (!Objects.equals(ContextUtil.getCurrentUserId(), productMapper.selectOwner(productImageE.getProductId()))) {
            throw new AuthException(Constant.UNAUTH);
        }
    }

    @Override
    public List<ProductImageInfoDTO> getProductImages(String productId) {
        ProductImageE productImageE = new ProductImageE();
        productImageE.setProductId(productId);

        return Util.convertList(productImageMapper.select(productImageE), ProductImageInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ProductImageInfoDTO createProductImage(ProductImageCreateDTO createDTO) throws IOException {
        productService.checkOwner(productService.getProductById(createDTO.getProductId()));

        String fileName = imageService.uploadBase64Image(createDTO.getBase64Image(), ImageCategory.PRODUCT_DETAIL);
        AssertionUtil.FailureCallback minioRollback = new AssertionUtil.MinioRollback(ImageCategory.PRODUCT_DETAIL, fileName);

        ProductImageE productImageE = Util.convert(createDTO, ProductImageE.class);
        productImageE.setFileName(fileName);
        AssertionUtil.equals(productImageMapper.insert(productImageE), 1, Constant.INSERT_ROW_ERROR, minioRollback);

        return Util.convert(productImageE, ProductImageInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ProductImageInfoDTO updateProductImage(ProductImageUpdateDTO updateDTO) {
        ProductImageE updateTarget = getProductImageById(updateDTO.getId());
        checkOwner(updateTarget);

        if (updateProperties(updateTarget, updateDTO, "description")) {
            AssertionUtil.equals(productImageMapper.updateByPrimaryKey(updateTarget), 1, Constant.INSERT_ROW_ERROR);
        }

        return Util.convert(updateTarget, ProductImageInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void removeProductImage(String id) {
        ProductImageE productImageE = getProductImageById(id);
        checkOwner(productImageE);

        AssertionUtil.equals(productImageMapper.deleteByPrimaryKey(id), 1, Constant.DELETE_ROW_ERROR);
        imageService.removeImage(ImageCategory.PRODUCT_DETAIL, productImageE.getFileName());
    }
}
