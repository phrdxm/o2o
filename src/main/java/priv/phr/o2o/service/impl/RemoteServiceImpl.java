package priv.phr.o2o.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import priv.phr.o2o.common.enumeration.FileType;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.factory.MinioFactory;
import priv.phr.o2o.service.RemoteService;
import io.minio.errors.MinioException;
import org.springframework.stereotype.Service;
import org.xmlpull.v1.XmlPullParserException;

@Service
public class RemoteServiceImpl implements RemoteService {

    @Override
    public InputStream getFromMinio(String bucketName, String fileName)
            throws MinioException, IOException, InvalidKeyException, NoSuchAlgorithmException, XmlPullParserException {
        return MinioFactory.createClient().getObject(bucketName, fileName);
    }

    @Override
    public void uploadImageFile(ByteArrayInputStream uploadStream, FileType fileType, ImageCategory imageCategory, String fileName)
            throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        MinioFactory.createClient().putObject(imageCategory.getName(), fileName, uploadStream, uploadStream.available(),
                fileType.getContentType().toString());
    }

    @Override
    public void removeIncompleteUpload(String bucketName, String fileName)
            throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        MinioFactory.createClient().removeIncompleteUpload(bucketName, fileName);
    }

    @Override
    public void removeImageFile(String bucketName, String fileName)
            throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException {
        MinioFactory.createClient().removeObject(bucketName, fileName);
    }
}
