package priv.phr.o2o.service.impl;

import static priv.phr.o2o.common.Constant.*;
import static priv.phr.o2o.common.enumeration.ImageCategory.PRODUCT;
import static priv.phr.o2o.common.enumeration.ImageCategory.PRODUCT_DETAIL;
import static priv.phr.o2o.common.util.Util.*;
import static org.springframework.util.StringUtils.hasText;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.exception.ProductCategoryNotFoundException;
import priv.phr.o2o.common.exception.ProductNamesakeException;
import priv.phr.o2o.common.exception.ProductNotFoundException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.ProductCreateDTO;
import priv.phr.o2o.dto.ProductInfoDTO;
import priv.phr.o2o.dto.ProductUpdateDTO;
import priv.phr.o2o.entity.ProductE;
import priv.phr.o2o.entity.ProductImageE;
import priv.phr.o2o.mapper.ProductImageMapper;
import priv.phr.o2o.mapper.ProductMapper;
import priv.phr.o2o.mapper.ShopMapper;
import priv.phr.o2o.service.ImageService;
import priv.phr.o2o.service.ProductCategoryService;
import priv.phr.o2o.service.ProductService;
import priv.phr.o2o.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import priv.phr.o2o.common.util.ContextUtil;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ImageService imageService;

    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ShopService shopService;

    @Autowired
    private ProductImageMapper productImageMapper;

    @Autowired
    private ProductCategoryService productCategoryService;

    @Override
    public ProductE getProductById(String id) {
        ProductE productE = productMapper.selectByPrimaryKey(id);
        if (productE == null) {
            throw new ProductNotFoundException();
        }

        return productE;
    }

    @Override
    public void checkOwner(ProductE productE) {
        shopService.checkOwner(shopMapper.selectByPrimaryKey(productE.getShopId()));
    }

    @Override
    public void checkNamesake(String shopId, String name, String id) {
        if (productMapper.existsInShop(shopId, name, id) > 0) {
            throw new ProductNamesakeException();
        }
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ProductInfoDTO createProduct(ProductCreateDTO createDTO) throws IOException {
        shopService.checkOwner(shopService.getShopById(createDTO.getShopId()));
        checkNamesake(createDTO.getShopId(), createDTO.getName(), null);
        if (createDTO.getCategoryId() != null) {
            if (!Objects.equals(productCategoryService.getProductCategoryById(createDTO.getCategoryId()).getShopId(),
                    createDTO.getShopId())) {
                throw new ProductCategoryNotFoundException();
            }
        }

        String fileName = null;
        AssertionUtil.FailureCallback minioRollback = null;
        if (createDTO.getBase64Image() != null) {
            fileName = imageService.uploadBase64Image(createDTO.getBase64Image(), PRODUCT);
            minioRollback = new AssertionUtil.MinioRollback(PRODUCT, fileName);
        }

        ProductE productE = Util.convert(createDTO, ProductE.class);
        productE.setImageFileName(fileName);
        if (productE.getAvailable() == null) {
            productE.setAvailable(true);
        }
        if (productE.getNumber() == null) {
            productE.setNumber(MIN_PRODUCT_NUMBER);
        }
        AssertionUtil.equals(productMapper.insert(productE), 1, INSERT_ROW_ERROR, minioRollback);

        return Util.convert(productE, ProductInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ProductInfoDTO updateProductInfo(ProductUpdateDTO updateDTO) throws IOException {
        ProductE updateTarget = getProductById(updateDTO.getId());
        checkOwner(updateTarget);

        if (!hasText(updateDTO.getName())) {
            updateDTO.setName(null);
        }

        boolean isUpdatable = false;
        if (Util.isValueUpdatable(updateTarget.getName(), updateDTO.getName())) {
            checkNamesake(updateTarget.getShopId(), updateDTO.getName(), updateTarget.getId());
            updateTarget.setName(updateDTO.getName());
            isUpdatable = true;
        }

        AssertionUtil.FailureCallback minioRollback = null;
        String originalFileName = null;
        if (hasText(updateDTO.getBase64Image())) {
            String fileName = imageService.uploadBase64Image(updateDTO.getBase64Image(), PRODUCT);
            minioRollback = new AssertionUtil.MinioRollback(PRODUCT, fileName);
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(fileName);
            isUpdatable = true;
        } else if (updateDTO.getBase64Image() != null && hasText(updateTarget.getImageFileName())) {
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(null);
            isUpdatable = true;
        }

        if (Util.updateProperties(updateTarget, updateDTO, "description", "number", "normalPrice", "available")) {
            isUpdatable = true;
        }

        if (updateDTO.getPromotionPrice() != null && updateDTO.getPromotionPrice().compareTo(BigDecimal.ZERO) < 0) {
            if (updateTarget.getPromotionPrice() != null) {
                updateTarget.setPromotionPrice(null);
                isUpdatable = true;
            }
        } else if (updateProperties(updateTarget, updateDTO, "promotionPrice")) {
            isUpdatable = true;
        }

        if (isUpdatable) {
            AssertionUtil.equals(productMapper.updateByPrimaryKey(updateTarget), 1, UPDATE_ROW_ERROR, minioRollback);
        }

        imageService.removeImage(PRODUCT, originalFileName);

        return Util.convert(updateTarget, ProductInfoDTO.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public PageInfo<ProductInfoDTO> getProductsInPage(String shopId, String categoryId, int pageNum, int pageSize) {
        boolean available = !Objects.equals(shopService.getShopById(shopId).getOwnerId(), ContextUtil.getCurrentUserId()) &&
                Role.ADMIN != ContextUtil.getCurrentUser().getRole();

        if (pageSize < 1) {
            pageSize = 1;
        }

        PageHelper.startPage(pageNum, pageSize);
        PageInfo products = new PageInfo<>(productMapper.selectProductsInPageByShopId(shopId, categoryId, available));
        products.setList(Util.convertList(products.getList(), ProductInfoDTO.class));
        return products;
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void removeProduct(String id) {
        ProductE productE = getProductById(id);
        checkOwner(productE);

        AssertionUtil.equals(productMapper.deleteByPrimaryKey(id), 1, DELETE_ROW_ERROR);
        imageService.removeImage(PRODUCT, productE.getImageFileName());

        ProductImageE productImageE = new ProductImageE();
        productImageE.setProductId(id);

        List<String> fileNames = productImageMapper.selectFileNameByProductId(id);
        int count = productImageMapper.removeProductDetailImages(id);
        AssertionUtil.equals(fileNames.size(), count, DELETE_ROW_ERROR);

        for (String fileName : fileNames) {
            imageService.removeImage(PRODUCT_DETAIL, fileName);
        }
    }
}
