package priv.phr.o2o.service.impl;

import static org.springframework.util.StringUtils.hasText;
import static org.springframework.util.StringUtils.trimWhitespace;

import java.util.*;
import java.util.stream.Collectors;

import priv.phr.o2o.common.exception.ProductCategoryNamesakeException;
import priv.phr.o2o.common.exception.ProductCategoryNotFoundException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.dto.ProductCategoryInfoDTO;
import priv.phr.o2o.dto.ProductCategoryUpdateDTO;
import priv.phr.o2o.entity.ProductCategoryE;
import priv.phr.o2o.mapper.ProductCategoryMapper;
import priv.phr.o2o.mapper.ProductMapper;
import priv.phr.o2o.mapper.ShopMapper;
import priv.phr.o2o.service.ProductCategoryService;
import priv.phr.o2o.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.util.Util;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Autowired
    private ShopMapper shopMapper;

    @Autowired
    private ProductCategoryMapper productCategoryMapper;

    @Autowired
    private ShopService shopService;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public ProductCategoryInfoDTO getProductCategoryByIdAndCheckOwner(String id) {
        ProductCategoryE productCategoryE = getProductCategoryById(id);
        checkOwner(productCategoryE);

        return Util.convert(productCategoryE, ProductCategoryInfoDTO.class);
    }

    @Override
    public ProductCategoryE getProductCategoryById(String id) {
        ProductCategoryE productCategoryE = productCategoryMapper.selectByPrimaryKey(id);
        if (productCategoryE == null) {
            throw new ProductCategoryNotFoundException();
        }

        return productCategoryE;
    }

    @Override
    public void checkOwner(ProductCategoryE productCategoryE) {
        shopService.checkOwner(shopMapper.selectByPrimaryKey(productCategoryE.getShopId()));
    }

    @Override
    public void checkNamesake(String shopId, String name, String id) {
        if (productCategoryMapper.existsInShop(shopId, name, id) > 0) {
            throw new ProductCategoryNamesakeException();
        }
    }

    @Override
    public List<ProductCategoryInfoDTO> getProductCategories(String shopId) {
        shopService.getShopById(shopId);

        ProductCategoryE productCategoryE = new ProductCategoryE();
        productCategoryE.setShopId(shopId);

        return Util.convertList(productCategoryMapper.select(productCategoryE), ProductCategoryInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public Collection<ProductCategoryInfoDTO> addProductCategories(String shopId, Collection<String> productCategoryNames) {
        productCategoryNames = productCategoryNames.stream()
                .map(StringUtils::trimWhitespace)
                .filter(StringUtils::hasText)
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(productCategoryNames)) {
            return Collections.emptyList();
        }

        shopService.checkOwner(shopService.getShopById(shopId));

        productCategoryNames.removeAll(productCategoryMapper.selectExists(shopId, productCategoryNames));
        if (CollectionUtils.isEmpty(productCategoryNames)) {
            return Collections.emptyList();
        }

        List<ProductCategoryE> productCategories = new ArrayList<>(productCategoryNames.size());
        for (String name : productCategoryNames) {
            productCategories.add(new ProductCategoryE(shopId, name));
        }
        AssertionUtil.equals(productCategoryMapper.insertProductCategories(productCategories),
                productCategoryNames.size(), Constant.INSERT_ROW_ERROR);
        return Util.convertList(productCategories, ProductCategoryInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void removeProductCategory(String productCategoryId) {
        checkOwner(getProductCategoryById(productCategoryId));

        productMapper.updateCategoryWithNull(productCategoryId);
        AssertionUtil.equals(productCategoryMapper.deleteByPrimaryKey(productCategoryId), 1, Constant.DELETE_ROW_ERROR);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public ProductCategoryInfoDTO updateProductCategory(ProductCategoryUpdateDTO updateDTO) {
        ProductCategoryE updateTarget = getProductCategoryById(updateDTO.getId());
        checkOwner(updateTarget);

        if (!hasText(updateDTO.getName())) {
            return Util.convert(updateTarget, ProductCategoryInfoDTO.class);
        }

        updateDTO.setName(trimWhitespace(updateDTO.getName()));
        checkNamesake(updateTarget.getShopId(), updateDTO.getName(), updateTarget.getId());

        if (!Objects.equals(updateTarget.getName(), updateDTO.getName())) {
            updateTarget.setName(updateDTO.getName());
            AssertionUtil.equals(productCategoryMapper.updateByPrimaryKey(updateTarget), 1, Constant.UPDATE_ROW_ERROR);
        }

        return Util.convert(updateTarget, ProductCategoryInfoDTO.class);
    }
}
