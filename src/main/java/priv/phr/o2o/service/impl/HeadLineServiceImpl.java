package priv.phr.o2o.service.impl;

import static priv.phr.o2o.common.Constant.*;
import static priv.phr.o2o.common.enumeration.ImageCategory.HEAD_LINE;

import java.io.IOException;
import java.util.List;

import priv.phr.o2o.common.exception.HeadLineNotFoundException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.HeadLineCreateDTO;
import priv.phr.o2o.dto.HeadLineInfoDTO;
import priv.phr.o2o.dto.HeadLineUpdateDTO;
import priv.phr.o2o.entity.HeadLineE;
import priv.phr.o2o.mapper.HeadLineMapper;
import priv.phr.o2o.service.HeadLineService;
import priv.phr.o2o.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
public class HeadLineServiceImpl implements HeadLineService {

    @Autowired
    private HeadLineMapper headLineMapper;

    @Autowired
    private ImageService imageService;

    @Override
    public List<HeadLineInfoDTO> getAllHeadLines(Boolean available) {
        return Util.convertList(headLineMapper.selectByAvailable(available), HeadLineInfoDTO.class);
    }

    @Override
    public HeadLineE getHeadLineById(String id) {
        HeadLineE headLineE = headLineMapper.selectByPrimaryKey(id);
        if (headLineE == null) {
            throw new HeadLineNotFoundException();
        }

        return headLineE;
    }

    @Override
    public HeadLineInfoDTO createHeadLine(HeadLineCreateDTO createDTO) throws IOException {
        HeadLineE headLineE = Util.convert(createDTO, HeadLineE.class);

        if (createDTO.getAvailable() == null) {
            headLineE.setAvailable(true);
        }
        if (createDTO.getPriority() == null) {
            headLineE.setPriority(0);
        }

        AssertionUtil.FailureCallback minioRollback = null;
        if (createDTO.getBase64Image() != null) {
            headLineE.setImageFileName(imageService.uploadBase64Image(createDTO.getBase64Image(), HEAD_LINE));
            minioRollback = new AssertionUtil.MinioRollback(HEAD_LINE, headLineE.getImageFileName());
        }

        AssertionUtil.equals(headLineMapper.insert(headLineE), 1, INSERT_ROW_ERROR, minioRollback);

        return Util.convert(headLineE, HeadLineInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public HeadLineInfoDTO updateHeadLine(HeadLineUpdateDTO updateDTO) throws IOException {
        HeadLineE updateTarget = getHeadLineById(updateDTO.getId());
        if (!StringUtils.hasText(updateDTO.getTitle())) {
            updateDTO.setTitle(null);
        }
        if (!StringUtils.hasText(updateDTO.getUrl())) {
            updateDTO.setUrl(null);
        }

        boolean isUpdatable = Util.updateProperties(updateTarget, updateDTO, "title", "url", "available", "priority");

        AssertionUtil.FailureCallback minioRollback = null;
        String originalFileName = null;
        if (StringUtils.hasText(updateDTO.getBase64Image())) {
            String fileName = imageService.uploadBase64Image(updateDTO.getBase64Image(), HEAD_LINE);
            minioRollback = new AssertionUtil.MinioRollback(HEAD_LINE, fileName);
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(fileName);
            isUpdatable = true;
        } else if (updateDTO.getBase64Image() != null && StringUtils.hasText(updateTarget.getImageFileName())) {
            originalFileName = updateTarget.getImageFileName();
            updateTarget.setImageFileName(null);
            isUpdatable = true;
        }

        if (isUpdatable) {
            AssertionUtil.equals(headLineMapper.updateByPrimaryKey(updateTarget), 1, UPDATE_ROW_ERROR, minioRollback);
        }
        imageService.removeImage(HEAD_LINE, originalFileName);

        return Util.convert(updateTarget, HeadLineInfoDTO.class);
    }

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public void removeHeadLine(String id) {
        HeadLineE removeTarget = getHeadLineById(id);
        AssertionUtil.equals(headLineMapper.deleteByPrimaryKey(id), 1, DELETE_ROW_ERROR);
        imageService.removeImage(HEAD_LINE, removeTarget.getImageFileName());
    }
}
