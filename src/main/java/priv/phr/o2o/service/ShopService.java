package priv.phr.o2o.service;

import java.io.IOException;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.dto.ShopInfoDTO;
import priv.phr.o2o.dto.ShopRegisterDTO;
import priv.phr.o2o.dto.ShopUpdateDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.entity.ShopE;
import org.springframework.transaction.annotation.Transactional;

public interface ShopService {

    ShopE getShopById(String id);

    void checkOwner(ShopE shopE);

    void checkNamesake(String name, String id);

    @Transactional(rollbackFor = Throwable.class)
    ShopInfoDTO registerShop(ShopRegisterDTO shopRegisterDTO) throws IOException;

    @Transactional(rollbackFor = Throwable.class)
    ShopInfoDTO updateShopInfo(ShopUpdateDTO shopUpdateDTO) throws IOException;

    PageInfo<ShopInfoDTO> getShopsInPage(int pageNum, int pageSize);

    @Transactional(rollbackFor = Throwable.class)
    PageInfo<ShopInfoDTO> getShopsByCategoryAndAreaInPage(int pageNum, int pageSize, String categoryId, String areaId);

    ShopInfoDTO getShopInfo(String id);

    @Transactional(rollbackFor = Throwable.class)
    void removeShop(String id);

    @Transactional(rollbackFor = Throwable.class)
    UserInfoDTO getOwnerInfo(String id);
}
