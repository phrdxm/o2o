package priv.phr.o2o.service;

import java.io.IOException;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.dto.ProductCreateDTO;
import priv.phr.o2o.dto.ProductInfoDTO;
import priv.phr.o2o.dto.ProductUpdateDTO;
import priv.phr.o2o.entity.ProductE;
import org.springframework.transaction.annotation.Transactional;

public interface ProductService {
    ProductE getProductById(String id);

    void checkOwner(ProductE productE);

    void checkNamesake(String shopId, String name, String id);

    @Transactional(rollbackFor = Throwable.class)
    ProductInfoDTO createProduct(ProductCreateDTO createDTO) throws IOException;

    @Transactional(rollbackFor = Throwable.class)
    ProductInfoDTO updateProductInfo(ProductUpdateDTO updateDTO) throws IOException;

    PageInfo<ProductInfoDTO> getProductsInPage(String shopId, String categoryId, int pageNum, int pageSize);

    @Transactional(rollbackFor = Throwable.class)
    void removeProduct(String id);
}
