package priv.phr.o2o.service;

import priv.phr.o2o.dto.ShopAdviceDTO;
import priv.phr.o2o.dto.ShopInfoDTO;
import org.springframework.transaction.annotation.Transactional;

public interface ShopManagementService {
    @Transactional(rollbackFor = Throwable.class)
    ShopInfoDTO commitShop(String shopId);

    @Transactional(rollbackFor = Throwable.class)
    ShopInfoDTO enableShop(String shopId);

    @Transactional(rollbackFor = Throwable.class)
    ShopInfoDTO disableShop(String shopId, ShopAdviceDTO shopAdviceDTO);
}
