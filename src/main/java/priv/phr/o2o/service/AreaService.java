package priv.phr.o2o.service;

import java.util.List;

import priv.phr.o2o.dto.AreaInfoDTO;

public interface AreaService {
    List<AreaInfoDTO> getAllAreas();
}
