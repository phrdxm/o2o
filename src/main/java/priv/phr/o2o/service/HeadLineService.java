package priv.phr.o2o.service;

import java.io.IOException;
import java.util.List;

import priv.phr.o2o.dto.HeadLineCreateDTO;
import priv.phr.o2o.dto.HeadLineInfoDTO;
import priv.phr.o2o.dto.HeadLineUpdateDTO;
import priv.phr.o2o.entity.HeadLineE;

public interface HeadLineService {
    List<HeadLineInfoDTO> getAllHeadLines(Boolean available);

    HeadLineE getHeadLineById(String id);

    HeadLineInfoDTO createHeadLine(HeadLineCreateDTO headLineCreateDTO) throws IOException;

    HeadLineInfoDTO updateHeadLine(HeadLineUpdateDTO updateDTO) throws IOException;

    void removeHeadLine(String id);
}
