package priv.phr.o2o.service;

import java.util.List;

import priv.phr.o2o.dto.ShopCategoryInfoDTO;

public interface ShopCategoryService {
    List<ShopCategoryInfoDTO> getAllPrimaryCategories();

    List<ShopCategoryInfoDTO> getSubCategories(String parentId);
}
