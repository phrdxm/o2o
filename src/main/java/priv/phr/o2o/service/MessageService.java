package priv.phr.o2o.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.dto.MessageDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import org.springframework.transaction.annotation.Transactional;

public interface MessageService {
    @Transactional(rollbackFor = Throwable.class)
    void saveMessage(MessageDTO messageDTO);

    PageInfo<MessageDTO> getLatestMessagesInPage(String to, int pageNum, int pageSize);

    List<UserInfoDTO> getUsersInfoTalkingTo();
}
