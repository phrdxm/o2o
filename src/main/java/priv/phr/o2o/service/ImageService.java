package priv.phr.o2o.service;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.annotation.BucketExist;
import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.common.exception.ReadResourceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

public interface ImageService {

    void setRemoteService(RemoteService remoteService);

    String uploadBase64Image(String base64Code, ImageCategory imageCategory) throws IOException;

    String uploadImage(byte[] fileSrc, ImageCategory imageCategory) throws IOException;

    boolean isResourceNotFoundException(Throwable e);

    byte[] getImage(@BucketExist String bucketName,
                    @Pattern(regexp = Constant.UUID_REGEXP, message = Constant.IMAGE_FILE_NAME_ERROR) String fileName) throws IOException;

    void removeImage(ImageCategory imageCategory, String fileName);

    BufferedImage WATERMARK = initWatermark();

    static BufferedImage initWatermark() {
        try {
            BufferedImage watermark = ImageIO.read(new ClassPathResource("phr-o2o_watermark").getInputStream());
            Logger log = LoggerFactory.getLogger(ImageService.class);
            log.info("加载水印图片完成");
            return watermark;
        } catch (IOException e) {
            throw new ReadResourceException("读取水印文件异常", e);
        }
    }
}
