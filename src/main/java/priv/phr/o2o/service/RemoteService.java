package priv.phr.o2o.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import priv.phr.o2o.common.enumeration.FileType;
import priv.phr.o2o.common.enumeration.ImageCategory;
import io.minio.errors.MinioException;
import org.xmlpull.v1.XmlPullParserException;

public interface RemoteService {
    InputStream getFromMinio(String bucketName, String fileName)
            throws MinioException, IOException, InvalidKeyException, NoSuchAlgorithmException, XmlPullParserException;

    void uploadImageFile(ByteArrayInputStream uploadStream, FileType fileType, ImageCategory imageCategory, String fileName)
            throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException;

    void removeIncompleteUpload(String bucketName, String fileName)
            throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException;

    void removeImageFile(String bucketName, String fileName) throws MinioException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException, IOException;
}
