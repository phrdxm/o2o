package priv.phr.o2o.controller;

import java.io.IOException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.annotation.Captcha;
import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.*;
import priv.phr.o2o.service.UserService;

@RestController
@RequestMapping("/user")
@Validated
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/sign-in")
    @Captcha
    public JwtDTO signIn(@Valid @RequestBody LocalAuthDTO localAuthDTO) {
        return userService.signIn(localAuthDTO);
    }

    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    @Captcha
    public UserInfoDTO register(@Valid @RequestBody UserRegisterDTO userRegisterDTO) throws IOException {
        return userService.register(userRegisterDTO);
    }

    @GetMapping
    @RequireRole(Role.USER)
    public UserInfoDTO getUserInfo(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                   @RequestParam(value = "id", required = false) String userId) {
        return userService.getUserInfo(userId);
    }

    @PutMapping
    @RequireRole(Role.USER)
    @ResponseStatus(HttpStatus.CREATED)
    public UserInfoDTO updateUserInfo(@Valid @RequestBody UserUpdateDTO userUpdateDTO) throws IOException {
        return userService.updateUserInfo(userUpdateDTO);
    }

    @PutMapping("/password")
    @RequireRole(Role.USER)
    @Captcha
    public ResponseEntity<Object> updatePassword(@Valid @RequestBody UpdatePasswordDTO updatePasswordDTO) {
        userService.updatePassword(updatePasswordDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/auth-token")
    @RequireRole(Role.USER)
    @ResponseStatus(HttpStatus.CREATED)
    public String createAuthToken(@Valid @RequestBody AuthTokenCreateDTO authTokenCreateDTO) {
        return userService.createAuthToken(authTokenCreateDTO);
    }
}
