package priv.phr.o2o.controller.admin;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.ShopAdviceDTO;
import priv.phr.o2o.dto.ShopInfoDTO;
import priv.phr.o2o.service.ShopManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;

@RestController
@RequestMapping("/admin/shop")
@Validated
@CrossOrigin
public class ShopManagementController {

    @Autowired
    private ShopManagementService shopManagementService;

    @PutMapping("/commit/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ShopInfoDTO commitShop(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                  @PathVariable("id") String id) {
        return shopManagementService.commitShop(id);
    }

    @PutMapping("/enable/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.ADMIN)
    public ShopInfoDTO enableShop(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                  @PathVariable("id") String id) {
        return shopManagementService.enableShop(id);
    }

    @PutMapping("/disable/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ShopInfoDTO disableShop(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                   @PathVariable("id") String id,
                                   @Valid @RequestBody(required = false) ShopAdviceDTO shopAdviceDTO) {
        return shopManagementService.disableShop(id, shopAdviceDTO);
    }
}
