package priv.phr.o2o.controller.admin;

import javax.validation.constraints.Pattern;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.dto.UserManagementInfoDTO;
import priv.phr.o2o.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;

@RestController
@RequestMapping("/admin")
@RequireRole(Role.ADMIN)
@Validated
@CrossOrigin
public class UserManagementController {

    @Autowired
    private UserManagementService userManagementService;

    @PutMapping("/user/frozen/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public UserInfoDTO updateUserFrozen(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                        @PathVariable("id") String userId,
                                        @RequestParam("frozen") boolean frozen) {
        return userManagementService.updateUserFrozen(userId, frozen);
    }

    @GetMapping("/users")
    public PageInfo<UserManagementInfoDTO> getUsersInPage(@RequestParam("page") int pageNum, @RequestParam("size") int pageSize) {
        return userManagementService.getUsersInPage(pageNum, pageSize);
    }
}
