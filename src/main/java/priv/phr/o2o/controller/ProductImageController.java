package priv.phr.o2o.controller;

import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.ProductImageCreateDTO;
import priv.phr.o2o.dto.ProductImageInfoDTO;
import priv.phr.o2o.dto.ProductImageUpdateDTO;
import priv.phr.o2o.service.ProductImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;

@RestController
@Validated
@CrossOrigin
public class ProductImageController {

    @Autowired
    private ProductImageService productImageService;

    @GetMapping("/product-images")
    public List<ProductImageInfoDTO> getProductImages(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                      @RequestParam("product-id") String productId) {
        return productImageService.getProductImages(productId);
    }

    @PostMapping("/product-image")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ProductImageInfoDTO createProductImage(@Valid @RequestBody ProductImageCreateDTO productImageCreateDTO) throws IOException {
        return productImageService.createProductImage(productImageCreateDTO);
    }

    @PutMapping("/product-image")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ProductImageInfoDTO updateProductImage(@Valid @RequestBody ProductImageUpdateDTO productImageUpdateDTO) {
        return productImageService.updateProductImage(productImageUpdateDTO);
    }

    @DeleteMapping("/product-image/{id}")
    @RequireRole(Role.OWNER)
    public ResponseEntity removeProductImage(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.IMAGE_URI_ERROR)
                                             @PathVariable("id") String id) {
        productImageService.removeProductImage(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
