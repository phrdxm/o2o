package priv.phr.o2o.controller;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.exception.*;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.JWTUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.FailureDTO;
import priv.phr.o2o.dto.MessageDTO;
import priv.phr.o2o.entity.UserE;
import priv.phr.o2o.service.MessageService;

@ServerEndpoint("/chat")
@Component
public class O2OWebSocket {

    private static final Logger log = LoggerFactory.getLogger(O2OWebSocket.class);

    private static CopyOnWriteArraySet<O2OWebSocket> webSocketSet = new CopyOnWriteArraySet<>();

    private Session session;

    private UserE user;

    private MessageService messageService = ContextUtil.getBean(MessageService.class);

    private MessageDTO decode(String message) {
        Objects.requireNonNull(message);
        MessageDTO messageDTO;
        try {
            messageDTO = JSON.parseObject(message, MessageDTO.class);
        } catch (Exception e) {
            throw new MessageFormatException(MessageFormatException.JSON_PARSE_ERROR, e);
        }
        if (!Util.regexpMatches(Constant.UUID_PATTERN, messageDTO.getTo())) {
            throw new MessageTargetNotFoundException();
        }
        if (messageDTO.getMessage() == null) {
            messageDTO.setMessage("");
        } else if (messageDTO.getMessage().length() > 255) {
            throw new MessageFormatException(MessageFormatException.MESSAGE_TOO_LONG);
        }

        messageDTO.setFrom(user.getId());
        messageDTO.setSendTime(new Date());

        return messageDTO;
    }

    private String encode(Object o) {
        Objects.requireNonNull(o);
        return JSON.toJSONString(o);
    }

    @OnOpen
    public void onOpen(Session session) {
    }

    @OnClose
    public void onClose() {
        webSocketSet.remove(this);
        if (user != null) {
            log.info("{} 退出 websocket 连接", user.getId());
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("websocket on error", error);
        session.getAsyncRemote().sendText(encode(new FailureDTO(error)));
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        if (Util.regexpMatches(Constant.JWT_PATTERN, message)) {
            UserE user = JWTUtil.parseJWT(message);
            if (user.getFrozen()) {
                throw new AccountFrozenException(user);
            }
            if (user.getRole() == null || user.getRole().getCode() < 1) {
                throw new RoleException(Role.USER, user.getRole(), Constant.UNAUTH);
            }
            this.session = session;
            this.user = user;
            webSocketSet.add(this);
            session.getAsyncRemote().sendText("ACK");
            return;
        }

        if (user == null) {
            throw new AuthException(Constant.UNAUTH);
        }

        MessageDTO messageDTO = decode(message);
        for (O2OWebSocket webSocket : webSocketSet) {
            if (Objects.equals(messageDTO.getTo(), webSocket.user.getId())) {
                String encodedMessage = encode(messageDTO);
                webSocket.session.getAsyncRemote().sendText(encodedMessage);
                log.info("转发 {}", encodedMessage);
                break;
            }
        }

        messageService.saveMessage(messageDTO);
    }
}
