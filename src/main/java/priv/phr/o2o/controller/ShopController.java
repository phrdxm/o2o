package priv.phr.o2o.controller;

import java.io.IOException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.annotation.Auth;
import priv.phr.o2o.common.annotation.Captcha;
import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.annotation.Token;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.ShopInfoDTO;
import priv.phr.o2o.dto.ShopRegisterDTO;
import priv.phr.o2o.dto.ShopUpdateDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;

@RestController
@Validated
@CrossOrigin
public class ShopController {

    @Autowired
    private ShopService shopService;

    @PostMapping("/shop")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.USER)
    @Captcha
    public ShopInfoDTO register(@Valid @RequestBody ShopRegisterDTO shopRegisterDTO) throws IOException {
        return shopService.registerShop(shopRegisterDTO);
    }

    @PutMapping("/shop")
    @RequireRole(Role.OWNER)
    @ResponseStatus(HttpStatus.CREATED)
    public ShopInfoDTO updateShopInfo(@Valid @RequestBody ShopUpdateDTO shopUpdateDTO) throws IOException {
        return shopService.updateShopInfo(shopUpdateDTO);
    }

    @GetMapping("/shops")
    @RequireRole(Role.OWNER)
    public PageInfo<ShopInfoDTO> getShopsInPage(@RequestParam("page") int pageNum, @RequestParam("size") int pageSize) {
        return shopService.getShopsInPage(pageNum, pageSize);
    }

    @GetMapping("/shops-conditional")
    public PageInfo<ShopInfoDTO> getShopsByCategoryAndAreaInPage(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                                 @RequestParam(value = "category-id", required = false) String categoryId,
                                                                 @Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                                 @RequestParam(value = "area-id", required = false) String areaId,
                                                                 @RequestParam("page") int pageNum,
                                                                 @RequestParam("size") int pageSize) {
        return shopService.getShopsByCategoryAndAreaInPage(pageNum, pageSize, categoryId, areaId);
    }

    @GetMapping("/shop/{id}")
    public ShopInfoDTO getShopInfo(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                   @PathVariable("id") String id) {
        return shopService.getShopInfo(id);
    }

    @DeleteMapping("/shop/{id}")
    @RequireRole(Role.OWNER)
    @Auth
    public ResponseEntity removeShop(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                     @PathVariable("id") String id,
                                     @RequestParam @Token String token) {
        shopService.removeShop(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/shop/{id}/owner")
    public UserInfoDTO getShopOwnerInfo(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                        @PathVariable("id") String id) {
        return shopService.getOwnerInfo(id);
    }
}
