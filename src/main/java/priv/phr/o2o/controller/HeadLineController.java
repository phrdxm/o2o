package priv.phr.o2o.controller;

import static priv.phr.o2o.common.Constant.UUID_ERROR;
import static priv.phr.o2o.common.Constant.UUID_REGEXP;

import java.io.IOException;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.HeadLineCreateDTO;
import priv.phr.o2o.dto.HeadLineInfoDTO;
import priv.phr.o2o.dto.HeadLineUpdateDTO;
import priv.phr.o2o.service.HeadLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/head-line")
@Validated
public class HeadLineController {

    @Autowired
    private HeadLineService headLineService;

    @GetMapping
    public List<HeadLineInfoDTO> getAllHeadLines(@RequestParam(value = "available", required = false) Boolean available) {
        return headLineService.getAllHeadLines(available);
    }

    @GetMapping("/{id}")
    public HeadLineInfoDTO getHeadLineById(@Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                           @PathVariable("id") String id) {
        return Util.convert(headLineService.getHeadLineById(id), HeadLineInfoDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.ADMIN)
    public HeadLineInfoDTO createHeadLine(@RequestBody @Valid HeadLineCreateDTO headLineCreateDTO) throws IOException {
        return headLineService.createHeadLine(headLineCreateDTO);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.ADMIN)
    public HeadLineInfoDTO updateHeadLine(@RequestBody @Valid HeadLineUpdateDTO updateDTO) throws IOException {
        return headLineService.updateHeadLine(updateDTO);
    }

    @DeleteMapping("/{id}")
    @RequireRole(Role.ADMIN)
    public ResponseEntity removeHeadLine(@Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                         @PathVariable("id") String id) {
        headLineService.removeHeadLine(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
