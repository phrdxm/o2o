package priv.phr.o2o.controller;

import java.util.List;
import javax.validation.constraints.Pattern;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.MessageDTO;
import priv.phr.o2o.dto.UserInfoDTO;
import priv.phr.o2o.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.phr.o2o.common.Constant;

@RestController
@Validated
@CrossOrigin
@RequireRole(Role.USER)
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/message-history")
    public PageInfo<MessageDTO> getLatestMessagesInPage(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                        @RequestParam("to") String to,
                                                        @RequestParam("page") int pageNum,
                                                        @RequestParam("size") int pageSize) {
        return messageService.getLatestMessagesInPage(to, pageNum, pageSize);
    }

    @GetMapping("/message/users")
    public List<UserInfoDTO> getUsersInfoTalkingTo() {
        return messageService.getUsersInfoTalkingTo();
    }
}
