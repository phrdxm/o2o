package priv.phr.o2o.controller;

import static priv.phr.o2o.common.Constant.UUID_ERROR;
import static priv.phr.o2o.common.Constant.UUID_REGEXP;

import java.io.IOException;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import com.github.pagehelper.PageInfo;
import priv.phr.o2o.common.annotation.Captcha;
import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.dto.ProductCreateDTO;
import priv.phr.o2o.dto.ProductInfoDTO;
import priv.phr.o2o.dto.ProductUpdateDTO;
import priv.phr.o2o.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@CrossOrigin
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/product")
    @Captcha
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ProductInfoDTO createProduct(@Valid @RequestBody ProductCreateDTO productCreateDTO) throws IOException {
        return productService.createProduct(productCreateDTO);
    }

    @PutMapping("/product")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ProductInfoDTO updateProductInfo(@Valid @RequestBody ProductUpdateDTO productUpdateDTO) throws IOException {
        return productService.updateProductInfo(productUpdateDTO);
    }

    @GetMapping("/products")
    @RequireRole(Role.TOURIST)
    public PageInfo<ProductInfoDTO> getProductsInPage(@Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                                      @RequestParam("shop-id") String shopId,
                                                      @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                                      @RequestParam(value = "category-id", required = false) String categoryId,
                                                      @RequestParam("page") int pageNum,
                                                      @RequestParam("size") int pageSize) {
        return productService.getProductsInPage(shopId, categoryId, pageNum, pageSize);
    }

    @GetMapping("/product/{id}")
    public ProductInfoDTO getProductById(@Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                         @PathVariable("id") String id) {
        return Util.convert(productService.getProductById(id), ProductInfoDTO.class);
    }

    @DeleteMapping("/product/{id}")
    @RequireRole(Role.OWNER)
    public ResponseEntity removeProduct(@Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
                                        @PathVariable("id") String id) {
        productService.removeProduct(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
