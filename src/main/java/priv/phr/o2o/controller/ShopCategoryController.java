package priv.phr.o2o.controller;

import java.util.List;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.dto.ShopCategoryInfoDTO;
import priv.phr.o2o.service.ShopCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.phr.o2o.common.Constant;

@RestController
@Validated
@CrossOrigin
public class ShopCategoryController {

    @Autowired
    private ShopCategoryService shopCategoryService;

    @GetMapping("/primary-categories")
    public List<ShopCategoryInfoDTO> getAllPrimaryCategories() {
        return shopCategoryService.getAllPrimaryCategories();
    }

    @GetMapping("/category/sub-categories")
    public List<ShopCategoryInfoDTO> getSubCategories(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                      @RequestParam(value = "parent-id", required = false) String parentId) {
        return shopCategoryService.getSubCategories(parentId);
    }
}
