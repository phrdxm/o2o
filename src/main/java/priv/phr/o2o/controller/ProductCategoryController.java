package priv.phr.o2o.controller;

import java.util.Collection;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.enumeration.Role;
import priv.phr.o2o.dto.ProductCategoryInfoDTO;
import priv.phr.o2o.dto.ProductCategoryUpdateDTO;
import priv.phr.o2o.service.ProductCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.common.Constant;

@RestController
@Validated
@CrossOrigin
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @GetMapping("/product-category/{id}")
    @RequireRole(Role.OWNER)
    public ProductCategoryInfoDTO getProductCategory(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                     @PathVariable("id") String productCategoryId) {
        return productCategoryService.getProductCategoryByIdAndCheckOwner(productCategoryId);
    }

    @GetMapping("/product-categories")
    public List<ProductCategoryInfoDTO> getProductCategories(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                             @RequestParam("shop-id") String shopId) {
        return productCategoryService.getProductCategories(shopId);
    }

    @PostMapping("/product-categories")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public Collection<ProductCategoryInfoDTO> addProductCategories(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                                   @RequestParam("shop-id") String shopId,
                                                                   @Valid
                                                                   @RequestBody List<String> productCategoryNames) {
        return productCategoryService.addProductCategories(shopId, productCategoryNames);
    }

    @DeleteMapping("/product-category/{id}")
    @RequireRole(Role.OWNER)
    public ResponseEntity removeProductCategory(@Pattern(regexp = Constant.UUID_REGEXP, message = Constant.UUID_ERROR)
                                                @PathVariable("id") String productCategoryId) {
        productCategoryService.removeProductCategory(productCategoryId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/product-category")
    @ResponseStatus(HttpStatus.CREATED)
    @RequireRole(Role.OWNER)
    public ProductCategoryInfoDTO updateProductCategory(@Valid @RequestBody ProductCategoryUpdateDTO productCategoryUpdateDTO) {
        return productCategoryService.updateProductCategory(productCategoryUpdateDTO);
    }
}
