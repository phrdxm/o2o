package priv.phr.o2o.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.enumeration.FileType;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/image")
@CrossOrigin
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping("/{bucket_name}/{file_name}")
    public void getImage(@PathVariable("bucket_name") String bucketName,
                         @PathVariable("file_name") String fileName,
                         HttpServletResponse response) throws IOException {
        byte[] fileSrc = imageService.getImage(bucketName, fileName);
        FileType fileType = FileType.getFileType(fileSrc);
        AssertionUtil.notEquals(fileType, FileType.UNKNOWN, Constant.SERVER_INNER_DATA_ERROR);

        response.setContentType(fileType.getContentType().toString());
        response.setHeader("Content-Disposition", String.format("inline;filename=%s.%s", fileName, fileType.getExtension()));
        response.setStatus(HttpStatus.OK.value());
        response.getOutputStream().write(fileSrc);
        response.flushBuffer();
    }
}
