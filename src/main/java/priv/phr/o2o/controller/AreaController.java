package priv.phr.o2o.controller;

import java.util.List;

import priv.phr.o2o.dto.AreaInfoDTO;
import priv.phr.o2o.service.AreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AreaController {

    @Autowired
    private AreaService areaService;

    @GetMapping("/areas")
    public List<AreaInfoDTO> getAllAreas() {
        return areaService.getAllAreas();
    }
}
