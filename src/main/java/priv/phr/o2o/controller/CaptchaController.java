package priv.phr.o2o.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.*;
import priv.phr.o2o.dto.CaptchaInfoDTO;
import priv.phr.o2o.service.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("/captcha")
@CrossOrigin
public class CaptchaController {

    @Autowired
    private CaptchaService captchaService;

    @PostMapping
    public CaptchaInfoDTO createCaptcha() throws IOException {
        return captchaService.createCaptcha();
    }
}
