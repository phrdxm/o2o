package priv.phr.o2o.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import priv.phr.o2o.common.AuditObject;

@Table(name = "t_message")
public class MessageE extends AuditObject {

    @Column(name = "from_user_id")
    private String from;

    @Column(name = "to_user_id")
    private String to;

    private String message;

    private Date sendTime;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }
}
