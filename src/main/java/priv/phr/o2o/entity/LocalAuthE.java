package priv.phr.o2o.entity;

import javax.persistence.Table;

import priv.phr.o2o.common.AuditObject;

@Table(name = "t_local_auth")
public class LocalAuthE extends AuditObject {

    private String username;

    private byte[] password;

    private String userId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
