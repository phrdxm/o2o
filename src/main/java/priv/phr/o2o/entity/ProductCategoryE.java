package priv.phr.o2o.entity;

import javax.persistence.Table;

import priv.phr.o2o.common.AuditObject;

@Table(name = "t_product_category")
public class ProductCategoryE extends AuditObject {

    public ProductCategoryE(String shopId, String name) {
        this.shopId = shopId;
        this.name = name;
    }

    public ProductCategoryE() {
    }

    private String shopId;

    private String name;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
