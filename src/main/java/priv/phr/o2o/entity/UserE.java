package priv.phr.o2o.entity;

import javax.persistence.Table;

import priv.phr.o2o.common.AuditObject;
import priv.phr.o2o.common.enumeration.Gender;
import priv.phr.o2o.common.enumeration.Role;

@Table(name = "t_user")
public class UserE extends AuditObject {

    public static final UserE TOURIST;

    static {
        TOURIST = new UserE();
        TOURIST.setRole(Role.TOURIST);
    }

    private String nickname;

    private String avatorFileName;

    private String email;

    private Gender gender;

    private Boolean frozen;

    private Role role;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatorFileName() {
        return avatorFileName;
    }

    public void setAvatorFileName(String avatorFileName) {
        this.avatorFileName = avatorFileName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Boolean getFrozen() {
        return frozen;
    }

    public void setFrozen(Boolean frozen) {
        this.frozen = frozen;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
