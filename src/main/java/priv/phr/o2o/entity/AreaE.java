package priv.phr.o2o.entity;

import javax.persistence.Table;

import priv.phr.o2o.common.AuditObject;

@Table(name = "t_area")
public class AreaE extends AuditObject {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
