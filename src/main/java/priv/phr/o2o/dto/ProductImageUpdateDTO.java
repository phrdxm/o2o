package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ProductImageUpdateDTO {

    @Size(max = 255, message = DESCRIPTION_TOO_LONG)
    private String description;

    @NotNull(message = UUID_ERROR)
    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
