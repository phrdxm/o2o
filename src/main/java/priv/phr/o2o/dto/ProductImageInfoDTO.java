package priv.phr.o2o.dto;

import priv.phr.o2o.common.AuditObject;

public class ProductImageInfoDTO extends AuditObject {

    private String fileName;

    private String description;

    private String productId;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
