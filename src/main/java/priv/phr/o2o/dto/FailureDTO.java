package priv.phr.o2o.dto;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FailureDTO {

    @JsonProperty("failed")
    public boolean isFailed() {
        return true;
    }

    private final Set<String> messages = new HashSet<>();

    public FailureDTO() {
    }

    public String[] getMessages() {
        return messages.toArray(new String[0]);
    }

    public FailureDTO(String... messages) {
        this.messages.addAll(Arrays.asList(messages));
    }

    public FailureDTO(Throwable throwable) {
        this(throwable.getMessage());
    }

    public FailureDTO(Iterable<String> messages) {
        for (String message : messages) {
            this.messages.add(message);
        }
    }
}
