package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import java.math.BigDecimal;
import javax.validation.constraints.*;

import org.hibernate.validator.constraints.NotBlank;

public class ProductCreateDTO {

    @NotBlank(message = PRODUCT_NAME_BLANK)
    @Size(max = 32, message = PRODUCT_NAME_TOO_LONG)
    private String name;

    @Size(max = 255, message = DESCRIPTION_TOO_LONG)
    private String description;

    @Pattern(regexp = BASE64_REGEXP, message = IMAGE_FILE_ERROR)
    private String base64Image;

    @Min(value = MIN_PRODUCT_NUMBER, message = PRODUCT_NUMBER_NEGATIVE)
    private Integer number;

    @NotNull(message = NORMAL_PRICE_NULL)
    @Min(value = 0, message = NORMAL_PRICE_NEGATIVE)
    @Max(value = MAX_PRICE, message = PRICE_OUT_OF_RANGE)
    private BigDecimal normalPrice;

    @Min(value = 0, message = PROMOTION_PRICE_NEGATIVE)
    @Max(value = MAX_PRICE, message = PRICE_OUT_OF_RANGE)
    private BigDecimal promotionPrice;

    private Boolean available;

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String categoryId;

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    @NotNull(message = UUID_ERROR)
    private String shopId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public BigDecimal getNormalPrice() {
        return normalPrice;
    }

    public void setNormalPrice(BigDecimal normalPrice) {
        this.normalPrice = normalPrice;
    }

    public BigDecimal getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }
}
