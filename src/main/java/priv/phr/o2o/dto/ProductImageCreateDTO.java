package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ProductImageCreateDTO {

    @Size(max = 255, message = DESCRIPTION_TOO_LONG)
    private String description;

    @NotNull(message = IMAGE_NOT_NULL)
    @Pattern(regexp = BASE64_REGEXP, message = IMAGE_FILE_ERROR)
    private String base64Image;

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    @NotNull(message = UUID_ERROR)
    private String productId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
