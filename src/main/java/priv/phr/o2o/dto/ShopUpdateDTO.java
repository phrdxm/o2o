package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import priv.phr.o2o.common.annotation.BlankOrBase64;

public class ShopUpdateDTO {

    @NotNull(message = UUID_ERROR)
    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String id;

    @Size(max = 64, message = SHOP_NAME_TOO_LONG)
    private String name;

    @Size(max = 255, message = DESCRIPTION_TOO_LONG)
    private String description;

    @Size(max = 255, message = SHOP_ADDRESS_TOO_LONG)
    private String address;

    @Pattern(regexp = PHONE_REGEXP)
    private String phone;

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String areaId;

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String categoryId;

    @BlankOrBase64
    private String base64Image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }
}
