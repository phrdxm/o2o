package priv.phr.o2o.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.Constant;

public class LocalAuthDTO {

    @NotNull(message = Constant.USERNAME_NULL)
    @Pattern(regexp = Constant.USERNAME_PATTERN, message = Constant.USERNAME_INVALID)
    private String username;

    @NotNull(message = Constant.PWD_NULL)
    @Pattern(regexp = Constant.PWD_PATTERN, message = Constant.PWD_INVALID)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
