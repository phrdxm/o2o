package priv.phr.o2o.dto;

public class CaptchaInfoDTO {

    private String token;

    private String image;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
