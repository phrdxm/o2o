package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class HeadLineCreateDTO {

    @NotBlank(message = HEAD_LINE_TITLE_BLANK)
    @Size(max = 64, message = HEAD_LINE_TITLE_TOO_LONG)
    private String title;

    @NotBlank(message = HEAD_LINE_URL_BLANK)
    @Size(max = 255, message = HEAD_LINE_URL_TOO_LONG)
    private String url;

    @Pattern(regexp = BASE64_REGEXP, message = IMAGE_FILE_ERROR)
    private String base64Image;

    private Boolean available;

    private Integer priority;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
