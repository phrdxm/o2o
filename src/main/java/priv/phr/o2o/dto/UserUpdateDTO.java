package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.Size;

import priv.phr.o2o.common.annotation.BlankOrBase64;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Range;

public class UserUpdateDTO {

    @Size(max = 32, message = NICKNAME_TOO_LONG)
    private String nickname; // null and blank are valid

    @Email(message = EMAIL_PATTERN_ERROR)
    private String email; // null and "" are valid

    @Range(min = 0, max = 1, message = GENDER_CODE_ERROR)
    private Integer gender; // null, 0 and 1 are valid

    @BlankOrBase64
    private String base64Avator; // null and blank are valid

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBase64Avator() {
        return base64Avator;
    }

    public void setBase64Avator(String base64Avator) {
        this.base64Avator = base64Avator;
    }
}
