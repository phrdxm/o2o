package priv.phr.o2o.dto;

import javax.validation.constraints.Size;

import priv.phr.o2o.common.Constant;

public class ShopAdviceDTO {

    @Size(max = 255, message = Constant.ADVICE_TOO_LONG)
    private String advice;

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }
}
