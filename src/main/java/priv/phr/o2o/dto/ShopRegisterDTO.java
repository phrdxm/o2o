package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.entity.ShopE;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

public class ShopRegisterDTO {

    @NotBlank(message = SHOP_NAME_BLANK)
    @Size(max = 64, message = SHOP_NAME_TOO_LONG)
    private String name;

    @Size(max = 255, message = DESCRIPTION_TOO_LONG)
    private String description;

    @NotBlank(message = SHOP_ADDRESS_BLANK)
    @Size(max = 255, message = SHOP_ADDRESS_TOO_LONG)
    private String address;

    @Pattern(regexp = PHONE_REGEXP)
    private String phone;

    @NotNull(message = UUID_ERROR)
    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    private String areaId;

    @NotBlank(message = SHOP_CATEGORY_NAME_BLANK)
    private String categoryId;

    @Pattern(regexp = BASE64_REGEXP, message = IMAGE_FILE_ERROR)
    private String base64Image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public ShopE toShopE() {
        ShopE shopE = new ShopE();
        Util.copyProperties(this, shopE);
        shopE.setDescription(StringUtils.hasText(description) ? description : null);

        return shopE;
    }
}
