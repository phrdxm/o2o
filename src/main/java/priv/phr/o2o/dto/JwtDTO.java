package priv.phr.o2o.dto;

public class JwtDTO {

    private String jwt;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public JwtDTO(String jwt) {
        this.jwt = jwt;
    }

    public JwtDTO() {
    }
}
