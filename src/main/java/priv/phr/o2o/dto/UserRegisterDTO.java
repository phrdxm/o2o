package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Range;

public class UserRegisterDTO {

    @NotNull(message = USERNAME_NULL)
    @Pattern(regexp = USERNAME_PATTERN, message = USERNAME_INVALID)
    private String username;

    @NotNull(message = PWD_NULL)
    @Pattern(regexp = PWD_PATTERN, message = PWD_INVALID)
    private String password;

    @Size(max = 32, message = NICKNAME_TOO_LONG)
    private String nickname; // null and blank are valid

    @Email(message = EMAIL_PATTERN_ERROR)
    private String email; // null and "" are valid

    @NotNull(message = GENDER_CODE_ERROR)
    @Range(min = 0, max = 1, message = GENDER_CODE_ERROR)
    private Integer gender;

    @Pattern(regexp = BASE64_REGEXP, message = IMAGE_FILE_ERROR)
    private String base64Avator; // null is valid

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBase64Avator() {
        return base64Avator;
    }

    public void setBase64Avator(String base64Avator) {
        this.base64Avator = base64Avator;
    }
}
