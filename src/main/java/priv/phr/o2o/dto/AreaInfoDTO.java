package priv.phr.o2o.dto;

import priv.phr.o2o.common.AuditObject;

public class AreaInfoDTO extends AuditObject {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
