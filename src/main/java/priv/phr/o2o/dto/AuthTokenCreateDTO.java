package priv.phr.o2o.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.Constant;

public class AuthTokenCreateDTO {

    @NotNull(message = Constant.PWD_NULL)
    @Pattern(regexp = Constant.PWD_PATTERN, message = Constant.PWD_INVALID)
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
