package priv.phr.o2o.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import priv.phr.o2o.common.Constant;

public class UpdatePasswordDTO {

    @NotNull(message = Constant.PWD_NULL)
    @Pattern(regexp = Constant.PWD_PATTERN, message = Constant.PWD_INVALID)
    private String originalPassword;

    @NotNull(message = Constant.PWD_NULL)
    @Pattern(regexp = Constant.PWD_PATTERN, message = Constant.PWD_INVALID)
    private String newPassword;

    public String getOriginalPassword() {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword) {
        this.originalPassword = originalPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
