package priv.phr.o2o.dto;

import static priv.phr.o2o.common.Constant.UUID_ERROR;
import static priv.phr.o2o.common.Constant.UUID_REGEXP;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class ProductCategoryUpdateDTO {

    @Pattern(regexp = UUID_REGEXP, message = UUID_ERROR)
    @NotNull(message = UUID_ERROR)
    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
