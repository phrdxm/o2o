package priv.phr.o2o.dto;

import priv.phr.o2o.common.AuditObject;

public class UserInfoDTO extends AuditObject {

    private String nickname;

    private String avatorFileName;

    private String email;

    private Integer gender;

    private Boolean frozen;

    private Integer role;

    private String jwt;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatorFileName() {
        return avatorFileName;
    }

    public void setAvatorFileName(String avatorFileName) {
        this.avatorFileName = avatorFileName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Boolean getFrozen() {
        return frozen;
    }

    public void setFrozen(Boolean frozen) {
        this.frozen = frozen;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
