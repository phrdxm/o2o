package priv.phr.o2o.common.exception;

public class ResourceNotFoundException extends RuntimeException {

    private static final String RESOURCE_NOT_FOUND = "资源未找到";

    protected ResourceNotFoundException(String message) {
        super(message);
    }

    public ResourceNotFoundException() {
        super(RESOURCE_NOT_FOUND);
    }

    public ResourceNotFoundException(Throwable cause) {
        super(RESOURCE_NOT_FOUND, cause);
    }
}
