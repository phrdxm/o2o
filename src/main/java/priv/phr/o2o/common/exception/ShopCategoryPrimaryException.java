package priv.phr.o2o.common.exception;

public class ShopCategoryPrimaryException extends RuntimeException {

    private static final String SHOP_CATEGORY_PRIMARY = "店铺不可以归到一级分类下";

    public ShopCategoryPrimaryException() {
        super(SHOP_CATEGORY_PRIMARY);
    }
}
