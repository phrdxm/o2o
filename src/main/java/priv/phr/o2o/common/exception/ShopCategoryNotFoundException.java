package priv.phr.o2o.common.exception;

public class ShopCategoryNotFoundException extends ResourceNotFoundException {

    public static final String SHOP_CATEGORY_NOT_FOUND = "店铺分类未找到";

    public ShopCategoryNotFoundException() {
        super(SHOP_CATEGORY_NOT_FOUND);
    }
}
