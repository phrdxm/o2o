package priv.phr.o2o.common.exception;

public class ShopNamesakeException extends NamesakeException {

    public static final String SHOP_NAMESAKE = "店铺名重复";

    public ShopNamesakeException() {
        super(SHOP_NAMESAKE);
    }
}
