package priv.phr.o2o.common.exception;

public class FileLoadException extends RuntimeException {

    protected FileLoadException(String message) {
        super(message);
    }

    protected FileLoadException(String message, Throwable cause) {
        super(message, cause);
    }
}
