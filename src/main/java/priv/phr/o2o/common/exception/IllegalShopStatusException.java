package priv.phr.o2o.common.exception;

public class IllegalShopStatusException extends IllegalStateException {

    public static final String ILLEGAL_SHOP_STATUS = "店铺状态错误";

    public IllegalShopStatusException() {
        super(ILLEGAL_SHOP_STATUS);
    }
}
