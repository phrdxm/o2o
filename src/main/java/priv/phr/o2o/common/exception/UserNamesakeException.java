package priv.phr.o2o.common.exception;

public class UserNamesakeException extends NamesakeException {

    public static final String USER_NAMESAKE = "用户名重复";

    public UserNamesakeException() {
        super(USER_NAMESAKE);
    }
}
