package priv.phr.o2o.common.exception;

public class ProductCategoryNotFoundException extends ResourceNotFoundException {

    public static final String PRODUCT_CATEGORY_NOT_FOUND = "物品类别未找到";

    public ProductCategoryNotFoundException() {
        super(PRODUCT_CATEGORY_NOT_FOUND);
    }
}
