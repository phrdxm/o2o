package priv.phr.o2o.common.exception;

public class MessageTargetNotFoundException extends ResourceNotFoundException {

    public static final String MESSAGE_TARGET_NOT_FOUND = "目标用户未找到";

    public MessageTargetNotFoundException() {
        super(MESSAGE_TARGET_NOT_FOUND);
    }
}
