package priv.phr.o2o.common.exception;

import priv.phr.o2o.entity.UserE;

public class AccountFrozenException extends RuntimeException {

    private UserE frozenUser;

    public UserE getFrozenUser() {
        return frozenUser;
    }

    public static final String ACCOUNT_FROZEN = "用户已冻结";

    public AccountFrozenException(UserE frozenUser) {
        super(ACCOUNT_FROZEN);
        this.frozenUser = frozenUser;
    }
}
