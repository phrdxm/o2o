package priv.phr.o2o.common.exception;

public class ProductNotFoundException extends ResourceNotFoundException {

    public static final String PRODUCT_NOT_FOUND = "物品未找到";

    public ProductNotFoundException() {
        super(PRODUCT_NOT_FOUND);
    }
}
