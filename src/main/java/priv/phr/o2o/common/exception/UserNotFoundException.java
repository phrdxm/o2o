package priv.phr.o2o.common.exception;

public class UserNotFoundException extends ResourceNotFoundException {

    public static final String USER_NOT_FOUND = "该用户不存在";

    public UserNotFoundException() {
        super(USER_NOT_FOUND);
    }
}
