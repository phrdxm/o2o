package priv.phr.o2o.common.exception;

public class ReadResourceException extends RuntimeException {

    public ReadResourceException() {
    }

    public ReadResourceException(String message) {
        super(message);
    }

    public ReadResourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReadResourceException(Throwable cause) {
        super(cause);
    }
}
