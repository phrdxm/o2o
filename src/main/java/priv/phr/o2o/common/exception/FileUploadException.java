package priv.phr.o2o.common.exception;

public class FileUploadException extends FileLoadException {

    private static final String FILE_UPLOAD_FAILED = "文件上传失败";

    public FileUploadException() {
        super(FILE_UPLOAD_FAILED);
    }

    public FileUploadException(Throwable cause) {
        super(FILE_UPLOAD_FAILED, cause);
    }
}
