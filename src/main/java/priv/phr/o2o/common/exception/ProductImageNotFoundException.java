package priv.phr.o2o.common.exception;

public class ProductImageNotFoundException extends ResourceNotFoundException {

    public static final String PRODUCT_IMAGE_NOT_FOUND = "详情图未找到";

    public ProductImageNotFoundException() {
        super(PRODUCT_IMAGE_NOT_FOUND);
    }
}
