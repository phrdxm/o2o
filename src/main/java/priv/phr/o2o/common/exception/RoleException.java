package priv.phr.o2o.common.exception;

import priv.phr.o2o.common.enumeration.Role;

public class RoleException extends RuntimeException {

    private final Role requireRole;

    private final Role actualRole;

    public Role getRequireRole() {
        return requireRole;
    }

    public Role getActualRole() {
        return actualRole;
    }

    public RoleException(Role requireRole, Role actualRole, String message) {
        super(message);
        this.requireRole = requireRole;
        this.actualRole = actualRole;
    }
}
