package priv.phr.o2o.common.exception;

public class NamesakeException extends RuntimeException {

    public NamesakeException() {
    }

    public NamesakeException(String message) {
        super(message);
    }
}
