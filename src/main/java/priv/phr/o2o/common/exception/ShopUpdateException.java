package priv.phr.o2o.common.exception;

public class ShopUpdateException extends RuntimeException {

    public ShopUpdateException() {
    }

    public ShopUpdateException(String message) {
        super(message);
    }

    public ShopUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShopUpdateException(Throwable cause) {
        super(cause);
    }
}
