package priv.phr.o2o.common.exception;

public class CaptchaException extends RuntimeException {

    private String captchaHeader;

    public String getCaptchaHeader() {
        return captchaHeader;
    }

    public void setCaptchaHeader(String captchaHeader) {
        this.captchaHeader = captchaHeader;
    }

    public CaptchaException(String message, String captchaHeader) {
        super(message);
        this.captchaHeader = captchaHeader;
    }
}
