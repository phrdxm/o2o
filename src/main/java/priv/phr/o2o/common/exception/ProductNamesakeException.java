package priv.phr.o2o.common.exception;

public class ProductNamesakeException extends NamesakeException {

    public static final String PRODUCT_NAMESAKE = "物品名重复";

    public ProductNamesakeException() {
        super(PRODUCT_NAMESAKE);
    }
}
