package priv.phr.o2o.common.exception;

public class FileDownloadException extends FileLoadException {

    private static final String FILE_DOWNLOAD_FAILED = "下载文件失败";

    public FileDownloadException() {
        super(FILE_DOWNLOAD_FAILED);
    }

    public FileDownloadException(Throwable cause) {
        super(FILE_DOWNLOAD_FAILED, cause);
    }
}
