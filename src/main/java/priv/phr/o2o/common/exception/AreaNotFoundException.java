package priv.phr.o2o.common.exception;

public class AreaNotFoundException extends ResourceNotFoundException {

    public static final String AREA_NOT_FOUND = "区域未找到";

    public AreaNotFoundException() {
        super(AREA_NOT_FOUND);
    }
}
