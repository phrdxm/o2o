package priv.phr.o2o.common.exception;

public class ProductCategoryNamesakeException extends NamesakeException {

    public static final String PRODUCT_CATEGORY_NAMESAKE = "物品分类名重复";

    public ProductCategoryNamesakeException() {
        super(PRODUCT_CATEGORY_NAMESAKE);
    }
}
