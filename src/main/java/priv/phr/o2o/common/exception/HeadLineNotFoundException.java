package priv.phr.o2o.common.exception;

public class HeadLineNotFoundException extends ResourceNotFoundException {

    public static final String HEAD_LINE_NOT_FOUND = "头条未找到";

    public HeadLineNotFoundException() {
        super(HEAD_LINE_NOT_FOUND);
    }
}
