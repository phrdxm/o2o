package priv.phr.o2o.common.exception;

public class ShopNotFoundException extends ResourceNotFoundException {

    public static final String SHOP_NOT_FOUND = "店铺未找到";

    public ShopNotFoundException() {
        super(SHOP_NOT_FOUND);
    }
}
