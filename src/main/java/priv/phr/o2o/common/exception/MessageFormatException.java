package priv.phr.o2o.common.exception;

public class MessageFormatException extends RuntimeException {

    public static final String JSON_PARSE_ERROR = "消息格式错误";

    public static final String MESSAGE_TOO_LONG = "消息过长，不能超过255个字符";

    public MessageFormatException(String message) {
        super(message);
    }

    public MessageFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
