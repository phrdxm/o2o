package priv.phr.o2o.common.exception;

public class UploadFileNotImageException extends RuntimeException {

    private static final String FILE_NOT_IMAGE = "上传文件类型错误，请上传.jpg，.jpeg或.png格式的文件";

    public UploadFileNotImageException() {
        super(FILE_NOT_IMAGE);
    }
}
