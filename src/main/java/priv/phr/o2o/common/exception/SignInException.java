package priv.phr.o2o.common.exception;

public class SignInException extends RuntimeException {

    private final String username;

    public String getUsername() {
        return username;
    }

    public SignInException(String message, String username) {
        super(message);
        this.username = username;
    }
}
