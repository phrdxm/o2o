package priv.phr.o2o.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.validator.BlankOrBase64Validator;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BlankOrBase64Validator.class)
public @interface BlankOrBase64 {

    String message() default Constant.IMAGE_FILE_ERROR;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
