package priv.phr.o2o.common.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DynamicDataSourceHolder {

    private static final Logger log = LoggerFactory.getLogger(DynamicDataSourceHolder.class);

    public static enum DBType {
        DB_MASTER("master"),
        DB_SLAVE("slave");

        private final String key;

        public String getKey() {
            return key;
        }

        DBType(String key) {
            this.key = key;
        }
    }

    private static final ThreadLocal<DBType> contextHolder = new ThreadLocal<>();

    public static DBType getDBType() {
        DBType dbType = contextHolder.get();
        if (dbType == null) {
            return DBType.DB_MASTER;
        }

        return dbType;
    }

    public static void setDBType(DBType dbType) {
        if (dbType != null) {
            log.info("切换数据库类型至 {}", dbType.key); // debug
        }
        contextHolder.set(dbType);
    }

    public static void clearDBType() {
        contextHolder.remove();
    }
}
