package priv.phr.o2o.common.config;

import java.io.InputStream;

import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.service.ImageService;
import io.minio.MinioClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "minio")
@Profile({"prod", "dev"})
public class MinioProperties implements InitializingBean {

    private static final Logger log = LoggerFactory.getLogger(MinioProperties.class);

    private static final String[] shopCategoryImageNames = new String[]{
            "d141020a-eec2-4732-86e0-fbce463aeff5",
            "b9dbd863-b214-4bda-96ba-2ffbb1ce7f9a",
            "55585358-0498-447d-9213-fcab32aef974",
            "a8486838-fd92-4129-aaab-f9858d1491cc",
            "ec93c1bf-6184-4f6b-be56-af2458c24ab5",
            "08e3e83e-12e2-4bdf-8bb2-23e193c03e68"
    };

    @Autowired
    private ImageService imageService;

    private String url;

    private String accessKey;

    private String secretKey;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    private void initBuckets(MinioClient client) throws Exception {
        for (ImageCategory imageCategory : ImageCategory.values()) {
            if (client.bucketExists(imageCategory.getName())) {
                log.info("桶 {} 已存在", imageCategory.getName());
            } else {
                log.info("桶 {} 不存在，创建", imageCategory.getName());
                client.makeBucket(imageCategory.getName());
                log.info("创建完成");
            }
        }
    }

    private void initPrimaryShopCategoryImages(MinioClient client) {
        for (String name : shopCategoryImageNames) {
            try (InputStream inputStream = client.getObject(ImageCategory.SHOP_CATEGORY.getName(), name)) {
            } catch (Exception e) {
                if (imageService.isResourceNotFoundException(e)) {
                    log.info("店铺分类图 {} 不存在，开始上传", name);
                    try (InputStream i = new ClassPathResource(name).getInputStream()) {
                        client.putObject(ImageCategory.SHOP_CATEGORY.getName(), name, i, "image/png");
                        log.info("上传成功");
                    } catch (Exception err) {
                        log.error("上传失败：{}", err.getMessage());
                    }
                } else {
                    log.error("检查店铺分类图 {} 是否存在的过程中出现错误：{}", name, e.getMessage());
                }
                continue;
            }
            log.info("店铺分类图 {} 已存在", name);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        MinioClient client = new MinioClient(url, accessKey, secretKey);
        initBuckets(client);
        initPrimaryShopCategoryImages(client);
    }
}
