package priv.phr.o2o.common.config;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;

@Configuration
@Profile("prod")
public class DynamicDataSourceConfig {

    @Bean
    @ConfigurationProperties("master.datasource")
    public DataSource masterDataSource() {
        return new DruidDataSource();
    }

    @Bean
    @ConfigurationProperties("slave.datasource")
    public DataSource slaveDataSource() {
        return new DruidDataSource();
    }

    @Bean
    @DependsOn({"masterDataSource", "slaveDataSource"})
    @Primary
    public DynamicDataSource dynamicDataSource(@Qualifier("masterDataSource") DataSource masterDataSource,
                                               @Qualifier("slaveDataSource") DataSource slaveDataSource) {
        DynamicDataSource dynamicDataSource = new DynamicDataSource();
        dynamicDataSource.setDefaultTargetDataSource(masterDataSource);

        Map<Object, Object> dataSourceMap = new HashMap<>();
        dataSourceMap.put(DynamicDataSourceHolder.DBType.DB_MASTER.getKey(), masterDataSource);
        dataSourceMap.put(DynamicDataSourceHolder.DBType.DB_SLAVE.getKey(), slaveDataSource);

        dynamicDataSource.setTargetDataSources(dataSourceMap);
        return dynamicDataSource;
    }
}
