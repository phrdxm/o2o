package priv.phr.o2o.common.config;

import java.util.Collections;
import java.util.Properties;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CaptchaConfig {

    public static final String CAPTCHA_CHARS = "345789" +
            "acdefghijkmnpqrstuvwxy" +
            "ABCDEFGHIJKMNPQRSTUVWXY";

    public static final Integer CAPTCHA_LENGTH = 4;

    @Bean
    public DefaultKaptcha defaultKaptcha() {
        DefaultKaptcha captchaProducer = new DefaultKaptcha();
        Properties properties = new Properties();
        properties.setProperty("kaptcha.border", "no");
        properties.setProperty("kaptcha.textproducer.font.color", "red");
        properties.setProperty("kaptcha.image.width", "135");
        properties.setProperty("kaptcha.image.height", "50");
        properties.setProperty("kaptcha.textproducer.font.size", "36");
        properties.setProperty("kaptcha.textproducer.char.string", CAPTCHA_CHARS);
        properties.setProperty("kaptcha.noise.color", "black");
        properties.setProperty("kaptcha.textproducer.char.length", CAPTCHA_LENGTH.toString());
        properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
        Config config = new Config(properties);
        captchaProducer.setConfig(config);
        return captchaProducer;
    }
}
