package priv.phr.o2o.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.annotation.BlankOrBase64;
import org.springframework.util.StringUtils;

public class BlankOrBase64Validator implements ConstraintValidator<BlankOrBase64, String> {

    @Override
    public void initialize(BlankOrBase64 blankOrBase64) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return !StringUtils.hasText(s) || Constant.BASE64_PATTERN.matcher(s).matches();
    }
}
