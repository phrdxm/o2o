package priv.phr.o2o.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import priv.phr.o2o.common.annotation.BucketExist;
import priv.phr.o2o.common.enumeration.ImageCategory;

public class BucketExistValidator implements ConstraintValidator<BucketExist, String> {

    @Override
    public void initialize(BucketExist bucketExist) {
    }

    @Override
    public boolean isValid(String bucketName, ConstraintValidatorContext constraintValidatorContext) {
        return ImageCategory.parse(bucketName) != null;
    }
}
