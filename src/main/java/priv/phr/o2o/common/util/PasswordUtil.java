package priv.phr.o2o.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

public class PasswordUtil {

    private PasswordUtil() {
        throw new AssertionError("non instantiatable");
    }

    private static final int SALT_LENGTH = 6;

    private static final String ALGORITHM_NAME = "MD5";

    private static byte[] md5Encode(byte[] plainBytes) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(ALGORITHM_NAME);
        } catch (NoSuchAlgorithmException e) {
            AssertionUtil.fail("不支持加密算法 " + ALGORITHM_NAME);
        }
        messageDigest.update(plainBytes);
        return messageDigest.digest();
    }

    private static byte[] mix(byte[] plainTextBytes, byte[] salt) {
        byte[] plainBytes = new byte[plainTextBytes.length + SALT_LENGTH];
        int mixLength = Math.min(plainTextBytes.length, SALT_LENGTH);
        for (int i = 0; i < mixLength; i++) {
            plainBytes[i * 2] = plainTextBytes[i];
            plainBytes[i * 2 + 1] = salt[i];
        }
        if (plainTextBytes.length > SALT_LENGTH) {
            System.arraycopy(plainTextBytes, mixLength, plainBytes, mixLength * 2,
                    plainTextBytes.length - SALT_LENGTH);
        } else if (SALT_LENGTH > plainTextBytes.length) {
            System.arraycopy(salt, plainTextBytes.length, plainBytes, plainTextBytes.length * 2,
                    SALT_LENGTH - plainTextBytes.length);
        }

        return plainBytes;
    }

    private static byte[] getSalt(byte[] encoded) {
        byte[] salt = new byte[SALT_LENGTH];
        for (int i = 0; i < SALT_LENGTH; i++) {
            salt[i] = encoded[i * 2 + 1];
        }

        return salt;
    }

    /**
     * 加密明文密码
     *
     * @param plainText 明文密码
     * @return 加密后的字节数组
     * @throws IllegalArgumentException 如果plainText为null
     */
    public static byte[] encode(String plainText) {
        AssertionUtil.notNull(plainText, "明文密码不能为null");
        byte[] salt = new byte[SALT_LENGTH];
        new Random().nextBytes(salt);

        return mix(md5Encode(mix(plainText.getBytes(), salt)), salt);
    }

    /**
     * 校验比对明文密码和加密串
     *
     * @param plainText 明文密码
     * @param encoded   加密串
     * @return 匹配返回true，否则返回false
     * @throws IllegalArgumentException 如果plainText为null
     */
    public static boolean check(String plainText, byte[] encoded) {
        AssertionUtil.notNull(plainText, "明文密码不能为null");
        if (encoded == null || encoded.length != SALT_LENGTH + 16) {
            return false;
        }

        byte[] salt = getSalt(encoded);
        return Arrays.equals(encoded, mix(md5Encode(mix(plainText.getBytes(), salt)), salt));
    }
}
