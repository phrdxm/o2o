package priv.phr.o2o.common.util;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

public class Util {

    private Util() {
        throw new AssertionError("non instantiatable");
    }

    public static void closeStreams(Closeable... streams) throws IOException {
        for (Closeable stream : streams) {
            if (stream != null) {
                stream.close();
            }
        }
    }

    public static boolean regexpMatches(Pattern regexPattern, String str) {
        if (regexPattern == null || str == null) {
            return false;
        }

        return regexPattern.matcher(str).matches();
    }

    public static void copyProperties(Object src, Object dest) {
        if (src == null || dest == null) {
            return;
        }
        ReflectionUtils.doWithFields(src.getClass(), field -> {
            Field destField = ReflectionUtils.findField(dest.getClass(), field.getName());
            if (destField != null && destField.getType().isAssignableFrom(field.getType())) {
                destField.setAccessible(true);
                field.setAccessible(true);
                ReflectionUtils.setField(destField, dest, ReflectionUtils.getField(field, src));
            }
        });
    }

    public static <TSrc, TDest> TDest convert(TSrc src, Class<TDest> destClass) {
        if (src == null || destClass == null) {
            return null;
        }

        TDest destObj;
        try {
            destObj = destClass.newInstance();
        } catch (ReflectiveOperationException e) {
            throw new IllegalStateException("Util.convert 目标类型不能通过默认方式构造", e);
        }
        copyProperties(src, destObj);

        return destObj;
    }

    public static <TSrc, TDest> List<TDest> convertList(Collection<TSrc> srcCollection, Class<TDest> destClass) {
        if (CollectionUtils.isEmpty(srcCollection) || destClass == null) {
            return Collections.emptyList();
        }

        List<TDest> destList = new ArrayList<>(srcCollection.size());
        for (TSrc srcObj : srcCollection) {
            TDest destObj;
            try {
                destObj = destClass.newInstance();
            } catch (ReflectiveOperationException e) {
                throw new IllegalStateException("Util.convertList 目标类型不能通过默认方式构造", e);
            }
            copyProperties(srcObj, destObj);
            destList.add(destObj);
        }

        return destList;
    }

    public static boolean isValueUpdatable(Object originalValue, Object newValue) {
        return newValue != null && !Objects.equals(originalValue, newValue);
    }

    public static boolean updateProperties(Object originalObj, Object newObj, String... fieldNames) {
        boolean updated = false;
        if (originalObj == null || newObj == null) {
            return updated;
        }

        for (String fieldName : fieldNames) {
            Field originalField = ReflectionUtils.findField(originalObj.getClass(), fieldName);
            Field newField = ReflectionUtils.findField(newObj.getClass(), fieldName);
            if (originalField == null || newField == null || !originalField.getType().isAssignableFrom(newField.getType())) {
                continue;
            }

            originalField.setAccessible(true);
            newField.setAccessible(true);

            Object originalValue = ReflectionUtils.getField(originalField, originalObj);
            Object newValue = ReflectionUtils.getField(newField, newObj);

            if (isValueUpdatable(originalValue, newValue)) {
                ReflectionUtils.setField(originalField, originalObj, newValue);
                updated = true;
            }
        }

        return updated;
    }
}
