package priv.phr.o2o.common.util;

import javax.servlet.http.HttpServletRequest;

import priv.phr.o2o.entity.UserE;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class ContextUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static ApplicationContext getContext() {
        return context;
    }

    public static <T> T getBean(Class<T> requiredType) {
        return context.getBean(requiredType);
    }

    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    private static final ThreadLocal<UserE> currentUser = ThreadLocal.withInitial(() -> null);

    /**
     * 获取当前会话的用户
     */
    public static UserE getCurrentUser() {
        return currentUser.get();
    }

    /**
     * 设置当前会话的用户
     */
    public static void setCurrentUser(UserE user) {
        currentUser.set(user);
    }

    public static String getCurrentUserId() {
        return getCurrentUser().getId();
    }
}
