package priv.phr.o2o.common.util;

import java.util.Date;
import java.util.UUID;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.exception.SessionException;
import priv.phr.o2o.entity.UserE;
import priv.phr.o2o.mapper.UserMapper;
import io.jsonwebtoken.*;
import org.apache.commons.codec.binary.Base64;

public class JWTUtil {

    private JWTUtil() {
        throw new AssertionError("non instantiatable");
    }

    private static final String JWT_SECRET = "47f90702-f9da-47df-a483-5621a1144008";

    /**
     * JWT过期时间，默认7天
     */
    private static final Long TTL_MILLIS = 604800000L;

    /**
     * 生成签发JWT需要的签名密钥
     */
    private static SecretKey generalKey() {
        byte[] encodedKey = Base64.decodeBase64(JWT_SECRET);
        return new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
    }

    /**
     * 根据用户信息创建JWT
     */
    public static String createJWT(UserE user) {
        Date now = new Date();

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(now)
                .setSubject(user.getId())
                .signWith(SignatureAlgorithm.HS256, generalKey())
                .setExpiration(new Date(now.getTime() + TTL_MILLIS))
                .compact();
    }

    /**
     * 将JWT解析为用户信息
     */
    public static UserE parseJWT(String jwt) {
        AssertionUtil.matchesPattern(Constant.JWT_PATTERN, jwt, "JWT格式错误");

        try {
            Claims claims = Jwts.parser().setSigningKey(generalKey()).parseClaimsJws(jwt).getBody();

            UserE user = ContextUtil.getBean(UserMapper.class).selectByPrimaryKey(claims.getSubject());
            if (user == null) {
                throw new SessionException(Constant.SESSION_DATA_MISSING);
            }
            return user;
        } catch (ExpiredJwtException e) {
            throw new SessionException(Constant.JWT_EXPIRED, e);
        } catch (JwtException e) {
            throw new SessionException(Constant.JWT_PARSE_ERROR, e);
        }
    }
}
