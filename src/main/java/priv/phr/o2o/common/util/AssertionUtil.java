package priv.phr.o2o.common.util;

import java.util.Objects;
import java.util.regex.Pattern;

import priv.phr.o2o.common.enumeration.ImageCategory;
import priv.phr.o2o.service.ImageService;
import org.springframework.util.StringUtils;

public class AssertionUtil {

    public abstract static class FailureCallback {

        private Object[] callbackArgs;

        public FailureCallback(Object... callbackArgs) {
            this.callbackArgs = callbackArgs;
        }

        protected abstract void doWhenFailed(Object[] callbackArgs);
    }

    public static class MinioRollback extends FailureCallback {

        public MinioRollback(ImageCategory imageCategory, String fileName) {
            super(imageCategory, fileName);
        }

        @Override
        protected void doWhenFailed(Object[] callbackArgs) {
            ContextUtil.getBean(ImageService.class).removeImage((ImageCategory) callbackArgs[0], (String) callbackArgs[1]);
        }
    }

    private AssertionUtil() {
        throw new AssertionError("non instantiatable");
    }

    public static void fail(String message) {
        throw new AssertionError(message);
    }

    public static void notNull(Object object, String message) {
        notNull(object, message, null);
    }

    public static void notNull(Object object, String message, FailureCallback callback) {
        if (object == null) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void isTrue(Boolean bool, String message) {
        isTrue(bool, message, null);
    }

    public static void isTrue(Boolean bool, String message, FailureCallback callback) {
        if (bool == null || !bool) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void equals(Object object1, Object object2, String message) {
        equals(object1, object2, message, null);
    }

    public static void equals(Object object1, Object object2, String message, FailureCallback callback) {
        if (!Objects.equals(object1, object2)) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void notEquals(Object object1, Object object2, String message) {
        notEquals(object1, object2, message, null);
    }

    public static void notEquals(Object object1, Object object2, String message, FailureCallback callback) {
        if (Objects.equals(object1, object2)) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void notBlank(String str, String message) {
        notBlank(str, message, null);
    }

    public static void notBlank(String str, String message, FailureCallback callback) {
        if (!StringUtils.hasText(str)) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void matchesPattern(Pattern regPattern, String str, String message) {
        matchesPattern(regPattern, str, message, null);
    }

    public static void matchesPattern(Pattern regPattern, String str, String message, FailureCallback callback) {
        if (!Util.regexpMatches(regPattern, str)) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }

    public static void max(int value, int max, String message) {
        max(value, max, message, null);
    }

    public static void max(int value, int max, String message, FailureCallback callback) {
        if (value > max) {
            if (callback != null) {
                callback.doWhenFailed(callback.callbackArgs);
            }
            throw new AssertionError(message);
        }
    }
}
