package priv.phr.o2o.common;

import java.util.regex.Pattern;

import priv.phr.o2o.common.config.CaptchaConfig;

public class Constant {

    public static final String UUID_ERROR = "id格式错误";

    public static final String UUID_REGEXP = "[0-9a-f]{8}-(?:[0-9a-f]{4}-){3}[0-9a-f]{12}";

    public static final Pattern UUID_PATTERN = Pattern.compile(UUID_REGEXP);

    public static final String BASE64_REGEXP = "[a-zA-Z+/\\d]+={0,2}";

    public static final Pattern BASE64_PATTERN = Pattern.compile(BASE64_REGEXP);

    public static final String JWT_REGEXP = "(?:" + BASE64_REGEXP + "\\.){2}.+";

    public static final Pattern JWT_PATTERN = Pattern.compile(JWT_REGEXP);

    public static final String AUTHORIZATION_PREFIX = "Bearer ";

    public static final Pattern AUTHORIZATION_PATTERN = Pattern.compile(AUTHORIZATION_PREFIX + JWT_REGEXP);

    public static final String PWD_PATTERN = "(?:\\w|[~`!@#$%^&*?,:;()\\-.+={}\\[\\]]){6,}";

    public static final String USERNAME_PATTERN = "\\w{6,16}";

    public static final String SERVER_INNER_DATA_ERROR = "服务器内部数据错误";

    public static final String IMAGE_FILE_NAME_ERROR = "文件名错误";

    public static final String UNKNOWN_BUCKET_ERROR = "路径不存在";

    public static final String JWT_PARSE_ERROR = "会话错误";

    public static final String SESSION_DATA_MISSING = "会话错误";

    public static final String JWT_EXPIRED = "会话超时";

    public static final String JWT_NOT_FOUND = "请先登录";

    public static final String UNAUTH = "权限不足";

    public static final String USERNAME_OR_PWD_ERROR = "用户名或密码错误";

    public static final String PWD_ERROR = "密码错误";

    public static final String USERNAME_NULL = "用户名不能为空";

    public static final String PWD_NULL = "密码不能为空";

    public static final String USERNAME_INVALID = "用户名为6到16个字符，只能包含字母数字下划线";

    public static final String PWD_INVALID = "密码长度必须6位以上，只能包含字母数字下划线以及特殊字符~`!@#$%^&*?,:;()-.+={}[]";

    public static final String GENDER_CODE_ERROR = "性别代码错误";

    public static final String NICKNAME_TOO_LONG = "用户昵称过长，最大32个字符";

    public static final String EMAIL_PATTERN_ERROR = "邮箱格式错误";

    public static final String IMAGE_URI_PREFIX = "image";

    public static final String IMAGE_FILE_ERROR = "图像文件损坏";

    public static final String INSERT_ROW_ERROR = "保存数据错误";

    public static final String UPDATE_ROW_ERROR = "更新数据错误";

    public static final String DELETE_ROW_ERROR = "删除数据错误";

    public static final String SHOP_NAME_TOO_LONG = "店铺名称过长";

    public static final String SHOP_NAME_BLANK = "店铺名称不能为空";

    public static final String DESCRIPTION_TOO_LONG = "描述过长";

    public static final String SHOP_ADDRESS_TOO_LONG = "店铺地址过长";

    public static final String SHOP_ADDRESS_BLANK = "店铺地址不能为空";

    public static final String PHONE_REGEXP = "(?:13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}";

    public static final String SHOP_CATEGORY_NAME_BLANK = "店铺所属分类不能为空";

    public static final String UPLOAD_FILE_TOO_LARGE = "上传文件过大，不能超过1MB";

    public static final String CAPTCHA_HEADER_NOT_FOUND = "请先填写验证码";

    public static final String CAPTCHA_HEADER_ERROR = "请先填写验证码";

    public static final String CAPTCHA_EXPIRED = "验证码已过期，点击图片刷新";

    public static final String CAPTCHA_ERROR = "验证码错误";

    public static final String CAPTCHA_REGEXP = "[" + CaptchaConfig.CAPTCHA_CHARS + "]{" + CaptchaConfig.CAPTCHA_LENGTH + "}";

    public static final Pattern CAPTCHA_PATTERN = Pattern.compile(CAPTCHA_REGEXP);

    public static final String AUTH_TOKEN_ERROR = "认证失败";

    public static final String NORMAL_PRICE_NEGATIVE = "原价不能为负";

    public static final String PROMOTION_PRICE_NEGATIVE = "现价不能为负";

    public static final String PRODUCT_NAME_BLANK = "物品名称不能为空";

    public static final String PRODUCT_NAME_TOO_LONG = "物品名称过长";

    public static final int MAX_PRICE = 99999;

    public static final int MIN_PRODUCT_NUMBER = 1;

    public static final String PRICE_OUT_OF_RANGE = "价格上限" + MAX_PRICE;

    public static final String PRODUCT_NUMBER_NEGATIVE = "物品数量至少" + MIN_PRODUCT_NUMBER + "个";

    public static final String NORMAL_PRICE_NULL = "原价不能为空";

    public static final String IMAGE_NOT_NULL = "图片不能为空";

    public static final String IMAGE_URI_ERROR = "图片URI格式错误";

    public static final String ORIGINAL_PWD_ERROR = "旧密码错误";

    public static final String ADVICE_TOO_LONG = "描述过长";

    public static final String HEAD_LINE_TITLE_BLANK = "头条标题不能为空";

    public static final String HEAD_LINE_TITLE_TOO_LONG = "头条标题过长，不能超过64个字符";

    public static final String HEAD_LINE_URL_BLANK = "头条链接不能为空";

    public static final String HEAD_LINE_URL_TOO_LONG = "头条链接过长，不能超过255个字符";
}
