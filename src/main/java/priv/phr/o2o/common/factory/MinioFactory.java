package priv.phr.o2o.common.factory;

import priv.phr.o2o.common.config.MinioProperties;
import priv.phr.o2o.common.util.ContextUtil;
import io.minio.MinioClient;
import io.minio.errors.MinioException;

public class MinioFactory {

    private MinioFactory() {
        throw new AssertionError("non instantiatable");
    }

    public static MinioClient createClient() throws MinioException {
        MinioProperties minioProperties = ContextUtil.getBean(MinioProperties.class);
        return new MinioClient(minioProperties.getUrl(), minioProperties.getAccessKey(), minioProperties.getSecretKey());
    }
}
