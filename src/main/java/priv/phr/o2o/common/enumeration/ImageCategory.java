package priv.phr.o2o.common.enumeration;

import java.util.Objects;

public enum ImageCategory {
    USER_PROFILE("user-profile"),
    HEAD_LINE("head-line"),
    SHOP("shop"),
    SHOP_CATEGORY("shop-category"),
    PRODUCT("product"),
    PRODUCT_DETAIL("product-detail");

    private final String name;

    public String getName() {
        return name;
    }

    ImageCategory(String name) {
        this.name = name;
    }

    public static ImageCategory parse(String name) {
        for (ImageCategory imageCategory : values()) {
            if (Objects.equals(imageCategory.name, name)) {
                return imageCategory;
            }
        }

        return null;
    }
}
