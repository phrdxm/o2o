package priv.phr.o2o.common.enumeration;

import java.util.Objects;

public enum ShopStatus {

    DISABLE(-1),
    UNDER_REVIEW(0),
    ENABLE(1);

    private final Integer code;

    public Integer getCode() {
        return code;
    }

    ShopStatus(Integer code) {
        this.code = code;
    }

    public static ShopStatus parse(Integer code) {
        for (ShopStatus status : ShopStatus.values()) {
            if (Objects.equals(status.code, code)) {
                return status;
            }
        }

        return null;
    }
}
