package priv.phr.o2o.common.enumeration;

import java.util.Arrays;
import java.util.Objects;

import org.springframework.http.MediaType;

public enum FileType {
    JPEG("FFD8FF", "jpeg", MediaType.IMAGE_JPEG),
    PNG("89504E47", "png", MediaType.IMAGE_PNG),
    UNKNOWN(null, null, null);

    private String header;

    private String extension;

    private MediaType contentType;

    public String getHeader() {
        return header;
    }

    public String getExtension() {
        return extension;
    }

    public MediaType getContentType() {
        return contentType;
    }

    FileType(String header, String extension, MediaType contentType) {
        this.header = header;
        this.extension = extension;
        this.contentType = contentType;
    }

    /**
     * 根据文件流的前几个字节判断文件类型为jpeg或png
     *
     * @param fileSrc 文件字节
     * @return 图像文件类型，jpeg或png
     */
    public static FileType getFileType(byte[] fileSrc) {
        byte[] header = Arrays.copyOf(fileSrc, 4);
        String headerHex = String.format("%02X%02X%02X%02X", header[0], header[1], header[2], header[3]);
        for (FileType fileType : values()) {
            if (Objects.equals(fileType, UNKNOWN)) {
                continue;
            }
            if (headerHex.startsWith(fileType.header)) {
                return fileType;
            }
        }

        return UNKNOWN;
    }
}
