package priv.phr.o2o.common.enumeration;

import java.util.Objects;

public enum Gender {
    MALE(0),
    FEMALE(1);

    private final Integer code;

    public Integer getCode() {
        return code;
    }

    Gender(Integer code) {
        this.code = code;
    }

    public static Gender parse(Integer code) {
        for (Gender gender : Gender.values()) {
            if (Objects.equals(gender.code, code)) {
                return gender;
            }
        }

        return null;
    }

}
