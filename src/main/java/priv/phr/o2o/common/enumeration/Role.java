package priv.phr.o2o.common.enumeration;

import java.util.Objects;

public enum Role {
    ADMIN(3, "管理员"),
    OWNER(2, "店家"),
    USER(1, "普通用户"),
    TOURIST(0, "游客");

    private final Integer code;

    private final String description;

    public Integer getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    Role(Integer code, String description) {
        this.code = code;
        this.description = description;
    }

    public static Role parse(Integer code) {
        for (Role role : values()) {
            if (Objects.equals(role.code, code)) {
                return role;
            }
        }

        return null;
    }
}
