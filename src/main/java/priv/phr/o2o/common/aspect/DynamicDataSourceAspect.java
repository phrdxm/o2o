package priv.phr.o2o.common.aspect;

import priv.phr.o2o.common.config.DynamicDataSourceHolder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Aspect
@Component
@Profile("prod")
public class DynamicDataSourceAspect {

    @Pointcut("execution(* priv.phr.o2o.mapper.*Mapper.*(..))")
    public void crudPointcut() {
    }

    @Before("crudPointcut()")
    public void chooseDataSource(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().getName();
        if (!TransactionSynchronizationManager.isActualTransactionActive() &&
                (methodName.startsWith("select") || methodName.startsWith("exists"))) {
            if (Math.random() < 0.5) {
                DynamicDataSourceHolder.setDBType(DynamicDataSourceHolder.DBType.DB_MASTER);
            } else {
                DynamicDataSourceHolder.setDBType(DynamicDataSourceHolder.DBType.DB_SLAVE);
            }
        } else {
            DynamicDataSourceHolder.setDBType(DynamicDataSourceHolder.DBType.DB_MASTER);
        }
    }

    @After("crudPointcut()")
    public void afterCRUD() {
        DynamicDataSourceHolder.clearDBType();
    }
}
