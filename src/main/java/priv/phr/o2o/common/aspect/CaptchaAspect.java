package priv.phr.o2o.common.aspect;

import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.exception.CaptchaException;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.Util;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Aspect
@Component
@Order(Ordered.LOWEST_PRECEDENCE - 2)
public class CaptchaAspect {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Pointcut("@annotation(priv.phr.o2o.common.annotation.Captcha) || @within(priv.phr.o2o.common.annotation.Captcha)")
    public void checkCaptchaPointcut() {
    }

    @Before("checkCaptchaPointcut()")
    public void check() {
        String captchaHeader = ContextUtil.getRequest().getHeader("Captcha");
        if (!StringUtils.hasText(captchaHeader)) {
            throw new CaptchaException(Constant.CAPTCHA_HEADER_NOT_FOUND, captchaHeader);
        }

        String[] keyValuePair = captchaHeader.split("=");
        if (keyValuePair.length != 2 || !Util.regexpMatches(Constant.UUID_PATTERN, keyValuePair[0])) {
            throw new CaptchaException(Constant.CAPTCHA_HEADER_ERROR, captchaHeader);
        }

        if (!Util.regexpMatches(Constant.CAPTCHA_PATTERN, keyValuePair[1])) {
            throw new CaptchaException(Constant.CAPTCHA_ERROR, null);
        }

        String key = keyValuePair[0];
        String value = keyValuePair[1];

        String actualValue = redisTemplate.opsForValue().get(key);
        if (!StringUtils.hasText(actualValue)) {
            throw new CaptchaException(Constant.CAPTCHA_EXPIRED, null);
        }

        if (!actualValue.equalsIgnoreCase(value)) {
            throw new CaptchaException(Constant.CAPTCHA_ERROR, null);
        }
    }
}
