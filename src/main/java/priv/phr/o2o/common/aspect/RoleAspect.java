package priv.phr.o2o.common.aspect;

import static priv.phr.o2o.common.Constant.*;

import java.lang.reflect.Method;
import java.util.Objects;

import priv.phr.o2o.common.annotation.RequireRole;
import priv.phr.o2o.common.exception.AccountFrozenException;
import priv.phr.o2o.common.exception.RoleException;
import priv.phr.o2o.common.exception.SessionException;
import priv.phr.o2o.common.util.AssertionUtil;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.JWTUtil;
import priv.phr.o2o.common.util.Util;
import priv.phr.o2o.entity.UserE;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(Ordered.LOWEST_PRECEDENCE - 1)
public class RoleAspect {

    @Pointcut("@within(priv.phr.o2o.common.annotation.RequireRole) || @annotation(priv.phr.o2o.common.annotation.RequireRole)")
    public void checkRolePointcut() {
    }

    private UserE getUserFromRequest() {
        String authorization = ContextUtil.getRequest().getHeader("Authorization");
        if (!Util.regexpMatches(AUTHORIZATION_PATTERN, authorization)) {
            throw new SessionException(JWT_NOT_FOUND);
        }

        return JWTUtil.parseJWT(authorization.substring(AUTHORIZATION_PREFIX.length()));
    }

    private RequireRole getRequireRoleAnnotation(JoinPoint joinPoint) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        RequireRole annotation = method.getAnnotation(RequireRole.class);
        if (annotation == null) {
            annotation = method.getDeclaringClass().getAnnotation(RequireRole.class);
        }

        return annotation;
    }

    private void doCheck(UserE user, RequireRole annotation) {
        if (user.getFrozen()) {
            throw new AccountFrozenException(user);
        }
        if (user.getRole() == null || user.getRole().getCode() < annotation.value().getCode()) {
            throw new RoleException(annotation.value(), user.getRole(), UNAUTH);
        }
    }

    @Before("checkRolePointcut()")
    public void check(JoinPoint joinPoint) {
        RequireRole annotation = getRequireRoleAnnotation(joinPoint);
        AssertionUtil.notNull(annotation, "角色检查切面注解丢失");

        UserE user;
        try {
            user = getUserFromRequest();
        } catch (SessionException e) {
            if (Objects.equals(e.getMessage(), JWT_NOT_FOUND) && annotation.value().getCode() == 0) {
                ContextUtil.setCurrentUser(UserE.TOURIST);
                return;
            }
            throw e;
        }
        doCheck(user, annotation);
        ContextUtil.setCurrentUser(user);
    }
}
