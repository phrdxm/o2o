package priv.phr.o2o.common.aspect;

import java.util.Date;
import java.util.UUID;

import priv.phr.o2o.common.AuditObject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuditFieldAspect {

    @Pointcut("execution(* priv.phr.o2o.mapper.*.insert*(..))")
    public void insertPointcut() {

    }

    @Pointcut("execution(* priv.phr.o2o.mapper.*.update*(..))")
    public void updatePointcut() {

    }

    private static void auditInsert(AuditObject auditObject) {
        Date date = new Date();
        auditObject.setId(UUID.randomUUID().toString());
        auditObject.setCreateTime(date);
        auditObject.setLastUpdateTime(date);
    }

    private static void auditUpdate(AuditObject auditObject) {
        auditObject.setLastUpdateTime(new Date());
    }

    @Before("insertPointcut()")
    public void beforeInsert(JoinPoint joinPoint) {
        for (Object param : joinPoint.getArgs()) {
            if (AuditObject.class.isAssignableFrom(param.getClass())) {
                auditInsert((AuditObject) param);
            } else if (Iterable.class.isAssignableFrom(param.getClass())) {
                for (Object element : (Iterable) param) {
                    if (AuditObject.class.isAssignableFrom(element.getClass())) {
                        auditInsert((AuditObject) element);
                    }
                }
            }
        }
    }

    @Before("updatePointcut()")
    public void beforeUpdate(JoinPoint joinPoint) {
        for (Object param : joinPoint.getArgs()) {
            if (AuditObject.class.isAssignableFrom(param.getClass())) {
                auditUpdate((AuditObject) param);
            }
        }
    }
}
