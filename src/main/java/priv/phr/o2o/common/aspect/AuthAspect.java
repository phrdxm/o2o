package priv.phr.o2o.common.aspect;

import static priv.phr.o2o.common.Constant.AUTH_TOKEN_ERROR;
import static priv.phr.o2o.common.Constant.UUID_PATTERN;

import java.lang.reflect.Parameter;
import java.util.Objects;

import priv.phr.o2o.common.annotation.Token;
import priv.phr.o2o.common.exception.AuthException;
import priv.phr.o2o.common.util.ContextUtil;
import priv.phr.o2o.common.util.Util;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuthAspect {

    private static final Logger log = LoggerFactory.getLogger(AuthAspect.class);

    @Pointcut("@annotation(priv.phr.o2o.common.annotation.Auth) || @within(priv.phr.o2o.common.annotation.Auth)")
    public void authPointcut() {
    }

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Before("authPointcut()")
    public void auth(JoinPoint joinPoint) {
        Parameter[] parameters = ((MethodSignature)joinPoint.getSignature()).getMethod().getParameters();
        for (int i = 0; i < parameters.length; i++) {
            if (parameters[i].getAnnotation(Token.class) != null) {
                String token = (String) (joinPoint.getArgs()[i]);
                if (!Util.regexpMatches(UUID_PATTERN, token)) {
                    throw new AuthException(AUTH_TOKEN_ERROR);
                }

                if (!Objects.equals(redisTemplate.opsForValue().get(ContextUtil.getCurrentUserId()), token)) {
                    throw new AuthException(AUTH_TOKEN_ERROR);
                }
            }
        }
    }
}
