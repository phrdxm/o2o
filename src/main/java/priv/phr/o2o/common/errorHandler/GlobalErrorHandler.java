package priv.phr.o2o.common.errorHandler;

import static org.springframework.http.HttpStatus.*;

import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import io.undertow.server.RequestTooBigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import priv.phr.o2o.common.Constant;
import priv.phr.o2o.common.exception.*;
import priv.phr.o2o.dto.FailureDTO;

@RestControllerAdvice
public class GlobalErrorHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalErrorHandler.class);

    // --- BAD_REQUEST ---

    @ExceptionHandler({MethodArgumentNotValidException.class, BindException.class, ConstraintViolationException.class})
    @ResponseStatus(BAD_REQUEST)
    public FailureDTO handleValidException(Exception exception) {
        Set<String> errorMsgs = new HashSet<>();
        if (exception instanceof ConstraintViolationException) {
            for (ConstraintViolation<?> violation : ((ConstraintViolationException) exception).getConstraintViolations()) {
                errorMsgs.add(violation.getMessage());
            }
        } else {
            BindingResult bindingResult;
            if (exception instanceof MethodArgumentNotValidException) {
                bindingResult = ((MethodArgumentNotValidException) exception).getBindingResult();
            } else {
                bindingResult = ((BindException) exception).getBindingResult();
            }

            for (ObjectError error : bindingResult.getAllErrors()) {
                errorMsgs.add(error.getDefaultMessage());
            }
        }
        FailureDTO failureDTO = new FailureDTO(errorMsgs);
        log.error("参数校验错误：{}", String.join(";", failureDTO.getMessages()));
        return failureDTO;
    }

    @ExceptionHandler(UploadFileNotImageException.class)
    @ResponseStatus(BAD_REQUEST)
    public FailureDTO handleUploadFileNotImageException(UploadFileNotImageException uploadFileNotImageException) {
        log.debug(uploadFileNotImageException.getMessage());
        return new FailureDTO(uploadFileNotImageException);
    }

    @ExceptionHandler({RequestTooBigException.class})
    @ResponseStatus(BAD_REQUEST)
    public FailureDTO handleUploadFileTooLargeException(RequestTooBigException requestTooBigException) {
        log.debug(requestTooBigException.getMessage());
        return new FailureDTO(Constant.UPLOAD_FILE_TOO_LARGE);
    }

    @ExceptionHandler(CaptchaException.class)
    @ResponseStatus(BAD_REQUEST)
    public FailureDTO handleCaptchaException(CaptchaException captchaException) {
        if (captchaException.getCaptchaHeader() != null) {
            log.error("请求头字段错误 Captcha: {}", captchaException.getCaptchaHeader());
        }
        return new FailureDTO(captchaException);
    }

    // --- NOT_FOUND ---

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public FailureDTO handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        log.debug(resourceNotFoundException.getMessage());
        return new FailureDTO(resourceNotFoundException);
    }

    // --- INTERNAL_SERVER_ERROR ---

    @ExceptionHandler(AssertionError.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public FailureDTO handleAssertionError(AssertionError assertionError) {
        log.error("断言失败", assertionError);
        return new FailureDTO("服务器内部错误");
    }

    @ExceptionHandler(FileLoadException.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public FailureDTO handleFileLoadException(FileLoadException fileLoadException) {
        log.error(fileLoadException.getMessage(), fileLoadException);
        return new FailureDTO(fileLoadException);
    }

    // --- UNAUTHORIZED ---

    @ExceptionHandler(SessionException.class)
    @ResponseStatus(UNAUTHORIZED)
    public FailureDTO handleSessionException(SessionException sessionException) {
        log.error(sessionException.getMessage());
        return new FailureDTO(sessionException);
    }

    @ExceptionHandler(RoleException.class)
    @ResponseStatus(UNAUTHORIZED)
    public FailureDTO handleRoleException(RoleException roleException) {
        log.error("{}，需要 {} 角色，实际角色为 {}", roleException.getMessage(), roleException.getRequireRole().getDescription(),
                roleException.getActualRole().getDescription());
        return new FailureDTO(roleException);
    }

    @ExceptionHandler(SignInException.class)
    @ResponseStatus(UNAUTHORIZED)
    public FailureDTO handleSignInException(SignInException signInException) {
        log.error("用户 {} 登录时发生意外：{}", signInException.getUsername(), signInException.getMessage());
        return new FailureDTO(signInException);
    }

    @ExceptionHandler(AuthException.class)
    @ResponseStatus(UNAUTHORIZED)
    public FailureDTO handleAuthException(AuthException authException) {
        log.error(authException.getMessage());
        return new FailureDTO(authException);
    }

    // --- FORBIDDEN ---

    @ExceptionHandler(AccountFrozenException.class)
    @ResponseStatus(FORBIDDEN)
    public FailureDTO handleAccountFrozenException(AccountFrozenException accountFrozenException) {
        log.error("用户 {} 已冻结", accountFrozenException.getFrozenUser().getId());
        return new FailureDTO(accountFrozenException);
    }

    // --- UNPROCESSABLE_ENTITY ---

    @ExceptionHandler(NamesakeException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public FailureDTO handleNamesakeException(NamesakeException namesakeException) {
        log.debug(namesakeException.getMessage());
        return new FailureDTO(namesakeException);
    }

    @ExceptionHandler(IllegalShopStatusException.class)
    @ResponseStatus(UNPROCESSABLE_ENTITY)
    public FailureDTO handleIllegalShopStatusException(IllegalShopStatusException illegalShopStatusException) {
        log.error(illegalShopStatusException.getMessage());
        return new FailureDTO(illegalShopStatusException);
    }
}
