package priv.phr.o2o.common.util

import priv.phr.o2o.Application
import priv.phr.o2o.common.Constant
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.exception.SessionException
import priv.phr.o2o.common.util.JWTUtil
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class JWTUtilSpec extends Specification {

    @Autowired
    private UserMapper userMapper

    def "JWTUtil create&parse"() {
        UserE user = new UserE()

        when: "正常创建"
        user.nickname = "test user"
        user.gender = Gender.MALE
        user.role = Role.USER
        userMapper.insert(user)
        def jwt = JWTUtil.createJWT(user)
        then:
        notThrown(Throwable)
        print(jwt)

        when: "待解析字符串为null"
        JWTUtil.parseJWT(null)
        then:
        AssertionError assertionError = thrown()
        assertionError.message == "JWT格式错误"

        when: "待解析字符串为空串"
        JWTUtil.parseJWT("")
        then:
        assertionError = thrown()
        assertionError.message == "JWT格式错误"

        when: "待解析字符串格式错误"
        JWTUtil.parseJWT("abc")
        then:
        assertionError = thrown()
        assertionError.message == "JWT格式错误"

        when: "待解析字符串格式正确，但解析错误"
        JWTUtil.parseJWT("a.b.c")
        then:
        SessionException sessionException = thrown()
        sessionException.message == Constant.JWT_PARSE_ERROR

        when: "正常解析"
        UserE user1 = JWTUtil.parseJWT(jwt)
        then:
        notThrown(Throwable)
        user.properties.each {
            Objects.equals(user1.properties[it.key], it.value)
        }
        userMapper.delete(user)

        when: "数据库中没有会话数据"
        user1 = JWTUtil.parseJWT(jwt)
        then:
        sessionException = thrown()
        sessionException.message == Constant.JWT_PARSE_ERROR
    }
}
