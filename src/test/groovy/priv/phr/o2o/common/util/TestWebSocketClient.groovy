package priv.phr.o2o.common.util

import com.alibaba.fastjson.JSON
import priv.phr.o2o.dto.MessageDTO
import org.java_websocket.client.WebSocketClient
import org.java_websocket.handshake.ServerHandshake
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class TestWebSocketClient extends WebSocketClient {

    private static final Logger log = LoggerFactory.getLogger(TestWebSocketClient)

    private List<Object> results

    private boolean received

    boolean isReceived() {
        if (received) {
            received = false
            true
        } else {
            false
        }
    }

    void receiveBlocking() {
        while (!isReceived()) {
            Thread.sleep(50);
        }
    }

    private int index = -1

    void setResults(List<Object> results) {
        this.results = results
    }

    TestWebSocketClient(String uri) {
        super(URI.create(uri))
    }

    @Override
    void onOpen(ServerHandshake serverHandshake) {
        log.info("on open")
    }

    @Override
    void onMessage(String s) {
        log.info("on message: {}", s)
        if (results[++index] instanceof String) {
            assert Objects.equals(results[index], s)
        } else if (results[index] instanceof MessageDTO) {
            MessageDTO webSocketDTO = JSON.parseObject(s, MessageDTO)
            assert results[index].from == webSocketDTO.from
            assert results[index].to == webSocketDTO.to
            assert results[index].message == webSocketDTO.message
        } else {
            assert Objects.equals(JSON.toJSONString(results[index]), s)
        }
        received = true
    }

    @Override
    void onClose(int i, String s, boolean b) {
        log.info("on close")
    }

    @Override
    void onError(Exception e) {
        log.info("on error: {}", e.getMessage())
    }
}
