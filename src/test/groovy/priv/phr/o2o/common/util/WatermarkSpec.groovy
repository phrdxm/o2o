package priv.phr.o2o.common.util

import priv.phr.o2o.service.ImageService
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.Resource
import spock.lang.Specification

class WatermarkSpec extends Specification {

    def "watermark test"() {
        when:
        Resource newImg = new ClassPathResource("new_img.jpg")
        if (newImg.exists()) {
            newImg.file.delete();
        }
        Thumbnails.of(new ClassPathResource("original_img.jpg").file).size(200, 200)
                .watermark(Positions.BOTTOM_RIGHT, ImageService.WATERMARK, 0.75f)
                .outputQuality(0.8f).toFile(new File("src/test/resources/new_img.jpg"));
        then:
        notThrown(Throwable)
    }
}
