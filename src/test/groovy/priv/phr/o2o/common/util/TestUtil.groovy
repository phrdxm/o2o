package priv.phr.o2o.common.util

import priv.phr.o2o.common.Constant
import priv.phr.o2o.common.util.JWTUtil
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.UserMapper
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import tk.mybatis.mapper.common.BaseMapper

import java.util.concurrent.TimeUnit

import static priv.phr.o2o.common.util.ContextUtil.getBean
import static priv.phr.o2o.common.util.ContextUtil.getContext
import static org.springframework.http.HttpStatus.*

class TestUtil {

    static final String defaultUUID = "00000000-0000-0000-0000-000000000000"

    static final String defaultCaptcha = "8888"

    static HttpHeaders createHttpHeaders(UserE userE) {
        getBean(UserMapper).insert(userE)
        HttpHeaders headers = new HttpHeaders()
        headers.add("Authorization", Constant.AUTHORIZATION_PREFIX + JWTUtil.createJWT(userE))
        headers
    }

    static void cleanupEntity() {
        for (def mapper : getContext().getBeansOfType(BaseMapper).values()) {
            for (def entity : mapper.selectAll()) {
                mapper.deleteByPrimaryKey(entity.id)
            }
        }
    }

    static void addCaptcha(HttpHeaders headers) {
        String token = UUID.randomUUID().toString()
        getBean(StringRedisTemplate).opsForValue().set(token, defaultCaptcha, 1L, TimeUnit.MINUTES)
        headers.add("Captcha", token + "=" + defaultCaptcha)
    }

    static void failed(ResponseEntity response, HttpStatus httpStatus, String... messages) {
        assert response.statusCode == httpStatus
        assert response.body.failed
        assert response.body.messages.length == messages.length
        assert Arrays.asList(response.body.messages).containsAll(messages)
    }

    static void unauthorized(ResponseEntity response, String... messages) {
        failed(response, UNAUTHORIZED, messages)
    }

    static void notFound(ResponseEntity response, String... messages) {
        failed(response, NOT_FOUND, messages)
    }

    static void badRequest(ResponseEntity response, String... messages) {
        failed(response, BAD_REQUEST, messages)
    }

    static void unprocessableEntity(ResponseEntity response, String... messages) {
        failed(response, UNPROCESSABLE_ENTITY, messages)
    }

    static void forbidden(ResponseEntity response, String... messages) {
        failed(response, FORBIDDEN, messages)
    }
}
