package priv.phr.o2o.common.util

import priv.phr.o2o.common.util.PasswordUtil
import spock.lang.Specification

class PasswordUtilSpec extends Specification {

    def "PasswordUtil encode&check"() {
        when:
        PasswordUtil.encode(null)
        then:
        AssertionError error = thrown()
        with(error) {
            message == "明文密码不能为null"
        }

        when:
        PasswordUtil.check(null, null)
        then:
        error = thrown()
        with(error) {
            message == "明文密码不能为null"
        }

        expect:
        PasswordUtil.check("", PasswordUtil.encode(""))
        def encoded = PasswordUtil.encode("123456")
        for (def b : encoded) {
            print(Integer.toHexString(b & 0xff))
        }
        PasswordUtil.check("123456", encoded)

        !PasswordUtil.check("", null)
        !PasswordUtil.check("", new byte[0])
        !PasswordUtil.check("", new byte[22])
        !PasswordUtil.check("123456", PasswordUtil.encode("654321"))
    }
}
