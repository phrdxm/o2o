package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.entity.ShopCategoryE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.ShopCategoryMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.util.TestUtil.cleanupEntity
import static priv.phr.o2o.common.util.TestUtil.createHttpHeaders
import static org.springframework.http.HttpMethod.GET

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ShopCategoryControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ShopCategoryMapper shopCategoryMapper

    def setup() {
        cleanupEntity()
    }

    def "/primary-categories & /category/sub-categories?parent-id={parentId}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.USER)
        def headers = createHttpHeaders(userE)

        ShopCategoryE rootCategory1 = new ShopCategoryE(name: "root category1", parentId: null)
        shopCategoryMapper.insert(rootCategory1)
        ShopCategoryE subCategory1 = new ShopCategoryE(name: "sub category1", parentId: rootCategory1.id)
        shopCategoryMapper.insert(subCategory1)
        ShopCategoryE rootCategory2 = new ShopCategoryE(name: "root category2", parentId: null)
        shopCategoryMapper.insert(rootCategory2)
        ShopCategoryE subCategory2 = new ShopCategoryE(name: "sub category2", parentId: rootCategory2.id)
        shopCategoryMapper.insert(subCategory2)

        when: "获取第一级分类"
        def response = restTemplate.exchange("/primary-categories",
                GET,
                new HttpEntity<Object>(headers),
                List
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            body.size() == 2

            body[0].id == rootCategory1.id
            body[0].createTime == rootCategory1.createTime.time
            body[0].lastUpdateTime == rootCategory1.lastUpdateTime.time
            body[0].name == rootCategory1.name
            body[0].description == null
            body[0].imageFileName == null
            body[0].parentId == null

            body[1].id == rootCategory2.id
            body[1].createTime == rootCategory2.createTime.time
            body[1].lastUpdateTime == rootCategory2.lastUpdateTime.time
            body[1].name == rootCategory2.name
            body[1].description == null
            body[1].imageFileName == null
            body[1].parentId == null
        }

        when: "获取特定子分类"
        response = restTemplate.exchange("/category/sub-categories?parent-id={parentId}",
                GET,
                new HttpEntity<Object>(headers),
                List,
                subCategory1.parentId
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            body.size() == 1
            body[0].id == subCategory1.id
            body[0].createTime == subCategory1.createTime.time
            body[0].lastUpdateTime == subCategory1.lastUpdateTime.time
            body[0].name == subCategory1.name
            body[0].description == null
            body[0].imageFileName == null
            body[0].parentId == subCategory1.parentId
        }

        when: "获取所有子分类"
        response = restTemplate.exchange("/category/sub-categories",
                GET,
                new HttpEntity<Object>(headers),
                List
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            body.size() == 2

            body[0].id == subCategory1.id
            body[0].createTime == subCategory1.createTime.time
            body[0].lastUpdateTime == subCategory1.lastUpdateTime.time
            body[0].name == subCategory1.name
            body[0].description == null
            body[0].imageFileName == null
            body[0].parentId == subCategory1.parentId

            body[1].id == subCategory2.id
            body[1].createTime == subCategory2.createTime.time
            body[1].lastUpdateTime == subCategory2.lastUpdateTime.time
            body[1].name == subCategory2.name
            body[1].description == null
            body[1].imageFileName == null
            body[1].parentId == subCategory2.parentId
        }
    }

    def cleanup() {
        cleanupEntity()
    }
}
