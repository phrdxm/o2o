package priv.phr.o2o.controller.admin

import com.github.pagehelper.PageInfo
import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.exception.UserNotFoundException
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.UserInfoDTO
import priv.phr.o2o.entity.LocalAuthE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.LocalAuthMapper
import priv.phr.o2o.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpMethod.PUT
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class UserManagementControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private UserMapper userMapper

    @Autowired
    private LocalAuthMapper localAuthMapper

    def "/admin/user/frozen/{id}?frozen={frozen}"() {
        UserE userE1 = new UserE(nickname: "test", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(userE1)

        when: "用户不存在"
        def response = restTemplate.exchange("/admin/user/frozen/{id}?frozen={frozen}",
                PUT,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID, true
        )
        then:
        notFound(response, UserNotFoundException.USER_NOT_FOUND)

        when: "冻结管理员"
        UserE userE2 = new UserE(nickname: "test admin", gender: Gender.MALE, role: Role.ADMIN, frozen: false)
        userMapper.insert(userE2)
        response = restTemplate.exchange("/admin/user/frozen/{id}?frozen={frozen}",
                PUT,
                new HttpEntity<Object>(headers),
                UserInfoDTO,
                userE2.id, true
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                !frozen
            }
        }

        when: "冻结普通用户"
        userE2.role = Role.USER
        userMapper.updateByPrimaryKey(userE2)
        response = restTemplate.exchange("/admin/user/frozen/{id}?frozen={frozen}",
                PUT,
                new HttpEntity<Object>(headers),
                UserInfoDTO,
                userE2.id, true
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                frozen
            }
        }
    }

    def "GET /admin/users?page={page}&size={size}"() {
        UserE admin = new UserE(nickname: "admin", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(admin)
        LocalAuthE adminLocalAuthE = new LocalAuthE(username: "admin", userId: admin.id)
        localAuthMapper.insert(adminLocalAuthE)
        when: "空列表"
        def response = restTemplate.exchange("/admin/users?page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 0
            }
        }

        when: "正常获取"
        UserE user1 = new UserE(nickname: "user1", gender: Gender.MALE, role: Role.OWNER)
        userMapper.insert(user1)
        LocalAuthE localAuth1 = new LocalAuthE(username: "user1", userId: user1.id)
        localAuthMapper.insert(localAuth1)
        UserE user2 = new UserE(nickname: "user2", gender: Gender.MALE, role: Role.USER)
        userMapper.insert(user2)
        LocalAuthE localAuth2 = new LocalAuthE(username: "user2", userId: user2.id)
        localAuthMapper.insert(localAuth2)
        response = restTemplate.exchange("/admin/users?page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 1
                list[0].username == localAuth1.username
                list[0].nickname == user1.nickname
            }
        }
    }

    def cleanup() {
        cleanupEntity()
    }
}
