package priv.phr.o2o.controller.admin

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.enumeration.ShopStatus
import priv.phr.o2o.common.exception.IllegalShopStatusException
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.ShopInfoDTO
import priv.phr.o2o.entity.ShopE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.ShopMapper
import priv.phr.o2o.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.ADVICE_TOO_LONG
import static priv.phr.o2o.common.Constant.UNAUTH
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.PUT
import static org.springframework.http.HttpStatus.CREATED

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ShopManagementControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ShopMapper shopMapper

    @Autowired
    private UserMapper userMapper

    def "/admin/shop/commit/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)

        when: "店铺状态错误"
        def response = restTemplate.exchange("/admin/shop/commit/{id}",
                PUT,
                new HttpEntity<Object>(headers),
                FailureDTO,
                shopE.id
        )
        then:
        unprocessableEntity(response, IllegalShopStatusException.ILLEGAL_SHOP_STATUS)

        when: "正常提交"
        shopE.status = ShopStatus.DISABLE
        shopMapper.updateByPrimaryKey(shopE)
        response = restTemplate.exchange("/admin/shop/commit/{id}",
                PUT,
                new HttpEntity<Object>(headers),
                ShopInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                name == shopE.name
                lastUpdateTime.after(createTime)
                status == ShopStatus.UNDER_REVIEW.code
            }
        }
    }

    def "/admin/shop/enable/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.UNDER_REVIEW,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID,
                advice: "建议"
        )
        shopMapper.insert(shopE)

        when:
        def response = restTemplate.exchange("/admin/shop/enable/{id}",
                PUT,
                new HttpEntity<Object>(headers),
                ShopInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                name == shopE.name
                lastUpdateTime.after(createTime)
                status == ShopStatus.ENABLE.code
                advice == null
            }
        }
    }

    def "/admin/shop/disable/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)

        when: "建议过长"
        def response = restTemplate.exchange("/admin/shop/disable/{id}",
                PUT,
                new HttpEntity<Object>([advice: "1" * 256], headers),
                FailureDTO,
                shopE.id
        )
        then:
        badRequest(response, ADVICE_TOO_LONG)

        when: "普通用户禁用别人店铺"
        response = restTemplate.exchange("/admin/shop/disable/{id}",
                PUT,
                new HttpEntity<Object>([advice: "建议"], headers),
                FailureDTO,
                shopE.id
        )
        then:
        unauthorized(response, UNAUTH)

        when: "普通用户禁用自己店铺"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        response = restTemplate.exchange("/admin/shop/disable/{id}",
                PUT,
                new HttpEntity<Object>(headers),
                ShopInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.after(createTime)
                status == ShopStatus.DISABLE.code
            }
        }

        when: "管理员禁用店铺"
        shopE.ownerId = defaultUUID
        shopMapper.updateByPrimaryKey(shopE)
        userE.role = Role.ADMIN
        userMapper.updateByPrimaryKey(userE)
        response = restTemplate.exchange("/admin/shop/disable/{id}",
                PUT,
                new HttpEntity<Object>([advice: "建议"], headers),
                ShopInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.after(createTime)
                status == ShopStatus.DISABLE.code
                advice == "建议"
            }
        }
    }

    def cleanup() {
        cleanupEntity()
    }
}
