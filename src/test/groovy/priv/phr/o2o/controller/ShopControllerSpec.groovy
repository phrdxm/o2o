package priv.phr.o2o.controller

import com.github.pagehelper.PageInfo
import priv.phr.o2o.Application
import priv.phr.o2o.common.Constant
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.ImageCategory
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.enumeration.ShopStatus
import priv.phr.o2o.common.exception.AreaNotFoundException
import priv.phr.o2o.common.exception.ShopCategoryNotFoundException
import priv.phr.o2o.common.exception.ShopNamesakeException
import priv.phr.o2o.common.exception.ShopNotFoundException
import priv.phr.o2o.common.util.Util
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.ShopInfoDTO
import priv.phr.o2o.dto.UserInfoDTO
import priv.phr.o2o.entity.*
import priv.phr.o2o.mapper.*
import priv.phr.o2o.entity.AreaE
import priv.phr.o2o.entity.ProductCategoryE
import priv.phr.o2o.entity.ProductE
import priv.phr.o2o.entity.ShopCategoryE
import priv.phr.o2o.entity.ShopE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.AreaMapper
import priv.phr.o2o.mapper.ProductCategoryMapper
import priv.phr.o2o.mapper.ProductMapper
import priv.phr.o2o.mapper.ShopCategoryMapper
import priv.phr.o2o.mapper.ShopMapper
import priv.phr.o2o.mapper.UserMapper
import priv.phr.o2o.service.ImageService
import priv.phr.o2o.service.RemoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.data.redis.core.StringRedisTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import java.util.concurrent.TimeUnit

import static priv.phr.o2o.common.Constant.UNAUTH
import static priv.phr.o2o.common.Constant.UUID_PATTERN
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ShopControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private AreaMapper areaMapper

    @Autowired
    private ShopCategoryMapper shopCategoryMapper

    @Autowired
    private ShopMapper shopMapper

    @Autowired
    private UserMapper userMapper

    @Autowired
    private ImageService imageService

    @Autowired
    private StringRedisTemplate redisTemplate

    @Autowired
    private ProductMapper productMapper

    @Autowired
    private ProductCategoryMapper productCategoryMapper

    def "/shop"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.USER)
        def headers = createHttpHeaders(userE)
        addCaptcha(headers)

        when: "区域不存在"
        ShopCategoryE shopCategoryE = new ShopCategoryE(name: "test shop category", parentId: defaultUUID)
        shopCategoryMapper.insert(shopCategoryE)
        def response = restTemplate.exchange("/shop",
                POST,
                new HttpEntity<Object>([
                        name       : "test shop",
                        description: "描述",
                        address    : "地址",
                        areaId     : defaultUUID,
                        categoryId : shopCategoryE.id
                ], headers),
                FailureDTO
        )
        then:
        notFound(response, AreaNotFoundException.AREA_NOT_FOUND)

        when: "店铺类别不存在"
        AreaE areaE = new AreaE(name: "test area")
        areaMapper.insert(areaE)
        response = restTemplate.exchange("/shop",
                POST,
                new HttpEntity<Object>([
                        name       : "test shop",
                        description: "描述",
                        address    : "地址",
                        areaId     : areaE.id,
                        categoryId : defaultUUID
                ], headers),
                FailureDTO
        )
        then:
        notFound(response, ShopCategoryNotFoundException.SHOP_CATEGORY_NOT_FOUND)

        when: "店铺名重复"
        userE.role = Role.OWNER
        userMapper.updateByPrimaryKey(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)
        response = restTemplate.exchange("/shop",
                POST,
                new HttpEntity<Object>([
                        name       : shopE.name,
                        description: "描述",
                        address    : "地址",
                        areaId     : areaE.id,
                        categoryId : shopCategoryE.id
                ], headers),
                FailureDTO
        )
        then:
        unprocessableEntity(response, ShopNamesakeException.SHOP_NAMESAKE)

        when: "正常创建，无图片"
        response = restTemplate.exchange("/shop",
                POST,
                new HttpEntity<Object>([
                        name       : shopE.name + "1",
                        description: "描述",
                        address    : "地址",
                        areaId     : areaE.id,
                        categoryId : shopCategoryE.id
                ], headers),
                ShopInfoDTO
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.equals(createTime)
                name == shopE.name + "1"
                description == "描述"
                address == "地址"
                phone == null
                imageFileName == null
                status == ShopStatus.UNDER_REVIEW.code
                areaId == areaE.id
                categoryId == shopCategoryE.id
                ownerId == userE.id
            }
        }

        when: "正常创建，有图片"
        userE.role = Role.USER
        userMapper.updateByPrimaryKey(userE)
        shopMapper.deleteByPrimaryKey(shopE.id)
        response = restTemplate.exchange("/shop",
                POST,
                new HttpEntity<Object>([
                        name       : shopE.name + "1",
                        description: "描述",
                        address    : "地址",
                        areaId     : areaE.id,
                        categoryId : shopCategoryE.id,
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                ShopInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, imageFileName)
                lastUpdateTime.equals(createTime)
                name == shopE.name + "1"
                description == "描述"
                address == "地址"
                phone == null
                status == ShopStatus.UNDER_REVIEW.code
                areaId == areaE.id
                categoryId == shopCategoryE.id
                ownerId == userE.id
            }
        }
        userMapper.selectByPrimaryKey(userE.id).role == Role.OWNER
    }

    def "PUT /shop"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)

        when: "店铺不存在"
        def response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([id: defaultUUID], headers),
                FailureDTO
        )
        then:
        notFound(response, ShopNotFoundException.SHOP_NOT_FOUND)

        when: "店铺不属于当前用户"
        ShopE shopE1 = new ShopE(
                name: "test shop1",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE1)
        response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([id: shopE1.id], headers),
                FailureDTO
        )
        then:
        unauthorized(response, UNAUTH)

        when: "店铺名称重复"
        shopE1.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE1)
        ShopE shopE2 = new ShopE(
                name: "test shop2",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE2)
        response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([id: shopE1.id, name: shopE2.name], headers),
                FailureDTO
        )
        then:
        unprocessableEntity(response, ShopNamesakeException.SHOP_NAMESAKE)

        when: "正常更新，无图片"
        AreaE areaE = new AreaE(name: "test area")
        areaMapper.insert(areaE)
        ShopCategoryE shopCategoryE = new ShopCategoryE(name: "shop category", parentId: defaultUUID)
        shopCategoryMapper.insert(shopCategoryE)
        response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([
                        id         : shopE1.id,
                        name       : shopE1.name + "1",
                        address    : "地址",
                        description: "描述",
                        areaId     : areaE.id,
                        categoryId : shopCategoryE.id,
                ], headers),
                ShopInfoDTO
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                imageFileName == null
                lastUpdateTime.after(createTime)
                name == shopE1.name + "1"
                description == "描述"
                address == "地址"
                phone == null
                status == ShopStatus.UNDER_REVIEW.code
                areaId == areaE.id
                categoryId == shopCategoryE.id
                ownerId == userE.id
            }
        }

        when: "正常更新，有图片"
        response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([
                        id         : shopE1.id,
                        name       : shopE1.name + "2",
                        address    : "",
                        description: "描述1",
                        areaId     : areaE.id,
                        categoryId : shopCategoryE.id,
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                ShopInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, imageFileName)
                lastUpdateTime.after(createTime)
                name == shopE1.name + "2"
                description == "描述1"
                address == "地址"
                phone == null
                status == ShopStatus.UNDER_REVIEW.code
                areaId == areaE.id
                categoryId == shopCategoryE.id
                ownerId == userE.id
            }
        }

        when: "正常更新，删除图片"
        response = restTemplate.exchange("/shop",
                PUT,
                new HttpEntity<Object>([
                        id         : shopE1.id,
                        address    : "地址1",
                        description: "描述1",
                        base64Image: ""
                ], headers),
                ShopInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.SHOP.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                imageFileName == null
                lastUpdateTime.after(createTime)
                name == shopE1.name + "2"
                description == "描述1"
                address == "地址1"
                phone == null
                status == ShopStatus.UNDER_REVIEW.code
                areaId == areaE.id
                categoryId == shopCategoryE.id
                ownerId == userE.id
            }
        }
    }

    def "GET /shops?page={page}&size={size}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)

        when: "空列表"
        def response = restTemplate.exchange("/shops?page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 0
            }
        }

        when: "正常获取"
        ShopE shopE1 = new ShopE(
                name: "test shop1",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE1)
        ShopE shopE2 = new ShopE(
                name: "test shop2",
                status: ShopStatus.DISABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE2)
        response = restTemplate.exchange("/shops?page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 1
                list[0].id == shopE2.id
            }
        }
    }

    def "GET /shops-conditional?page={page}&size={size}&category-id={categoryId}&area-id={areaId}"() {
        AreaE areaE = new AreaE(name: "test area")
        areaMapper.insert(areaE)
        ShopCategoryE rootCategoryE = new ShopCategoryE(name: "root category")
        shopCategoryMapper.insert(rootCategoryE)
        ShopCategoryE subCategoryE = new ShopCategoryE(name: "sub category", parentId: rootCategoryE.id)
        shopCategoryMapper.insert(subCategoryE)

        ShopE shop1 = new ShopE(name: "shop1", status: ShopStatus.DISABLE, ownerId: defaultUUID, categoryId: defaultUUID, areaId: defaultUUID)
        ShopE shop2 = new ShopE(name: "shop2", status: ShopStatus.ENABLE, ownerId: defaultUUID, categoryId: defaultUUID, areaId: defaultUUID)
        ShopE shop3 = new ShopE(name: "shop3", status: ShopStatus.ENABLE, ownerId: defaultUUID, categoryId: subCategoryE.id, areaId: defaultUUID)
        ShopE shop4 = new ShopE(name: "shop4", status: ShopStatus.ENABLE, ownerId: defaultUUID, categoryId: defaultUUID, areaId: areaE.id)
        ShopE shop5 = new ShopE(name: "shop5", status: ShopStatus.ENABLE, ownerId: defaultUUID, categoryId: subCategoryE.id, areaId: areaE.id)
        shopMapper.insert(shop1)
        shopMapper.insert(shop2)
        shopMapper.insert(shop3)
        shopMapper.insert(shop4)
        shopMapper.insert(shop5)

        when: "无条件选择"
        def response = restTemplate.exchange("/shops-conditional?page={page}&size={size}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                1, 5
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                Objects.equals(
                        new HashSet([list[0].name, list[1].name, list[2].name, list[3].name]),
                        new HashSet(["shop2", "shop3", "shop4", "shop5"]))
            }
        }

        when: "选择指定分类下的店铺"
        response = restTemplate.exchange("/shops-conditional?page={page}&size={size}&category-id={categoryId}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                1, 5, subCategoryE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                Objects.equals(
                        new HashSet([list[0].name, list[1].name]),
                        new HashSet(["shop3", "shop5"]))
            }
        }

        when: "选择指定一级分类下的店铺"
        response = restTemplate.exchange("/shops-conditional?page={page}&size={size}&category-id={categoryId}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                1, 5, rootCategoryE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                Objects.equals(
                        new HashSet([list[0].name, list[1].name]),
                        new HashSet(["shop3", "shop5"]))
            }
        }

        when: "选择指定区域下的店铺"
        response = restTemplate.exchange("/shops-conditional?page={page}&size={size}&area-id={areaId}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                1, 5, areaE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                Objects.equals(
                        new HashSet([list[0].name, list[1].name]),
                        new HashSet(["shop4", "shop5"]))
            }
        }

        when: "选择指定区域和分类下的店铺"
        response = restTemplate.exchange("/shops-conditional?page={page}&size={size}&area-id={areaId}&category-id={categoryId}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                1, 5, areaE.id, subCategoryE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 1
                list[0].name == "shop5"
            }
        }
    }

    def "GET /shop/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.USER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.DISABLE,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)

        when:
        def response = restTemplate.exchange("/shop/{id}",
                GET,
                new HttpEntity<Object>(headers),
                ShopInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                name == shopE.name
                status == ShopStatus.DISABLE.code
                ownerId == shopE.ownerId
                categoryId == shopE.categoryId
                areaId == shopE.areaId
            }
        }
    }

    def "DELETE /shop/{id}?token={token}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.DISABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID,
                imageFileName: defaultUUID
        )
        shopMapper.insert(shopE)

        when: "token错误"
        def response = restTemplate.exchange("/shop/{id}?token={token}",
                DELETE,
                new HttpEntity<Object>(headers),
                FailureDTO,
                shopE.id, defaultUUID
        )
        then:
        unauthorized(response, Constant.AUTH_TOKEN_ERROR)

        when: "正常删除"
        String token = UUID.randomUUID().toString()
        redisTemplate.opsForValue().set(userE.id, token, 1L, TimeUnit.MINUTES)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id)
        productMapper.insert(productE)
        ProductCategoryE productCategoryE = new ProductCategoryE(name: "test product category", shopId: shopE.id)
        productCategoryMapper.insert(productCategoryE)
        response = restTemplate.exchange("/shop/{id}?token={token}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                shopE.id, token
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.SHOP.name, shopE.imageFileName)
            0 * _
        }
        response.statusCode == NO_CONTENT
        userMapper.selectByPrimaryKey(userE.id).role == Role.USER
        !productMapper.existsWithPrimaryKey(productE.id)
        !productCategoryMapper.existsWithPrimaryKey(productCategoryE.id)
    }

    def "GET /shop/{id}/owner"() {
        when: "店铺不存在"
        def response = restTemplate.exchange("/shop/{id}/owner",
                GET,
                new HttpEntity<Object>(),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, ShopNotFoundException.SHOP_NOT_FOUND)

        when: "正常获取"
        UserE userE = new UserE(nickname: "test user", gender: Gender.FEMALE, role: Role.OWNER)
        userMapper.insert(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID,
                imageFileName: defaultUUID
        )
        shopMapper.insert(shopE)
        response = restTemplate.exchange("/shop/{id}/owner",
                GET,
                new HttpEntity<Object>(),
                UserInfoDTO,
                shopE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                id == userE.id
                nickname == userE.nickname
                gender == Gender.FEMALE.code
                role == Role.OWNER.code
            }
        }
    }

    def cleanup() {
        cleanupEntity()
    }
}
