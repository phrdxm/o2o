package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.entity.AreaE
import priv.phr.o2o.mapper.AreaMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.util.TestUtil.cleanupEntity
import static org.springframework.http.HttpMethod.GET
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class AreaControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private AreaMapper areaMapper

    def "GET /areas"() {
        when: "正常获取"
        for (AreaE areaE : areaMapper.selectAll()) {
            areaMapper.deleteByPrimaryKey(areaE.id);
        }
        AreaE areaE = new AreaE(name: "test area")
        areaMapper.insert(areaE)
        def response = restTemplate.exchange("/areas",
                GET,
                new HttpEntity<Object>(),
                List
        )
        then:
        with(response) {
            statusCode == OK
            body == [
                    [
                            id: areaE.id,
                            name: areaE.name,
                            createTime: areaE.createTime.time,
                            lastUpdateTime: areaE.lastUpdateTime.time
                    ]
            ]
        }
    }

    def cleanup() {
        cleanupEntity()
    }

}
