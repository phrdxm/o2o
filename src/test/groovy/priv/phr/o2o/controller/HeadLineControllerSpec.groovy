package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.ImageCategory
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.exception.HeadLineNotFoundException
import priv.phr.o2o.common.util.TestUtil
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.HeadLineInfoDTO
import priv.phr.o2o.entity.HeadLineE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.HeadLineMapper
import priv.phr.o2o.service.ImageService
import priv.phr.o2o.service.RemoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.*
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class HeadLineControllerSpec extends Specification {

    def setup() {
        TestUtil.cleanupEntity()
    }

    @Autowired
    private HeadLineMapper headLineMapper

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ImageService imageService

    def "GET /head-line?available={available}"() {
        HeadLineE headLine1 = new HeadLineE(title: "test head line 1", available: false)
        HeadLineE headLine2 = new HeadLineE(title: "test head line 2", available: true)
        headLineMapper.insert(headLine1);
        headLineMapper.insert(headLine2);

        when: "获取所有头条"
        def response = restTemplate.exchange("/head-line",
                GET,
                new HttpEntity<Object>(),
                List
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                it.size() == 2
                it[0].title == headLine1.title
                it[1].title == headLine2.title
            }
        }

        when: "获取所有可用头条"
        response = restTemplate.exchange("/head-line?available=true",
                GET,
                new HttpEntity<Object>(),
                List
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                it.size() == 1
                it[0].title == headLine2.title
                it[0].available
            }
        }

        when: "获取所有不可用头条"
        response = restTemplate.exchange("/head-line?available=false",
                GET,
                new HttpEntity<Object>(),
                List
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                it.size() == 1
                it[0].title == headLine1.title
                !it[0].available
            }
        }
    }

    def "GET /head-line/{id}"() {
        HeadLineE headLine1 = new HeadLineE(title: "test head line 1", available: false)
        HeadLineE headLine2 = new HeadLineE(title: "test head line 2", available: true)
        headLineMapper.insert(headLine1);
        headLineMapper.insert(headLine2);

        when: "头套不存在"
        def response = restTemplate.exchange("/head-line/{id}",
                GET,
                new HttpEntity<Object>(),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, HeadLineNotFoundException.HEAD_LINE_NOT_FOUND)

        when: "正常查询头条"
        response = restTemplate.exchange("/head-line/{id}",
                GET,
                new HttpEntity<Object>(),
                HeadLineInfoDTO,
                headLine1.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                title == headLine1.title
                available == headLine1.available
            }
        }
    }

    def "POST /head-line"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(userE)

        when: "标题和链接为空"
        def response = restTemplate.exchange("/head-line",
                POST,
                new HttpEntity<Object>([title: null, url: ""], headers),
                FailureDTO
        )
        then:
        badRequest(response, HEAD_LINE_TITLE_BLANK, HEAD_LINE_URL_BLANK)

        when: "标题和链接过长"
        response = restTemplate.exchange("/head-line",
                POST,
                new HttpEntity<Object>([title: "1" * 1000, url: "1" * 1000], headers),
                FailureDTO
        )
        then:
        badRequest(response, HEAD_LINE_TITLE_TOO_LONG, HEAD_LINE_URL_TOO_LONG)

        when: "正常创建，无图片"
        response = restTemplate.exchange("/head-line",
                POST,
                new HttpEntity<Object>([
                        title: "test title",
                        url: "http://0.0.0.0/image/head-line/" + defaultUUID
                ], headers),
                HeadLineInfoDTO
        )
        then:
        0 * _
        with(response) {
            statusCode == CREATED
            with(body) {
                title == "test title"
                url == "http://0.0.0.0/image/head-line/" + defaultUUID
                imageFileName == null
                available
                priority == 0
            }
        }

        when: "正常创建，有图片"
        response = restTemplate.exchange("/head-line",
                POST,
                new HttpEntity<Object>([
                        title: "test title",
                        url: "http://0.0.0.0/image/head-line/" + defaultUUID,
                        priority: 123,
                        available: false,
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                HeadLineInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.setRemoteService(remoteService)
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                title == "test title"
                url == "http://0.0.0.0/image/head-line/" + defaultUUID
                priority == 123
                !available
                imageFileName != null
            }
        }
    }

    def "PUT /head-line"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(userE)

        HeadLineE headLineE = new HeadLineE(
                title: "test title",
                url: "test url",
                available: false,
                priority: 0
        )
        headLineMapper.insert(headLineE)

        when: "正常更新，无图片"
        def response = restTemplate.exchange("/head-line",
                PUT,
                new HttpEntity<Object>([
                        id: headLineE.id,
                        title: "",
                        url: "",
                        available: true,
                        priority: 1
                ], headers),
                HeadLineInfoDTO
        )
        then:
        0 * _
        with(response) {
            statusCode == CREATED
            with(body) {
                id == headLineE.id
                lastUpdateTime.after(createTime)
                title == "test title"
                url == "test url"
                imageFileName == null
                available
                priority == 1
            }
        }

        when: "正常更新，有图片"
        response = restTemplate.exchange("/head-line",
                PUT,
                new HttpEntity<Object>([
                        id: headLineE.id,
                        title: "test title 1",
                        url: "test url 1",
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes),
                        available: true,
                        priority: 1
                ], headers),
                HeadLineInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.setRemoteService(remoteService)
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                id == headLineE.id
                lastUpdateTime.after(createTime)
                title == "test title 1"
                url == "test url 1"
                imageFileName != null
                available
                priority == 1
            }
        }

        when: "正常更新，替换图片"
        response = restTemplate.exchange("/head-line",
                PUT,
                new HttpEntity<Object>([
                        id: headLineE.id,
                        title: "test title 1",
                        url: "test url 1",
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes),
                        available: true,
                        priority: 1
                ], headers),
                HeadLineInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.setRemoteService(remoteService)
            1 * remoteService.uploadImageFile(*_)
            1 * remoteService.removeImageFile(ImageCategory.HEAD_LINE.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                id == headLineE.id
                lastUpdateTime.after(createTime)
                title == "test title 1"
                url == "test url 1"
                imageFileName != null
                available
                priority == 1
            }
        }

        when: "正常更新，删除图片"
        response = restTemplate.exchange("/head-line",
                PUT,
                new HttpEntity<Object>([
                        id: headLineE.id,
                        title: "test title 2",
                        url: "test url 2",
                        base64Image: "   ",
                        available: false,
                        priority: 2
                ], headers),
                HeadLineInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.setRemoteService(remoteService)
            1 * remoteService.removeImageFile(ImageCategory.HEAD_LINE.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                id == headLineE.id
                lastUpdateTime.after(createTime)
                title == "test title 2"
                url == "test url 2"
                imageFileName == null
                !available
                priority == 2
            }
        }
    }

    def "DELETE /head-line/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.ADMIN)
        def headers = createHttpHeaders(userE)

        HeadLineE headLineE = new HeadLineE(title: "test title", imageFileName: defaultUUID)
        headLineMapper.insert(headLineE)

        when: "id不存在"
        def response = restTemplate.exchange("/head-line/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, HeadLineNotFoundException.HEAD_LINE_NOT_FOUND)

        when: "正常删除"
        response = restTemplate.exchange("/head-line/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                headLineE.id
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.setRemoteService(remoteService)
            1 * remoteService.removeImageFile(ImageCategory.HEAD_LINE.name, defaultUUID)
            0 * _
        }
        response.statusCode == NO_CONTENT
        headLineMapper.selectByPrimaryKey(headLineE.id) == null
    }
}
