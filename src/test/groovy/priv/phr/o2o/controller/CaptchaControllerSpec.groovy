package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.Constant
import priv.phr.o2o.dto.CaptchaInfoDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpMethod
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import java.util.regex.Pattern

import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class CaptchaControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    def "POST /captcha"() {
        when:
        def response = restTemplate.exchange("/captcha",
                HttpMethod.POST,
                null,
                CaptchaInfoDTO
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                Constant.UUID_PATTERN.matcher(token).matches()
                Pattern.compile(Constant.BASE64_REGEXP).matcher(image).matches()
            }
        }
    }
}
