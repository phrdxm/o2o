package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.ImageCategory
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.exception.AccountFrozenException
import priv.phr.o2o.common.exception.UserNamesakeException
import priv.phr.o2o.common.exception.UserNotFoundException
import priv.phr.o2o.common.util.PasswordUtil
import priv.phr.o2o.common.util.Util
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.JwtDTO
import priv.phr.o2o.dto.UserInfoDTO
import priv.phr.o2o.entity.LocalAuthE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.LocalAuthMapper
import priv.phr.o2o.mapper.UserMapper
import priv.phr.o2o.service.ImageService
import priv.phr.o2o.service.RemoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.*
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class UserControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private UserMapper userMapper

    @Autowired
    private LocalAuthMapper localAuthMapper

    @Autowired
    private ImageService imageService

    def "POST /user/sign-in"() {
        def headers = new HttpHeaders()
        addCaptcha(headers)
        when: "缺少用户名密码"
        def response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: null, password: null], headers),
                FailureDTO
        )
        then:
        badRequest(response, PWD_NULL, USERNAME_NULL)

        when: "用户名密码格式错误"
        response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: " ", password: " "], headers),
                FailureDTO
        )
        then:
        badRequest(response, PWD_INVALID, USERNAME_INVALID)

        when: "用户名不存在"
        response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: "00000000", password: "1234567"], headers),
                FailureDTO
        )
        then:
        unauthorized(response, USERNAME_OR_PWD_ERROR)

        when: "账户冻结"
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.USER, frozen: true)
        userMapper.insert(userE)
        def password = PasswordUtil.encode("1234567")
        LocalAuthE localAuthE = new LocalAuthE(username: "test_username", password: password, userId: userE.id)
        localAuthMapper.insert(localAuthE)
        response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: localAuthE.username, password: "1234567"], headers),
                FailureDTO
        )
        then:
        forbidden(response, AccountFrozenException.ACCOUNT_FROZEN)

        when: "密码错误"
        userE.frozen = false
        userMapper.updateByPrimaryKey(userE)
        response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: localAuthE.username, password: "12345678"], headers),
                FailureDTO
        )
        then:
        unauthorized(response, USERNAME_OR_PWD_ERROR)

        when: "正常登录"
        response = restTemplate.exchange("/user/sign-in",
                POST,
                new HttpEntity<Object>([username: localAuthE.username, password: "1234567"], headers),
                JwtDTO
        )
        then:
        response.statusCode == OK
        Util.regexpMatches(JWT_PATTERN, response.body.jwt)
    }

    def "POST /user/register"() {
        def headers = new HttpHeaders()
        addCaptcha(headers)

        when: "用户名重复"
        def password = PasswordUtil.encode("1234567")
        LocalAuthE localAuthE = new LocalAuthE(username: "test_username", password: password)
        localAuthMapper.insert(localAuthE)

        def response = restTemplate.exchange("/user/register",
                POST,
                new HttpEntity<Object>([
                        username: localAuthE.username,
                        password: "1234567",
                        gender: Gender.FEMALE.code,
                ], headers),
                FailureDTO,
        )
        then:
        unprocessableEntity(response, UserNamesakeException.USER_NAMESAKE)

        when: "正常注册，无图片"
        response = restTemplate.exchange("/user/register",
                POST,
                new HttpEntity<Object>([
                        username: localAuthE.username + "1",
                        password: "1234567",
                        gender: Gender.FEMALE.code,
                        email: "abc@111.com"
                ], headers),
                UserInfoDTO,
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                nickname == localAuthE.username + "1"
                avatorFileName == null
                email == "abc@111.com"
                gender == Gender.FEMALE.code
                !frozen
                role == Role.USER.code
            }
        }

        when: "正常注册，有图片"
        response = restTemplate.exchange("/user/register",
                POST,
                new HttpEntity<Object>([
                        username: localAuthE.username + "2",
                        nickname: "test_nickname",
                        password: "1234567",
                        gender: Gender.MALE.code,
                        base64Avator: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                UserInfoDTO,
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                nickname == "test_nickname"
                Util.regexpMatches(UUID_PATTERN, avatorFileName)
                email == null
                gender == Gender.MALE.code
                !frozen
                role == Role.USER.code
            }
        }
    }

    def "GET /user?id={id}"() {
        UserE userE = new UserE(nickname: "test user", gender: Gender.MALE, frozen: false, role: Role.USER)
        def headers = createHttpHeaders(userE)

        when: "获取自己的用户信息"
        def response = restTemplate.exchange("/user",
                GET,
                new HttpEntity<Object>(headers),
                UserInfoDTO
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                nickname == userE.nickname
                avatorFileName == null
                email == null
                gender == Gender.MALE.code
                !frozen
                role == Role.USER.code
            }
        }

        when: "用户信息不存在"
        response = restTemplate.exchange("/user?id={id}",
                GET,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, UserNotFoundException.USER_NOT_FOUND)

        when: "正常获取其他用户信息"
        UserE other = new UserE(nickname: "other user", gender: Gender.FEMALE, frozen: true, role: Role.OWNER)
        userMapper.insert(other)
        response = restTemplate.exchange("/user?id={id}",
                GET,
                new HttpEntity<Object>(headers),
                UserInfoDTO,
                other.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                nickname == other.nickname
                avatorFileName == null
                email == null
                gender == Gender.FEMALE.code
                frozen
                role == Role.OWNER.code
            }
        }
    }

    def "PUT /user"() {
        UserE userE = new UserE(nickname: "test user", gender: Gender.MALE, frozen: false, role: Role.USER)
        def headers = createHttpHeaders(userE)

        when: "正常更新，新增图片"
        def response = restTemplate.exchange("/user",
                PUT,
                new HttpEntity<Object>([
                        nickname: userE.nickname + "1",
                        email: "abc@111.com",
                        gender: Gender.FEMALE.code,
                        base64Avator: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                UserInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.after(createTime)
                nickname == userE.nickname + "1"
                Util.regexpMatches(UUID_PATTERN, avatorFileName)
                email == "abc@111.com"
                gender == Gender.FEMALE.code
                !frozen
                role == Role.USER.code
            }
        }

        when: "正常更新，更换图片"
        response = restTemplate.exchange("/user",
                PUT,
                new HttpEntity<Object>([
                        base64Avator: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                UserInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            1 * remoteService.removeImageFile(ImageCategory.USER_PROFILE.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.after(createTime)
                nickname == userE.nickname + "1"
                Util.regexpMatches(UUID_PATTERN, avatorFileName)
                email == "abc@111.com"
                gender == Gender.FEMALE.code
                !frozen
                role == Role.USER.code
            }
        }

        when: "正常更新，删除图片"
        response = restTemplate.exchange("/user",
                PUT,
                new HttpEntity<Object>([
                        base64Avator: "  "
                ], headers),
                UserInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.USER_PROFILE.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                lastUpdateTime.after(createTime)
                nickname == userE.nickname + "1"
                avatorFileName == null
                email == "abc@111.com"
                gender == Gender.FEMALE.code
                !frozen
                role == Role.USER.code
            }
        }
    }

    def "PUT /user/password"() {
        UserE userE = new UserE(nickname: "test user", gender: Gender.MALE, frozen: false, role: Role.USER)
        def headers = createHttpHeaders(userE)
        addCaptcha(headers);
        def password = PasswordUtil.encode("1234567")
        LocalAuthE localAuthE = new LocalAuthE(username: "test_user", password: password, userId: userE.id)
        localAuthMapper.insert(localAuthE)

        when: "旧密码错误"
        def response = restTemplate.exchange("/user/password",
                PUT,
                new HttpEntity<Object>([originalPassword: "12345678", newPassword: "7654321"], headers),
                FailureDTO
        )
        then:
        unauthorized(response, ORIGINAL_PWD_ERROR)

        when: "正常更新"
        response = restTemplate.exchange("/user/password",
                PUT,
                new HttpEntity<Object>([originalPassword: "1234567", newPassword: "7654321"], headers),
                Object
        )
        then:
        response.statusCode == CREATED
    }

    def "POST /user/auth-token"() {
        UserE userE = new UserE(nickname: "test user", gender: Gender.MALE, frozen: false, role: Role.USER)
        def headers = createHttpHeaders(userE)
        def password = PasswordUtil.encode("1234567")
        LocalAuthE localAuthE = new LocalAuthE(username: "test_user", password: password, userId: userE.id)
        localAuthMapper.insert(localAuthE)

        when: "密码错误"
        def response = restTemplate.exchange("/user/auth-token",
                POST,
                new HttpEntity<Object>([password: "7654321"], headers),
                FailureDTO
        )
        then:
        unauthorized(response, PWD_ERROR)

        when: "正常获取"
        response = restTemplate.exchange("/user/auth-token",
                POST,
                new HttpEntity<Object>([password: "1234567"], headers),
                String
        )
        then:
        response.statusCode == CREATED
        Util.regexpMatches(UUID_PATTERN, response.body)
    }

    def cleanup() {
        cleanupEntity()
    }
}
