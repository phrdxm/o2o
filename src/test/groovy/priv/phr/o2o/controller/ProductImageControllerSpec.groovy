package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.ImageCategory
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.enumeration.ShopStatus
import priv.phr.o2o.common.exception.ProductImageNotFoundException
import priv.phr.o2o.common.exception.ProductNotFoundException
import priv.phr.o2o.common.util.Util
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.ProductImageInfoDTO
import priv.phr.o2o.entity.ProductE
import priv.phr.o2o.entity.ProductImageE
import priv.phr.o2o.entity.ShopE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.ProductImageMapper
import priv.phr.o2o.mapper.ProductMapper
import priv.phr.o2o.mapper.ShopMapper
import priv.phr.o2o.service.ImageService
import priv.phr.o2o.service.RemoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.*
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ProductImageControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ProductImageMapper productImageMapper

    @Autowired
    private ShopMapper shopMapper

    @Autowired
    private ProductMapper productMapper

    @Autowired
    private ImageService imageService

    def "GET /product-images?product-id={productId}"() {

        when: "空列表"
        def response = restTemplate.exchange("/product-images?product-id={productId}",
                GET,
                new HttpEntity<Object>(),
                List,
                defaultUUID
        )
        then:
        with(response) {
            statusCode == OK
            body == []
        }

        when: "非空列表"
        ProductImageE productImageE = new ProductImageE(
                productId: defaultUUID,
                description: "描述",
                fileName: defaultUUID
        )
        productImageMapper.insert(productImageE)
        response = restTemplate.exchange("/product-images?product-id={productId}",
                GET,
                new HttpEntity<Object>(),
                List,
                defaultUUID
        )
        then:
        with(response) {
            statusCode == OK
            body == [
                    [
                            id            : productImageE.id,
                            createTime    : productImageE.createTime.time,
                            lastUpdateTime: productImageE.lastUpdateTime.time,
                            fileName      : productImageE.fileName,
                            description   : productImageE.description,
                            productId     : productImageE.productId
                    ]
            ]
        }
    }

    def "POST /product-image"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)

        when: "物品id为空，描述过长，图片为空"
        def response = restTemplate.exchange("/product-image",
                POST,
                new HttpEntity<Object>([
                        productId  : null,
                        description: "0" * 256,
                        base64Image: null
                ], headers),
                FailureDTO
        )
        then:
        badRequest(response, UUID_ERROR, DESCRIPTION_TOO_LONG, IMAGE_NOT_NULL)

        when: "物品id格式错误，图片编码错误"
        response = restTemplate.exchange("/product-image",
                POST,
                new HttpEntity<Object>([
                        productId  : "abc",
                        description: "0" * 1,
                        base64Image: "图片"
                ], headers),
                FailureDTO
        )
        then:
        badRequest(response, UUID_ERROR, IMAGE_FILE_ERROR)

        when: "物品不存在"
        response = restTemplate.exchange("/product-image",
                POST,
                new HttpEntity<Object>([
                        productId  : defaultUUID,
                        description: "0" * 1,
                        base64Image: Base64.encoder.encode(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                FailureDTO
        )
        then:
        0 * _
        notFound(response, ProductNotFoundException.PRODUCT_NOT_FOUND)

        when: "物品不属于当前用户"
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id)
        productMapper.insert(productE)

        response = restTemplate.exchange("/product-image",
                POST,
                new HttpEntity<Object>([
                        productId  : productE.id,
                        description: "0" * 1,
                        base64Image: Base64.encoder.encode(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                FailureDTO
        )
        then:
        0 * _
        unauthorized(response, UNAUTH)

        when: "正常添加"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        response = restTemplate.exchange("/product-image",
                POST,
                new HttpEntity<Object>([
                        productId  : productE.id,
                        description: "0" * 1,
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                ProductImageInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                productId == productE.id
                description == "0"
                Util.regexpMatches(UUID_PATTERN, fileName)
            }
        }
    }

    def "PUT /product-image"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id)
        productMapper.insert(productE)

        when: "详情图实体不存在"
        def response = restTemplate.exchange("/product-image",
                PUT,
                new HttpEntity<Object>([
                        description: "描述",
                        id         : defaultUUID
                ], headers),
                FailureDTO
        )
        then:
        notFound(response, ProductImageNotFoundException.PRODUCT_IMAGE_NOT_FOUND)

        when: "详情图实体不属于当前用户"
        ProductImageE productImageE = new ProductImageE(description: "描述", productId: productE.id, fileName: defaultUUID)
        productImageMapper.insert(productImageE)
        response = restTemplate.exchange("/product-image",
                PUT,
                new HttpEntity<Object>([
                        description: "描述1",
                        id         : productImageE.id
                ], headers),
                FailureDTO
        )
        then:
        unauthorized(response, UNAUTH)

        when: "正常更新"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        response = restTemplate.exchange("/product-image",
                PUT,
                new HttpEntity<Object>([
                        description: "描述1",
                        id         : productImageE.id
                ], headers),
                ProductImageInfoDTO
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                description == "描述1"
                productId == productE.id
                fileName == productImageE.fileName
            }
        }
    }

    def "DELETE /product-image/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id)
        productMapper.insert(productE)
        ProductImageE productImageE = new ProductImageE(description: "描述", productId: productE.id, fileName: defaultUUID)
        productImageMapper.insert(productImageE)

        when: "正常删除"
        def response = restTemplate.exchange("/product-image/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                productImageE.id
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.PRODUCT_DETAIL.name, defaultUUID)
            0 * _
        }
        response.statusCode == NO_CONTENT
    }

    def cleanup() {
        cleanupEntity()
    }
}
