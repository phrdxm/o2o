package priv.phr.o2o.controller

import com.github.pagehelper.PageInfo
import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.ImageCategory
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.enumeration.ShopStatus
import priv.phr.o2o.common.exception.ProductCategoryNotFoundException
import priv.phr.o2o.common.exception.ProductNamesakeException
import priv.phr.o2o.common.exception.ProductNotFoundException
import priv.phr.o2o.common.exception.ShopNotFoundException
import priv.phr.o2o.common.util.Util
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.ProductInfoDTO
import priv.phr.o2o.entity.*
import priv.phr.o2o.entity.ProductCategoryE
import priv.phr.o2o.entity.ProductE
import priv.phr.o2o.entity.ProductImageE
import priv.phr.o2o.entity.ShopE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.ProductCategoryMapper
import priv.phr.o2o.mapper.ProductImageMapper
import priv.phr.o2o.mapper.ProductMapper
import priv.phr.o2o.mapper.ShopMapper
import priv.phr.o2o.mapper.UserMapper
import priv.phr.o2o.service.ImageService
import priv.phr.o2o.service.RemoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.*
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ProductControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ShopMapper shopMapper

    @Autowired
    private ProductMapper productMapper

    @Autowired
    private ProductCategoryMapper productCategoryMapper

    @Autowired
    private UserMapper userMapper

    @Autowired
    private ProductImageMapper productImageMapper

    @Autowired
    private ImageService imageService

    def "POST /product"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id)
        productMapper.insert(productE)

        when: "没有验证码"
        def response = restTemplate.exchange("/product",
                POST,
                new HttpEntity<Object>([name: productE.name, shopId: shopE.id, normalPrice: 1], headers),
                FailureDTO
        )
        then:
        badRequest(response, CAPTCHA_HEADER_NOT_FOUND)

        when: "物品名重复"
        addCaptcha(headers)
        response = restTemplate.exchange("/product",
                POST,
                new HttpEntity<Object>([name: productE.name, shopId: shopE.id, normalPrice: 1], headers),
                FailureDTO
        )
        then:
        unprocessableEntity(response, ProductNamesakeException.PRODUCT_NAMESAKE)

        when: "找不到物品分类"
        ProductCategoryE productCategoryE = new ProductCategoryE(name: "test category", shopId: defaultUUID)
        productCategoryMapper.insert(productCategoryE)
        response = restTemplate.exchange("/product",
                POST,
                new HttpEntity<Object>([
                        name       : productE.name + "1",
                        shopId     : shopE.id,
                        normalPrice: 1,
                        categoryId : productCategoryE.id
                ], headers),
                FailureDTO
        )
        then:
        notFound(response, ProductCategoryNotFoundException.PRODUCT_CATEGORY_NOT_FOUND)

        when: "正常创建，没有图片"
        productCategoryE.shopId = shopE.id
        productCategoryMapper.updateByPrimaryKey(productCategoryE)
        response = restTemplate.exchange("/product",
                POST,
                new HttpEntity<Object>([
                        name       : productE.name + "1",
                        shopId     : shopE.id,
                        normalPrice: 1,
                        categoryId : productCategoryE.id
                ], headers),
                ProductInfoDTO
        )
        then:
        0 * _
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, id)
                createTime != null
                lastUpdateTime != null
                name == productE.name + "1"
                normalPrice == 1
                categoryId == productCategoryE.id
                shopId == shopE.id
                available
                number == MIN_PRODUCT_NUMBER
            }
        }

        when: "正常创建并上传图片"
        response = restTemplate.exchange("/product",
                POST,
                new HttpEntity<Object>([
                        name       : productE.name + "2",
                        shopId     : shopE.id,
                        normalPrice: 1,
                        available  : false,
                        number     : 2,
                        base64Image: Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                ProductInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, id)
                Util.regexpMatches(UUID_PATTERN, imageFileName)
                createTime != null
                lastUpdateTime != null
                name == productE.name + "2"
                normalPrice == 1
                categoryId == null
                shopId == shopE.id
                !available
                number == 2
            }
        }
    }

    def "PUT /product"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE1 = new ProductE(name: "test product1", shopId: shopE.id, number: 1, normalPrice: 1, available: true)
        productMapper.insert(productE1)

        when: "物品找不到"
        def response = restTemplate.exchange("/product",
                PUT,
                new HttpEntity<Object>([id: defaultUUID], headers),
                FailureDTO
        )
        then:
        notFound(response, ProductNotFoundException.PRODUCT_NOT_FOUND)

        when: "物品不属于当前用户"
        response = restTemplate.exchange("/product",
                PUT,
                new HttpEntity<Object>([id: productE1.id], headers),
                FailureDTO
        )
        then:
        unauthorized(response, UNAUTH)

        when: "物品名称重复"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        ProductE productE2 = new ProductE(name: "test product2", shopId: shopE.id)
        productMapper.insert(productE2)
        response = restTemplate.exchange("/product",
                PUT,
                new HttpEntity<Object>([id: productE1.id, name: productE2.name], headers),
                FailureDTO
        )
        then:
        unprocessableEntity(response, ProductNamesakeException.PRODUCT_NAMESAKE)

        when: "正常更新，新增图片"
        response = restTemplate.exchange("/product",
                PUT,
                new HttpEntity<Object>([
                        id            : productE1.id,
                        name          : productE1.name + "1",
                        description   : "描述",
                        promotionPrice: 10,
                        base64Image   : Base64.encoder.encodeToString(new ClassPathResource("phr-o2o_watermark").inputStream.bytes)
                ], headers),
                ProductInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.uploadImageFile(*_)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, id)
                createTime != null
                lastUpdateTime.after(createTime)
                name == productE1.name + "1"
                description == "描述"
                number == 1
                normalPrice == 1
                promotionPrice == 10
                available
                Util.regexpMatches(UUID_PATTERN, imageFileName)
            }
        }

        when: "正常更新，移除图片"
        response = restTemplate.exchange("/product",
                PUT,
                new HttpEntity<Object>([
                        id            : productE1.id,
                        description   : "描述1",
                        promotionPrice: -1,
                        base64Image   : "",
                        number        : 100,
                        available     : false
                ], headers),
                ProductInfoDTO
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.PRODUCT.name, _)
            0 * _
        }
        with(response) {
            statusCode == CREATED
            with(body) {
                Util.regexpMatches(UUID_PATTERN, id)
                createTime != null
                lastUpdateTime.after(createTime)
                name == productE1.name + "1"
                description == "描述1"
                number == 100
                normalPrice == 1
                promotionPrice == null
                imageFileName == null
                !available
            }
        }
    }

    def "GET /products?shop-id={shopId}&category-id={categoryId}&page={page}&size={size}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)

        ProductCategoryE productCategoryE = new ProductCategoryE(shopId: shopE.id, name: "test category")
        productCategoryMapper.insert(productCategoryE)

        ProductE productE1 = new ProductE(
                name: "test product1",
                shopId: shopE.id,
                number: 1,
                normalPrice: 1,
                available: false
        )
        productMapper.insert(productE1)
        ProductE productE2 = new ProductE(
                name: "test product2",
                shopId: shopE.id,
                categoryId: productCategoryE.id,
                number: 2,
                normalPrice: 2,
                available: true
        )
        productMapper.insert(productE2)

        when: "店铺未找到"
        def response = restTemplate.exchange("/products?&shop-id={shopId}&page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID, 0, 0
        )
        then:
        notFound(response, ShopNotFoundException.SHOP_NOT_FOUND)

        when: "游客查看指定分类物品"
        response = restTemplate.exchange("/products?&shop-id={shopId}&category-id={categoryId}&page={page}&size={size}",
                GET,
                new HttpEntity<Object>(),
                PageInfo,
                shopE.id, productCategoryE.id, 0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 1
                list[0].name == productE2.name
            }
        }

        when: "非管理员查看别人的物品"
        response = restTemplate.exchange("/products?&shop-id={shopId}&page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                shopE.id, 0, 0
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 1
                list[0].name == productE2.name
            }
        }

        when: "非管理员查看自己的物品"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        response = restTemplate.exchange("/products?&shop-id={shopId}&page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                shopE.id, 0, 2
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 2
                list[0].name == productE1.name
                list[1].name == productE2.name
            }
        }

        when: "管理员查看别人的物品"
        shopE.ownerId = defaultUUID
        shopMapper.updateByPrimaryKey(shopE)
        userE.role = Role.ADMIN
        userMapper.updateByPrimaryKey(userE)
        response = restTemplate.exchange("/products?&shop-id={shopId}&page={page}&size={size}",
                GET,
                new HttpEntity<Object>(headers),
                PageInfo,
                shopE.id, 0, 2
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                list.size() == 2
                list[0].name == productE1.name
                list[1].name == productE2.name
            }
        }
    }

    def "GET /product/{id}"() {
        ProductE productE = new ProductE(name: "test product")
        productMapper.insert(productE)

        when:
        def response = restTemplate.exchange("/product/{id}",
                GET,
                new HttpEntity<Object>(),
                ProductInfoDTO,
                productE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                id == productE.id
                name == productE.name
            }
        }
    }

    def "DELETE /product/{id}"() {
        ProductImageE productImageE
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                categoryId: defaultUUID,
                areaId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductE productE = new ProductE(
                name: "test product",
                shopId: shopE.id,
                number: 1,
                normalPrice: 1,
                available: false,
                imageFileName: defaultUUID
        )
        productMapper.insert(productE)

        when: "正常删除，无详情图"
        def response = restTemplate.exchange("/product/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                productE.id
        )
        then:
        interaction {
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.PRODUCT.name, productE.imageFileName)
            0 * _
        }
        response.statusCode == NO_CONTENT

        when: "正常删除，有详情图"
        response = restTemplate.exchange("/product/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                productE.id
        )
        then:
        interaction {
            productE = new ProductE(
                    name: "test product",
                    shopId: shopE.id,
                    number: 1,
                    normalPrice: 1,
                    available: false,
                    imageFileName: defaultUUID
            )
            productMapper.insert(productE)
            productImageE = new ProductImageE(productId: productE.id, fileName: defaultUUID)
            productImageMapper.insert(productImageE)
            RemoteService remoteService = Mock()
            imageService.remoteService = remoteService
            1 * remoteService.removeImageFile(ImageCategory.PRODUCT.name, productE.imageFileName)
            1 * remoteService.removeImageFile(ImageCategory.PRODUCT_DETAIL.name, productImageE.fileName)
            0 * _
        }
        response.statusCode == NO_CONTENT
        !productImageMapper.existsWithPrimaryKey(productImageE.id)
    }

    def cleanup() {
        cleanupEntity()
    }
}
