package priv.phr.o2o.controller

import com.alibaba.fastjson.JSON
import com.github.pagehelper.PageInfo
import priv.phr.o2o.Application
import priv.phr.o2o.common.Constant
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.exception.MessageFormatException
import priv.phr.o2o.common.exception.MessageTargetNotFoundException
import priv.phr.o2o.common.util.JWTUtil
import priv.phr.o2o.common.util.TestWebSocketClient
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.MessageDTO
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.util.TestUtil.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class O2OWebSocketSpec extends Specification {

    @LocalServerPort
    private int port

    @Autowired
    private UserMapper userMapper

    @Autowired
    private TestRestTemplate restTemplate

    @Shared
    private List<TestWebSocketClient> clients = []

    def cleanup() {
        cleanupEntity()
        for (def client : clients) {
            client.close()
        }
    }

    def "websocket test"() {
        UserE user1 = new UserE(nickname: "test user 1", gender: Gender.MALE, role: Role.USER)
        def headers1 = createHttpHeaders(user1)
        def jwt1 = JWTUtil.createJWT(user1)
        UserE user2 = new UserE(nickname: "test user 2", gender: Gender.MALE, role: Role.USER)
        def headers2 = createHttpHeaders(user2)
        def jwt2 = JWTUtil.createJWT(user2)
        UserE user3 = new UserE(nickname: "test user 3", gender: Gender.MALE, role: Role.OWNER)
        userMapper.insert(user3)
        def jwt3 = JWTUtil.createJWT(user3)

        when: "没有权限，发送jwt获取权限，消息格式错误"
        TestWebSocketClient testWebSocketClient1 = new TestWebSocketClient("ws://localhost:" + port + "/chat")
        clients << testWebSocketClient1
        testWebSocketClient1.results = [
                new FailureDTO(Constant.UNAUTH),
                "ACK",
                "ACK",
                new FailureDTO(MessageFormatException.JSON_PARSE_ERROR),
                new FailureDTO(MessageTargetNotFoundException.MESSAGE_TARGET_NOT_FOUND),
                new FailureDTO(MessageFormatException.MESSAGE_TOO_LONG),
                new FailureDTO(MessageTargetNotFoundException.MESSAGE_TARGET_NOT_FOUND)
        ]
        assert testWebSocketClient1.connectBlocking()
        for (def msg : [
                "123",
                jwt1,
                jwt1,
                "123",
                JSON.toJSONString(new MessageDTO(to: "123", message: "消息")),
                JSON.toJSONString(new MessageDTO(to: defaultUUID, message: "0" * 256)),
                JSON.toJSONString(new MessageDTO(to: defaultUUID, message: "消息"))
        ]) {
            testWebSocketClient1.send(msg)
            testWebSocketClient1.receiveBlocking()
        }
        then:
        notThrown(Throwable)

        when: "正常发送"
        TestWebSocketClient testWebSocketClient2 = new TestWebSocketClient("ws://localhost:" + port + "/chat")
        clients << testWebSocketClient2
        testWebSocketClient2.results = [
                "ACK",
                new MessageDTO(from: user1.id, to: user2.id, message: "消息1"),
                new MessageDTO(from: user1.id, to: user2.id, message: "消息2")
        ]
        testWebSocketClient2.connectBlocking()
        testWebSocketClient2.send(jwt2)
        testWebSocketClient2.receiveBlocking()

        TestWebSocketClient testWebSocketClient3 = new TestWebSocketClient("ws://localhost:" + port + "/chat")
        clients << testWebSocketClient3
        testWebSocketClient3.results = [
                "ACK",
                new MessageDTO(from: user1.id, to: user3.id, message: "消息3"),
                new MessageDTO(from: user2.id, to: user3.id, message: "消息4")
        ]
        testWebSocketClient3.connectBlocking()
        testWebSocketClient3.send(jwt3)
        testWebSocketClient3.receiveBlocking()

        testWebSocketClient1.send(JSON.toJSONString(new MessageDTO(to: user2.id, message: "消息1")))
        testWebSocketClient2.receiveBlocking()

        testWebSocketClient1.send(JSON.toJSONString(new MessageDTO(to: user2.id, message: "消息2")))
        testWebSocketClient2.receiveBlocking()

        testWebSocketClient1.send(JSON.toJSONString(new MessageDTO(to: user3.id, message: "消息3")))
        testWebSocketClient3.receiveBlocking()

        testWebSocketClient2.send(JSON.toJSONString(new MessageDTO(to: user3.id, message: "消息4")))
        testWebSocketClient3.receiveBlocking()
        then:
        notThrown(Throwable)

        when: "查询消息记录，空记录"
        def response = restTemplate.exchange("/message-history?to={to}&page={page}&size={size}",
                HttpMethod.GET,
                new HttpEntity<Object>(headers1),
                PageInfo,
                user1.id, 0, 0
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            body.list == []
        }

        when: "查询消息记录，有记录"
        response = restTemplate.exchange("/message-history?to={to}&page={page}&size={size}",
                HttpMethod.GET,
                new HttpEntity<Object>(headers1),
                PageInfo,
                user2.id, 0, 1
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            with(body) {
                list.size() == 1
                list[0].from == user1.id
                list[0].to == user2.id
                list[0].message == "消息2"
                list[0].sendTime != null
            }
        }

        when: "反向查询"
        response = restTemplate.exchange("/message-history?to={to}&page={page}&size={size}",
                HttpMethod.GET,
                new HttpEntity<Object>(headers2),
                PageInfo,
                user1.id, 0, 1
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            with(body) {
                list.size() == 1
                list[0].from == user1.id
                list[0].to == user2.id
                list[0].message == "消息2"
                list[0].sendTime != null
            }
        }

        when: "查询有沟通记录的用户信息"
        response = restTemplate.exchange("/message/users",
                HttpMethod.GET,
                new HttpEntity<Object>(headers2),
                List
        )
        then:
        with(response) {
            statusCode == HttpStatus.OK
            [body[0].id, body[1].id].sort() == [user1.id, user3.id].sort()
        }
    }
}
