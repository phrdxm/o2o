package priv.phr.o2o.controller

import priv.phr.o2o.Application
import priv.phr.o2o.common.enumeration.Gender
import priv.phr.o2o.common.enumeration.Role
import priv.phr.o2o.common.enumeration.ShopStatus
import priv.phr.o2o.common.exception.ProductCategoryNamesakeException
import priv.phr.o2o.common.exception.ProductCategoryNotFoundException
import priv.phr.o2o.common.exception.ShopNotFoundException
import priv.phr.o2o.common.util.Util
import priv.phr.o2o.dto.FailureDTO
import priv.phr.o2o.dto.ProductCategoryInfoDTO
import priv.phr.o2o.entity.ProductCategoryE
import priv.phr.o2o.entity.ProductE
import priv.phr.o2o.entity.ShopE
import priv.phr.o2o.entity.UserE
import priv.phr.o2o.mapper.ProductCategoryMapper
import priv.phr.o2o.mapper.ProductMapper
import priv.phr.o2o.mapper.ShopMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import static priv.phr.o2o.common.Constant.*
import static priv.phr.o2o.common.util.TestUtil.*
import static org.springframework.http.HttpMethod.*
import static org.springframework.http.HttpStatus.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = Application)
@ActiveProfiles("test")
@Stepwise
class ProductCategoryControllerSpec extends Specification {

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private ShopMapper shopMapper

    @Autowired
    private ProductCategoryMapper productCategoryMapper

    @Autowired
    private ProductMapper productMapper

    def "GET /product-categories?shop-id={shopId}"() {
        when: "shop-id格式错误"
        def response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                GET,
                new HttpEntity<Object>(),
                FailureDTO,
                "abc"
        )
        then:
        badRequest(response, UUID_ERROR)

        when: "店铺不存在"
        response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                GET,
                new HttpEntity<Object>(),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, ShopNotFoundException.SHOP_NOT_FOUND)

        when: "正常获取"
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)
        ProductCategoryE productCategory1 = new ProductCategoryE(shopId: shopE.id, name: "category a")
        ProductCategoryE productCategory2 = new ProductCategoryE(shopId: shopE.id, name: "category b")
        productCategoryMapper.insert(productCategory1)
        productCategoryMapper.insert(productCategory2)

        response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                GET,
                new HttpEntity<Object>(),
                List,
                shopE.id
        )
        then:
        with(response) {
            statusCode == OK
            body == [
                    [
                            id            : productCategory1.id,
                            createTime    : productCategory1.createTime.time,
                            lastUpdateTime: productCategory1.lastUpdateTime.time,
                            name          : productCategory1.name,
                    ],
                    [
                            id            : productCategory2.id,
                            createTime    : productCategory2.createTime.time,
                            lastUpdateTime: productCategory2.lastUpdateTime.time,
                            name          : productCategory2.name,
                    ],
            ]
        }
    }

    def "GET /product-category/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER);
        def headers = createHttpHeaders(userE)

        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)

        ProductCategoryE productCategoryE = new ProductCategoryE(shopId: shopE.id, name: "test category")
        productCategoryMapper.insert(productCategoryE)

        when: "分类不存在"
        def response = restTemplate.exchange("/product-category/{id}",
                GET,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, ProductCategoryNotFoundException.PRODUCT_CATEGORY_NOT_FOUND)

        when: "分类不属于当前用户"
        response = restTemplate.exchange("/product-category/{id}",
                GET,
                new HttpEntity<Object>(headers),
                FailureDTO,
                productCategoryE.id
        )
        then:
        unauthorized(response, UNAUTH)

        when: "正常获取"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id, categoryId: productCategoryE.id)
        productMapper.insert(productE)
        response = restTemplate.exchange("/product-category/{id}",
                GET,
                new HttpEntity<Object>(headers),
                ProductCategoryInfoDTO,
                productCategoryE.id
        )
        then:
        with(response) {
            statusCode == OK
            with(body) {
                id == productCategoryE.id
                name == productCategoryE.name
            }
        }
    }

    def "POST /product-categories?shop-id={shopId}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)
        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)

        when: "物品分类名为空"
        def response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                POST,
                new HttpEntity<Object>([" ", "", null], headers),
                List,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            body == []
        }

        when: "物品分类名重复"
        ProductCategoryE productCategoryE = new ProductCategoryE(shopId: shopE.id, name: "product category a")
        productCategoryMapper.insert(productCategoryE)
        response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                POST,
                new HttpEntity<Object>([" ", "", null, " product category a ", "product category a"], headers),
                List,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            body == []
        }

        when: "正常创建"
        response = restTemplate.exchange("/product-categories?shop-id={shopId}",
                POST,
                new HttpEntity<Object>([" ", "", null, " product category a ", "product category b "], headers),
                List,
                shopE.id
        )
        then:
        with(response) {
            statusCode == CREATED
            body.size() == 1
            Util.regexpMatches(UUID_PATTERN, body[0].id)
            body[0].createTime != null
            body[0].lastUpdateTime != null
            body[0].name == "product category b"
        }
    }

    def "DELETE /product-category/{id}"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)

        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: defaultUUID,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)

        ProductCategoryE productCategoryE = new ProductCategoryE(shopId: shopE.id, name: "test category")
        productCategoryMapper.insert(productCategoryE)

        when: "分类不存在"
        def response = restTemplate.exchange("/product-category/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                FailureDTO,
                defaultUUID
        )
        then:
        notFound(response, ProductCategoryNotFoundException.PRODUCT_CATEGORY_NOT_FOUND)

        when: "分类不属于当前用户"
        response = restTemplate.exchange("/product-category/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                FailureDTO,
                productCategoryE.id
        )
        then:
        unauthorized(response, UNAUTH)

        when: "正常删除"
        shopE.ownerId = userE.id
        shopMapper.updateByPrimaryKey(shopE)
        ProductE productE = new ProductE(name: "test product", shopId: shopE.id, categoryId: productCategoryE.id)
        productMapper.insert(productE)
        response = restTemplate.exchange("/product-category/{id}",
                DELETE,
                new HttpEntity<Object>(headers),
                Object,
                productCategoryE.id
        )
        then:
        response.statusCode == NO_CONTENT
        productMapper.selectByPrimaryKey(productE.id).categoryId == null
    }

    def "PUT /product-category"() {
        UserE userE = new UserE(nickname: "test", gender: Gender.MALE, role: Role.OWNER)
        def headers = createHttpHeaders(userE)

        ShopE shopE = new ShopE(
                name: "test shop",
                status: ShopStatus.ENABLE,
                ownerId: userE.id,
                areaId: defaultUUID,
                categoryId: defaultUUID
        )
        shopMapper.insert(shopE)

        ProductCategoryE productCategory1 = new ProductCategoryE(shopId: shopE.id, name: "test category1")
        productCategoryMapper.insert(productCategory1)

        ProductCategoryE productCategory2 = new ProductCategoryE(shopId: shopE.id, name: "test category2")
        productCategoryMapper.insert(productCategory2)

        when: "新分类名为空"
        def response = restTemplate.exchange("/product-category",
                PUT,
                new HttpEntity<Object>([id: productCategory1.id, name: ""], headers),
                ProductCategoryInfoDTO
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                id == productCategory1.id
                createTime == productCategory1.createTime
                lastUpdateTime == productCategory1.lastUpdateTime
                name == productCategory1.name
            }
        }

        when: "名称重复"
        response = restTemplate.exchange("/product-category",
                PUT,
                new HttpEntity<Object>([id: productCategory1.id, name: productCategory2.name + " "], headers),
                FailureDTO
        )
        then:
        unprocessableEntity(response, ProductCategoryNamesakeException.PRODUCT_CATEGORY_NAMESAKE)

        when: "正常更新"
        response = restTemplate.exchange("/product-category",
                PUT,
                new HttpEntity<Object>([id: productCategory1.id, name: " new name "], headers),
                ProductCategoryInfoDTO
        )
        then:
        with(response) {
            statusCode == CREATED
            with(body) {
                id == productCategory1.id
                createTime == productCategory1.createTime
                lastUpdateTime.after(createTime)
                name == "new name"
            }
        }
    }

    def cleanup() {
        cleanupEntity()
    }
}
