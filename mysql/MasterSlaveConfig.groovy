@Grab('mysql:mysql-connector-java:5.1.47')
@GrabConfig(systemClassLoader = true)

import groovy.sql.Sql

import java.sql.ResultSetMetaData

String masterIp = args[0]
String masterRootPwd = args[1]
String slaveIp = args[2]
String slaveRootPwd = args[3]

String slaveUser = "slave"
String slavePwd = "be8b7e58"
String driverName = "com.mysql.jdbc.Driver"

def masterSql = Sql.newInstance("jdbc:mysql://${masterIp}:3306/o2o_db", "root", masterRootPwd, driverName)
masterSql.eachRow("SELECT user, host FROM mysql.user WHERE user = ?;", [slaveUser]) {
    masterSql.execute("DROP USER ?@?;", [it.user, it.host])
    masterSql.execute("FLUSH PRIVILEGES;")
}
masterSql.execute("CREATE USER ${slaveUser};")
masterSql.execute("GRANT REPLICATION SLAVE ON *.* TO ?@? IDENTIFIED BY ?;", [slaveUser, slaveIp, slavePwd])
masterSql.execute("FLUSH PRIVILEGES;")
String masterLogFileName = null
Long masterLogPos = null
masterSql.eachRow("SHOW MASTER STATUS;") {
    masterLogFileName = it.File
    masterLogPos = it.Position
}
masterSql.close()

def slaveSql = Sql.newInstance("jdbc:mysql://${slaveIp}:3306/o2o_db", "root", slaveRootPwd, driverName)
slaveSql.execute("STOP SLAVE;")
slaveSql.execute("""
CHANGE MASTER TO master_host=?,
master_port=3306,
master_user=?,
master_password=?,
master_log_file=?,
master_log_pos=?;
""", [masterIp, slaveUser, slavePwd, masterLogFileName, masterLogPos])
slaveSql.execute("START SLAVE;")
slaveSql.eachRow("SHOW SLAVE STATUS;") {
    println "=" * 16
    ResultSetMetaData metaData = it.getMetaData()
    for (int i = 1; i <= metaData.columnCount; i++) {
        def columnLabel = metaData.getColumnLabel(i)
        println "${columnLabel}: ${it.getString(columnLabel)}"
    }
}
slaveSql.close()